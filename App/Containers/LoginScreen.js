import React, { Component } from 'react'
import {
  Text,
  View,
  TextInput,
  KeyboardAvoidingView,
  TouchableOpacity,
  Image
} from 'react-native'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/LoginScreenStyle'
import { Images } from '../Themes'
import { Actions } from 'react-native-router-flux'

class LoginScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      // textWrongPw: 'Oops! Wrong password. Try again!',
      // textHeader: 'Please type your password here.',
      phoneNumber: 'x'
    }
  }
  render () {
    const { phoneNumber } = this.state
    return (
      <View style={styles.container}>
        <Text style={styles.txtHeader}>Discover the best plates near you!</Text>
        <KeyboardAvoidingView style={styles.keyboardContainer} behavior='padding'>
          <View>
            <View style={styles.rowInput}>
              <Image source={Images.icFlag} style={styles.icFlag} />
              <TextInput
                style={styles.textInput}
                onChangeText={(text) => this.setState({ phoneNumber: text })}
                value={this.state.phoneNumber}
                placeholder='+971   Your mobile number'
                underlineColorAndroid={'transparent'}
                keyboardType='phone-pad'
              />
            </View>
            <Text style={styles.txtConnectSocial}>Connect with your preferred social networks.</Text>
          </View>
          {this.state.phoneNumber ? <View>
            <TouchableOpacity style={styles.btnArrow} onPress={() => Actions.loginPhone({ phoneNumber })}>
              <Image source={Images.icArrowRight} style={styles.icArrowRight} />
            </TouchableOpacity>
          </View> : null}
        </KeyboardAvoidingView>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen)
