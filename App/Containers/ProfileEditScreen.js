import React, { Component } from 'react'
import { Text, TouchableOpacity, Image, View, TextInput, StyleSheet, Alert, NativeModules } from 'react-native'
import { connect } from 'react-redux'
import DateTimePicker from 'react-native-modal-datetime-picker';
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/ProfileEditScreenStyle'
import { CardView, Hr } from '../Components'
import { Images, Metrics, Fonts, Colors } from '../Themes'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { Actions } from 'react-native-router-flux'
import ImagePicker from 'react-native-image-crop-picker'
import { bindActionCreators } from '../Redux/Actions'
import ProfileActions from '../Redux/ProfileRedux'
import { showError } from '../Utilities/utils'
import ActionSheet from 'react-native-actionsheet'
import RNPickerSelect from 'react-native-picker-select'
import _ from 'lodash'
import Permissions from 'react-native-permissions'
import RNGooglePlaces from 'react-native-google-places'
import Api from '../Services/Api'
import firebase from 'react-native-firebase'
import { LoginManager, AccessToken } from 'react-native-fbsdk'
import { GoogleSignin } from 'react-native-google-signin'
import Config from 'react-native-config'
const { RNTwitterSignIn } = NativeModules

const api = Api.create()

class ProfileEditScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      profile: {
        ...props.profile.user_meta,
        phone_number: props.profile.user_meta.phone_number || firebase.auth().currentUser.phoneNumber
      },
      username: props.profile.username,
      isDateTimePickerVisible: false
    }
  }

  componentDidMount () {
    setTimeout(() => {
      Actions.refresh({
        rightTitle: <Text style={styles.txtSkip}>Done</Text>,
        onRight: this.onSave
      })
    }, 300)
  };

  async _handlerActionSheetAvatar (index) {
    try {
      const options = {
        width: 400,
        height: 400,
        cropping: true,
        includeBase64: false
      }
      let image = null
      if (index === 0) {
        image = await ImagePicker.openCamera(options)
      }
      if (index === 1) {
        image = await ImagePicker.openPicker(options)
      }
      if (image && image.path) {
        this.props.uploadAvatar(image)
      }
    } catch (error) {
      __DEV__ && console.log(error.message)
    }
  }

  async getUserPlace () {
    let result = await Permissions.check('location', { type: 'whenInUse' })
    if (result === 'undetermined') {
      result = await Permissions.request('location', { type: 'whenInUse' })
      if (result !== 'authorized') {
        return false
      }
    } else if (result === 'denied') {
      Alert.alert('Plate requires to access to your location')
      return false
    }
    const places = await RNGooglePlaces.getCurrentPlace()
    if (!places.length) {
      return false
    }
    const place = places[0]
    const placeResponse = await api.getLocation(place)
    __DEV__ && console.log('placeResponse', placeResponse)
    if (!placeResponse.ok) {
      Alert.alert('Can not get your location')
      return false
    }
    let city = ''
    let neighborhood = ''
    let country = ''
    for (let result of placeResponse.data.results) {
      for (let addressComponent of result.address_components) {
        if (addressComponent.types.includes('country') && !country) {
          country = addressComponent.short_name
        }
        if (addressComponent.types.includes('locality') && !city) {
          city = addressComponent.short_name
        }
        if (addressComponent.types.includes('neighborhood') && !neighborhood) {
          neighborhood = addressComponent.short_name
        }
      }
    }
    const address = []
    neighborhood && address.push(neighborhood)
    city && address.push(city)
    if (city || neighborhood) {
      this.props.updateProfile({
        geo_loc: {
          lat: neighborhood.latitude,
          lon: neighborhood.longitude
        },
        location: {
          city,
          neighborhood,
          country
        }
      })
    }
  }

  onSave = () => {
    const { profile, username } = this.state
    this.props.updateProfile({
      user_meta: profile,
      username
    })
  }

  _handlerActionSheetGender (index) {
    if (index === 0) {
      this.setState({ profile: { ...this.state.profile, gender: 'male' } })
    }
    if (index === 1) {
      this.setState({ profile: { ...this.state.profile, gender: 'female' } })
    }
  }

  async fbLogin () {
    try {
      LoginManager.logOut()
    } catch (error) { }
    try {
      const result = await LoginManager.logInWithReadPermissions(['email', 'public_profile'])
      __DEV__ && console.log('Login result', result)
      if (!result.isCancelled) {
        const result = await AccessToken.getCurrentAccessToken()
        __DEV__ && console.log('result', result)

        // create a new firebase credential with the token
        const credential = firebase.auth.FacebookAuthProvider.credential(result.accessToken)

        // login with credential
        await firebase.auth().currentUser.linkAndRetrieveDataWithCredential(credential)
      }
    } catch (error) {
      __DEV__ && console.log('FB Login error: ', error.message, error.stack)
      showError(error.message)
      // console.log(JSON.stringify(error, null, 2))
      // showError('' + error)
    }
  }

  async googleLogin () {
    try {
      await GoogleSignin.signOut()
    } catch (error) { }
    try {
      const hasPlayService = await GoogleSignin.hasPlayServices({ autoResolve: true, showPlayServicesUpdateDialog: true })
      if (hasPlayService) {
        await GoogleSignin.configure({
          scopes: ['https://www.googleapis.com/auth/userinfo.profile'], // what API you want to access on behalf of the user, default is email and profile
          // iosClientId: Config.GOOGLE_IOS_CLIENT_ID, // only for iOS
          // webClientId: Config.GOOGLE_WEB_CLIENT_ID, // client ID of type WEB for your server (needed to verify user ID and offline access)
          // offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
          // hostedDomain: '', // specifies a hosted domain restriction
          forceConsentPrompt: true // [Android] if you want to show the authorization prompt at each login
          // accountName: '' // [Android] specifies an account name on the device that should be used
        })
        const result = await GoogleSignin.signIn()
        __DEV__ && console.log(JSON.stringify(result, null, 2))
        // alert(JSON.stringify(result))
        const credential = firebase.auth.GoogleAuthProvider.credential(result.idToken, result.accessToken)
        await firebase.auth().currentUser.linkAndRetrieveDataWithCredential(credential)
      } else {
        showError('Login with google requires Google Play services installed on your device')
      }
    } catch (error) {
      __DEV__ && console.log('Google login error', error, error.message)
      error.code !== -5 && error.code !== 12501 && showError(error.message)
    }
  }

  async twitterLogin () {
    try {
      RNTwitterSignIn.init(Config.TWITTER_COMSUMER_KEY, Config.TWITTER_CONSUMER_SECRET)
      const result = await RNTwitterSignIn.logIn()
      // alert(JSON.stringify(result))
      const { authToken, authTokenSecret } = result
      const credential = firebase.auth.TwitterAuthProvider.credential(authToken, authTokenSecret)
      await firebase.auth().currentUser.linkAndRetrieveDataWithCredential(credential)
    } catch (error) {
      __DEV__ && console.log(error)
    }
  }

  render () {
    const { profile, username } = this.state
    const linkedFacebook = firebase.auth().currentUser.providerData.find(provider => provider.providerId === 'facebook.com')
    const linkedGoogle = firebase.auth().currentUser.providerData.find(provider => provider.providerId === 'google.com')
    const linkedTwitter = firebase.auth().currentUser.providerData.find(provider => provider.providerId === 'twitter.com')
    return (
      <KeyboardAwareScrollView innerRef={ref => { this.scroll = ref }} style={styles.container}>
        <CardView style={styles.cardAva}>
          <TouchableOpacity onPress={() => this.actionSheetAvatar.show()}>
            <Image source={this.props.avatar ? { uri: this.props.avatar } : Images.profile} defaultSource={Images.profile} style={styles.icAvaMan} resizeMode='cover' />
            <Text style={styles.txtChangePF}>Change Profile Photo</Text>
          </TouchableOpacity>
        </CardView>
        <CardView style={styles.cardName}>
          <View style={styles.rowCardName}>
            <Text style={styles.labelCardName}>Username</Text>
            <TextInput
              style={styles.inputCardName}
              onChangeText={(username) => this.setState({ username })}
              value={username}
              placeholder='Username'
              underlineColorAndroid='transparent'
            />
          </View>

          <View style={styles.rowCardName}>
            <Text style={styles.labelCardName}>First name</Text>
            <TextInput
              style={styles.inputCardName}
              onChangeText={(first_name) => this.setState({ profile: { ...profile, first_name } })}
              value={profile.first_name}
              placeholder='First name'
              underlineColorAndroid='transparent'
              autoCapitalize={'words'}
            />
          </View>

          <View style={styles.rowCardName}>
            <Text style={styles.labelCardName}>Last name</Text>
            <TextInput
              style={styles.inputCardName}
              onChangeText={(last_name) => this.setState({ profile: { ...profile, last_name } })}
              value={profile.last_name}
              placeholder='Last name'
              underlineColorAndroid='transparent'
              autoCapitalize={'words'}
            />
          </View>

          {/* <View style={styles.rowCardName}> */}
          {/* <Text style={styles.labelCardName}>Location</Text> */}
          {/* <TouchableOpacity style={[styles.inputWrap]} onPress={() => this.getUserPlace()}> */}
          {/* <Text style={styles.rowValue}>{profile.city || 'Detect your location'}</Text> */}
          {/* </TouchableOpacity> */}
          {/* /!* <TextInput */}
          {/* readOnly */}
          {/* style={styles.inputCardName} */}
          {/* onChangeText={(location) => this.setState({ profile: { ...profile, location } })} */}
          {/* value={profile.location} */}
          {/* placeholder='Location' */}
          {/* underlineColorAndroid='transparent' */}
          {/* /> *!/ */}
          {/* </View> */}

          {/* <View style={styles.rowCardName}> */}
          {/* <Text style={styles.labelCardName}>Favorite cusines?</Text> */}
          {/* <TextInput */}
          {/* readOnly */}
          {/* style={styles.inputCardName} */}
          {/* onChangeText={(favorite_cusines) => this.setState({ profile: { ...profile, favorite_cusines } })} */}
          {/* value={profile.favorite_cusines} */}
          {/* placeholder='Favorite cusines' */}
          {/* underlineColorAndroid='transparent' */}
          {/* /> */}
          {/* </View> */}

          {/* <View style={styles.rowCardName}> */}
          {/* <Text style={styles.labelCardName}>Favorite food?</Text> */}
          {/* <TextInput */}
          {/* readOnly */}
          {/* style={styles.inputCardName} */}
          {/* onChangeText={(favorite_food) => this.setState({ profile: { ...profile, favorite_food } })} */}
          {/* value={profile.favorite_food} */}
          {/* placeholder='Favorite food' */}
          {/* underlineColorAndroid='transparent' */}
          {/* /> */}
          {/* </View> */}

        </CardView>
        <CardView style={[styles.cardName, { paddingBottom: Metrics.doubleBaseMargin, flex: 1 }]}>
          <Text style={styles.title}>Private Information</Text>

          <View style={styles.rowCardName}>
            <Text style={styles.labelCardName}>Email</Text>
            <TextInput
              style={styles.inputCardName}
              onChangeText={(email) => this.setState({ profile: { ...profile, email } })}
              value={profile.email}
              placeholder='Email address'
              underlineColorAndroid='transparent'
              keyboardType={'email-address'}
            />
          </View>

          <View style={styles.rowCardName}>
            <Text style={styles.labelCardName}>Phone number</Text>
            {/* <TextInput */}
            {/* style={styles.inputCardName} */}
            {/* onChangeText={(phone_number) => this.setState({ profile: { ...profile, phone_number } })} */}
            {/* value={profile.phone_number} */}
            {/* placeholder='Phone number' */}
            {/* underlineColorAndroid='transparent' */}
            {/* keyboardType={'phone-pad'} */}
            {/* /> */}
            <TouchableOpacity
              style={[styles.inputWrap]}
              activeOpacity={0.7}
              onPress={() => Actions.updatePhoneNumber({ profile, username })}
            >
              <Text style={[styles.inputWrapText, { color: '#0F1524' }]}>
                {profile.phone_number}
              </Text>
            </TouchableOpacity>
          </View>

          <View style={styles.rowCardName}>
            <Text style={styles.labelCardName}>Gender</Text>
            <TouchableOpacity
              style={[styles.inputWrap]}
              activeOpacity={0.7}
              onPress={() => this.actionSheetGender.show()}
            >
              <Text style={[styles.inputWrapText, { color: profile.gender ? '#0F1524' : '#BDBDBD' }]}>
                {profile.gender ? (profile.gender === 'male' ? 'Male' : 'Female') : 'Gender'}
              </Text>
            </TouchableOpacity>
          </View>

          <View style={styles.rowCardName}>
            <Text style={styles.labelCardName}>Birth year</Text>
            <View style={[styles.inputWrap]}>
              <RNPickerSelect
                placeholder={{
                  label: 'Birth year',
                  value: null
                }}
                items={_.range(new Date().getFullYear(), 1900, -1).map((item) => {
                  return { label: String(item), value: String(item) }
                })}
                onValueChange={(value) => this.setState({ profile: { ...this.state.profile, birth_year: value } })}
                style={{ ...pickerSelectStyles, underline: { opacity: 0 } }}
                placeholderTextColor='#BDBDBD'
                value={String(profile.birth_year)}
                hideIcon
                textInputProps={
                  {
                    underlineColorAndroid: 'transparent'
                  }
                }
              />
            </View>
          </View>

        </CardView>
        <CardView style={[styles.cardName, { marginBottom: Metrics.doubleBaseMargin, paddingBottom: 0 }]}>
          <Text style={styles.title}>Social Networks</Text>
          <View style={styles.rowItem}>
            <Image source={Images.icFb} style={{ height: 23, width: 11 }} />
            <Text style={[styles.txtSocialName, { marginLeft: 29 }]}>Facebook</Text>
            <TouchableOpacity
              activeOpacity={0.7}
              onPress={() => !linkedFacebook && this.fbLogin()}
            >
              <Text style={styles.txtSocialStatus}>{linkedFacebook ? 'Connected' : 'Connect'}</Text>
            </TouchableOpacity>
          </View>
          <Hr lineHeight={1} marginLeft={-20} marginRight={-20} lineColor={Colors.divide} marginTop={0} marginBottom={0} />
          <View style={styles.rowItem}>
            <Image source={Images.icGg} style={styles.iconSocial} />
            <Text style={[styles.txtSocialName, { marginLeft: 29 }]}>Google</Text>
            <TouchableOpacity
              activeOpacity={0.7}
              onPress={() => !linkedGoogle && this.googleLogin()}
            >
              <Text style={styles.txtSocialStatus}>{linkedGoogle ? 'Connected' : 'Connect'}</Text>
            </TouchableOpacity>
          </View>
          {/* <Hr lineHeight={1} marginLeft={-20} marginRight={-20} lineColor={Colors.divide} marginTop={0} marginBottom={0} /> */}
          {/* <View style={styles.rowItem}> */}
          {/* <Image source={Images.icTw} style={styles.iconSocial} /> */}
          {/* <Text style={[styles.txtSocialName, { marginLeft: 29 }]}>Twitter</Text> */}
          {/* <TouchableOpacity */}
          {/* activeOpacity={0.7} */}
          {/* onPress={() => !linkedTwitter && this.twitterLogin()} */}
          {/* > */}
          {/* <Text style={styles.txtSocialStatus}>{linkedTwitter ? 'Connected' : 'Connect'}</Text> */}
          {/* </TouchableOpacity> */}
          {/* </View> */}
        </CardView>

        <ActionSheet
          ref={o => { this.actionSheetAvatar = o }}
          title='Pick Avatar'
          options={['Capture Photo', 'Open Gallery', 'Cancel']}
          cancelButtonIndex={2}
          onPress={(index) => this._handlerActionSheetAvatar(index)}
        />
        <ActionSheet
          ref={o => { this.actionSheetGender = o }}
          title='Gender'
          options={['Male', 'Female', 'Cancel']}
          cancelButtonIndex={2}
          onPress={(index) => this._handlerActionSheetGender(index)}
        />
      </KeyboardAwareScrollView>
    )
  }
}

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontFamily: Fonts.type.avenirRoman,
    fontSize: Fonts.size.medium,
    paddingTop: 10,
    color: '#0F1524'
  },
  inputAndroid: {
    // fontFamily: Fonts.type.avenirRoman,
    // fontSize: Fonts.size.medium,
    color: '#0F1524',
    width: 100,
    marginLeft: -7
  }
})

const mapStateToProps = (state) => {
  return {
    profile: state.profile.profile,
    user: state.profile.profile.user_meta,
    avatar: state.profile.profile.user_meta.avatar_url
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    ...bindActionCreators(ProfileActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfileEditScreen)
