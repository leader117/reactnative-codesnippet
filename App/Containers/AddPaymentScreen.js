import React, { Component } from 'react'
import {
  Text,
  View,
  TouchableOpacity,
  ScrollView
} from 'react-native'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/AddPaymentScreenStyle'
import { Actions } from 'react-native-router-flux'
import { Hr, FullButton } from '../Components'
import { Images } from '../Themes'
import { Image } from 'react-native-animatable'

class AddPayment extends Component {
  constructor (props) {
    super(props)
    this.state = {
      // textWrongPw: 'Oops! Wrong password. Try again!',
      // textHeader: 'Please type your password here.',
    }
  }

  render () {
    return (
      <ScrollView style={styles.container} contentContainerStyle={{ paddingBottom: 50 }}>
        <Text style={styles.txtHeader}>You’ll need a payment method to begin ordering.</Text>
        <View style={styles.rowContainer}>
          <Image source={Images.paymentMethod} style={styles.squareView} resizeMode='contain' />
          {/* <View style={styles.squareView}>
            <Text style={styles.txtPlaceHolder}>{`Placeholder\nIllustration to come`}</Text>
          </View> */}
          <FullButton
            text='Add a Payment Method'
            styles={styles.btnAddPayment}
            onPress={Actions.addCard}
          />
          <TouchableOpacity style={styles.btnSkip} onPress={Actions.root}>
            <Text style={[styles.txtSkip, { fontSize: 14 }]}>
              Or, skip, and add a payment method later …
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddPayment)
