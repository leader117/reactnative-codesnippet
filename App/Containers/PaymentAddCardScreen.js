import React, { Component } from 'react'
import { ScrollView, Text, Image, View, TouchableOpacity, ImageBackground } from 'react-native'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/PaymentAddCardScreenStyle'
import { Hr } from '../Components'
import { Images, Metrics, Colors } from '../Themes'
import SafeAreaView from 'react-native-safe-area-view'
import Ionicons from 'react-native-vector-icons/Ionicons'
import { Actions } from 'react-native-router-flux'
import TextInput from '../Components/card/TextInput'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

class PaymentAddCardScreen extends Component {
  state = {

  }

  render () {
    return (
      <View style={styles.container}>
        <KeyboardAwareScrollView>
          <Text style={styles.title}>Please verify your card information.</Text>
          <View style={styles.form}>
            <TextInput
              ref='number'
              isMask
              type={'credit-card'}
              placeholder='Credit Card Number'
              showLabel
              value={this.state.number}
              onChangeText={(number) => {
                this.setState({ number })
                if (number && number.length === 19) {
                  this.refs.expiryDate.focus()
                }
              }}
            />
            <View style={styles.formRow}>
              <TextInput
                ref='expiryDate'
                isMask
                containerStyle={{ flex: 1, marginRight: 10 }}
                type={'datetime'}
                options={{
                  format: 'MM/YY'
                }}
                placeholder='Expiry Date'
                showLabel
                value={this.state.expiryDate}
                onChangeText={(expiryDate) => {
                  this.setState({ expiryDate })
                  if (expiryDate && expiryDate.length === 5) {
                    this.refs.ccv.focus()
                  }
                }}
                onKeyPress={(event) => {
                  if (event.nativeEvent.key === 'Backspace') {
                    if (this.state.expiryDate === '') {
                      this.refs.number.focus()
                    }
                  }
                }}
              />
              <TextInput
                ref='ccv'
                isMask
                containerStyle={{ flex: 1, marginLeft: 10 }}
                type={'only-numbers'}
                placeholder='CCV'
                maxLength={3}
                showLabel
                value={this.state.ccv}
                onChangeText={(ccv) => {
                  this.setState({ ccv })
                  if (ccv && ccv.length === 3) {
                    this.refs.name.focus()
                  }
                }}
                onKeyPress={(event) => {
                  if (event.nativeEvent.key === 'Backspace') {
                    if (this.state.ccv === '') {
                      this.refs.expiryDate.focus()
                    }
                  }
                }}
              />
            </View>
            <TextInput
              ref='name'
              containerStyle={{ marginTop: 30 }}
              type={'custom'}
              placeholder='Card Holder Name'
              showLabel
              value={this.state.name}
              onChangeText={(name) => this.setState({ name })}
              onKeyPress={(event) => {
                if (event.nativeEvent.key === 'Backspace') {
                  if (this.state.name === '') {
                    this.refs.ccv.focus()
                  }
                }
              }}
            />
          </View>
          <SafeAreaView>
            <TouchableOpacity
              style={styles.btn}
              activeOpacity={0.7}
              onPress={() => Actions.pop()}
            >
              <Text style={styles.btnText}>Next</Text>
            </TouchableOpacity>
          </SafeAreaView>
        </KeyboardAwareScrollView>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PaymentAddCardScreen)
