import React, { Component } from 'react'
import { ScrollView, Text, Image, View, TouchableOpacity, FlatList } from 'react-native'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/RestaurantPickUpPaySuccessScreenStyle'
import { Hr, CardView } from '../Components'
import { Images, Metrics, Colors } from '../Themes'
import SafeAreaView from 'react-native-safe-area-view'
import Entypo from 'react-native-vector-icons/Entypo'
import { Actions } from 'react-native-router-flux'

class RestaurantPickUpPaySuccessScreen extends Component {
  state = {
  }

  componentDidMount () {
    setTimeout(() => Actions.refresh({
      title: 'P.F. Chang’s'.toUpperCase()
    }), 300)
  }

  render () {
    const data = [
      {
        name: 'Dynamite Shrimp',
        qty: 1,
        aed: 37.60
      },
      {
        name: 'Diet Coke',
        qty: 1,
        aed: 10.00
      },
      {
        name: 'Brownie',
        qty: 1,
        aed: 20.00
      }
    ]

    return (
      <View style={styles.container}>
        <ScrollView contentContainerStyle={[styles.scrollView]}>
          <View style={{ alignSelf: 'center' }}>
            <Image style={styles.imageCheck} source={Images.iconCheck} />
          </View>
          <Text style={styles.txtSuccess}>Success</Text>
          <Text style={styles.txtInfo}>You have successfully paid for your meal!</Text>
          <Hr lineHeight={8} marginBottom={5} marginTop={20} marginLeft={0} marginRight={0} />
          <Text style={styles.txtInfo}>Just show the code to a staff member {'\n'} and pick-up your order!</Text>
          <Text style={styles.txtCode}>{'#598'}</Text>
          <Hr lineHeight={8} marginBottom={5} marginTop={20} marginLeft={0} marginRight={0} />
          <FlatList
            data={data}
            keyExtractor={(item, index) => index.toString()}
            ListHeaderComponent={() => (
              <View style={styles.rowHeader}>
                <Text style={styles.rowHeaderTitle}>Reciept</Text>
                <Entypo name='dot-single' size={20} color='#BDBDBD' />
                <Text style={styles.rowHeaderValue}>{data.length} Dishes</Text>
              </View>
            )}
            renderItem={({ item, index }) => (
              <View style={styles.item}>
                <View style={styles.rowItem}>
                  <Text numberOfLines={1} style={styles.rowItemName}>{index + 1}.   {item.name}</Text>
                  <Text style={styles.rowItemAed}>{item.aed} AED</Text>
                </View>
                <Text style={styles.itemQty}>Qty:{item.qty}</Text>
              </View>
            )}
            ListFooterComponent={() => (
              <View style={styles.footer}>
                <View style={styles.rowFooter}>
                  <Text style={styles.rowFooterTotal}>Total</Text>
                  <Text style={[styles.rowFooterTotal, { textAlign: 'right' }]}>
                    70.98
                  </Text>
                </View>
              </View>
            )}
          />
        </ScrollView>
        <View style={styles.footers} onLayout={(e) => this.setState({ footerHeight: e.nativeEvent.layout.height })}>
          <View style={styles.footerContent}>
            <SafeAreaView forceInset={{ bottom: 'always' }}>
              <TouchableOpacity
                style={styles.btnBill}
                activeOpacity={0.7}
                onPress={() => {}}
              >
                <Text style={styles.btnBillText}>{'Done'}</Text>
              </TouchableOpacity>
            </SafeAreaView>
          </View>
        </View>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RestaurantPickUpPaySuccessScreen)
