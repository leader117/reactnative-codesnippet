import React, { Component } from 'react'
import { FlatList, Text, KeyboardAvoidingView, View, TouchableOpacity, Image } from 'react-native'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/PlateHistoryScreenStyle'
import { Images } from '../Themes'
import { Actions } from 'react-native-router-flux'

class PlateHistoryScreen extends Component {
  render () {
    const items = [
      { name: 'Limetree Cafe' },
      { name: 'P.F. Chang\'s' },
      { name: 'Al Safadi' }
    ]
    return (
      <View style={styles.container}>
        <FlatList
          style={{ flex: 1 }}
          scrollEnabled={false}
          data={items}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item, index }) => {
            return (
              <TouchableOpacity
                onPress={() => Actions.plateHistoryComment({ item })}
                style={styles.item}>
                <View style={styles.viewTop}>
                  <View style={[styles.viewInfo, { marginBottom: 4 }]}>
                    <Text style={styles.name}>{item.name}</Text>
                    <View style={{ flexDirection: 'row' }}>
                      <View style={[styles.rowHeader, { marginLeft: 10 }]}>
                        <Image source={Images.starYellow} style={styles.rowHeaderIcon} />
                        <Text style={styles.rowHeaderText}>4.6</Text>
                      </View>
                      <View style={[styles.rowHeader, { marginLeft: 10, marginRight: 9 }]}>
                        <Image source={Images.comment} style={styles.rowHeaderIcon} />
                        <Text style={styles.rowHeaderText}>5</Text>
                      </View>
                    </View>
                  </View>
                  <View style={styles.viewInfo}>
                    <Text style={styles.date}>April 10,  2018 at 14:25</Text>
                    <Text style={styles.date}>Order ID: #3213412</Text>
                  </View>
                </View>
                <View style={styles.viewBottom}>
                  <Text style={styles.total}>Total</Text>
                  <Text style={styles.total}>AED 285.00</Text>
                </View>
              </TouchableOpacity>
            )
          }}
        />
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PlateHistoryScreen)
