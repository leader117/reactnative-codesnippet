import React, { Component } from 'react'
import {
  Text,
  View,
  TextInput,
  KeyboardAvoidingView,
  TouchableOpacity,
  Image,
  TouchableWithoutFeedback,
  Keyboard,
  Platform
} from 'react-native'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/AddCardScreenStyle'
import { Images, Metrics } from '../Themes'
import Ionicons from 'react-native-vector-icons/Ionicons'
import { Actions } from 'react-native-router-flux'
import { FullButton } from '../Components'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

class LoginPasswordScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      // textWrongPw: 'Oops! Wrong password. Try again!',
      // textHeader: 'Please type your password here.',
      cardNum: '',
      expiryDate: '',
      ccvNum: '',
      holderName: ''
    }
  }

  render () {
    const { cardNum, expiryDate, ccvNum, holderName } = this.state
    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <KeyboardAwareScrollView style={styles.keyboardContainer} enabled={Platform.OS === 'ios'} keyboardVerticalOffset={70} behavior='padding'>
          <View style={styles.container}>
            <Text style={styles.txtHeader}>Please verify your card information.</Text>
            <View style={styles.middleView}>
              <View style={styles.rowInput}>
                <Text style={styles.txtLabelInput}>Credit Card Number</Text>
                <TextInput
                  style={[styles.textInput, { marginLeft: 0 }]}
                  onChangeText={(text) => this.setState({ cardNum: text })}
                  value={cardNum}
                  placeholder='Enter Card Number'
                  underlineColorAndroid={'transparent'}
                  autoCapitalize='none'
                  autoCorrect={false}
                />
              </View>
              <View style={styles.rowDateCCV}>
                <View style={[styles.colDate, { marginRight: Metrics.baseMargin }]}>
                  <Text style={styles.txtLabelInput}>Expiry Date</Text>
                  <TextInput
                    style={[styles.textInput, { marginLeft: 0 }]}
                    onChangeText={(text) => this.setState({ expiryDate: text })}
                    value={expiryDate}
                    placeholder='Enter Expiry Date'
                    underlineColorAndroid={'transparent'}
                    autoCapitalize='none'
                    autoCorrect={false}
                  />
                </View>
                <View style={[styles.colDate, { marginLeft: Metrics.baseMargin }]}>
                  <Text style={styles.txtLabelInput}>CCV</Text>
                  <TextInput
                    style={[styles.textInput, { marginLeft: 0 }]}
                    onChangeText={(text) => this.setState({ ccvNum: text })}
                    value={ccvNum}
                    placeholder='Enter CCV'
                    underlineColorAndroid={'transparent'}
                    autoCapitalize='none'
                    autoCorrect={false}
                  />
                </View>
              </View>
              <View style={[styles.rowInput, { marginTop: Metrics.doubleBaseMargin * 2 }]}>
                <Text style={styles.txtLabelInput}>Card Holder Name</Text>
                <TextInput
                  style={[styles.textInput, { marginLeft: 0 }]}
                  onChangeText={(text) => this.setState({ holderName: text })}
                  value={holderName}
                  placeholder='Enter Card Number'
                  underlineColorAndroid={'transparent'}
                  autoCapitalize='none'
                  autoCorrect={false}
                />
              </View>
            </View>
            <View style={styles.bottomView}>
              <FullButton
                styles={styles.btnNext}
                text='Next'
                onPress={Actions.root}
              />
            </View>
          </View>
        </KeyboardAwareScrollView>
      </TouchableWithoutFeedback>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginPasswordScreen)
