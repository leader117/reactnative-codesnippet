import React, { Component } from 'react'
import {
  TouchableWithoutFeedback,
  Text,
  View,
  TextInput,
  KeyboardAvoidingView,
  TouchableOpacity,
  Image,
  Keyboard,
  Platform,
  Alert
} from 'react-native'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/InputNameScreenStyle'
import { Images, Metrics } from '../Themes'
// import { Actions } from 'react-native-router-flux'
import validator from 'validator'
import firebase from 'react-native-firebase'
import { bindActionCreators } from '../Redux/Actions'
import ProfileActions from '../Redux/ProfileRedux'
// import _ from 'lodash'
// import PhoneInput from 'react-native-phone-input'
// import CountryPicker from 'react-native-country-picker-modal'

class InputNameScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      // email: firebase.auth().currentUser.email,
      email: props.email,
      first_name: '',
      last_name: '',
      phone_number: firebase.auth().currentUser.phoneNumber,
      cca2: 'US'
    }
  }

  componentDidMount () {
    // this.setState({
    //   pickerData: this.phone.getPickerData()
    // })
  }

  onPressFlag () {
    this.countryPicker.openModal()
  }

  selectCountry (country) {
    this.phone.selectCountry(country.cca2.toLowerCase())
    this.setState({ cca2: country.cca2 })
  }

  async onPressConfirm () {
    const { first_name, last_name, email } = this.state
    if (validator.isLength(first_name, { min: 2, max: 15 }) && validator.isLength(last_name, { min: 2, max: 15 })) {
      try {
        await firebase.auth().currentUser.updateProfile({
          displayName: `${first_name} ${last_name}`,
          email
        })
        this.props.updateProfile({
          user_meta: {
            first_name,
            last_name,
            email
          }
        })
      } catch (error) {
        Alert.alert(
          'Error',
          'Can not connect to server'
        )
      }
    } else {
      Alert.alert(
        'Notification',
        'First Name and Last Name field have to contains 2 to 15 characters'
      )
    }
  }

  render () {
    const { first_name, last_name } = this.state
    // const { country } = this.props
    // const initialCountry = country ? country.country_code.toLowerCase() : 'fr'
    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={styles.container}>
          <KeyboardAvoidingView style={styles.keyboardContainer} enabled={Platform.OS === 'ios'} keyboardVerticalOffset={70} behavior='padding'>
            <Text style={styles.txtHeader}>What’s your name?</Text>
            <View style={styles.middleView}>
              <View style={styles.rowInput}>
                <TextInput
                  style={[styles.textInput, { marginLeft: 0 }]}
                  onChangeText={(text) => this.setState({ first_name: text })}
                  value={first_name}
                  placeholder='First Name'
                  underlineColorAndroid={'transparent'}
                />
              </View>
              <View style={[styles.rowInput, { marginTop: Metrics.doubleBaseMargin }]}>
                <TextInput
                  style={[styles.textInput, { marginLeft: 0 }]}
                  onChangeText={(text) => this.setState({ last_name: text })}
                  value={last_name}
                  placeholder='Last Name'
                  underlineColorAndroid={'transparent'}
                />
              </View>
              {/* <View style={[styles.rowInput, { marginTop: Metrics.doubleBaseMargin }]}>
                <Text style={styles.txtConnectSocial}>Phone Number</Text>
                <View style={styles.phoneInput}>
                  <PhoneInput
                    initialCountry={initialCountry}
                    style={styles.textInput}
                    ref={(ref) => (this.phone = ref)}
                    onPressFlag={() => this.onPressFlag()}
                    // value={phoneNumber}
                    onChangePhoneNumber={(phone_number) => this.setState({ phone_number })}
                    underlineColorAndroid={'transparent'}
                  />
                  <CountryPicker
                    ref={(ref) => {
                      this.countryPicker = ref
                    }}
                    onChange={value => this.selectCountry(value)}
                    translation='eng'
                    cca2={this.state.cca2}
                  >
                    <View />
                  </CountryPicker>
                </View>
              </View> */}
              {/* <View style={[styles.rowInput, { marginTop: Metrics.doubleBaseMargin }]}>
                <Text style={styles.txtConnectSocial}>Username</Text>
                <TextInput
                  editable
                  style={[styles.textInput, { marginLeft: 0 }]}
                  value={username}
                  placeholder='Last Name'
                  underlineColorAndroid={'transparent'}
                />
              </View> */}
            </View>

            <View style={styles.bottomView}>
              {!!first_name && !!last_name && (
                <TouchableOpacity style={styles.btnArrow} onPress={() => this.onPressConfirm()}>
                  <Image source={Images.icArrowRight} style={styles.icArrowRight} />
                </TouchableOpacity>
              )}
            </View>
          </KeyboardAvoidingView>
        </View>
      </TouchableWithoutFeedback>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    ...bindActionCreators(ProfileActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(InputNameScreen)
