import React, { Component } from 'react'
import {
  Text,
  ScrollView
} from 'react-native'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/PrivacyPolicyScreenStyle'
import SafeAreaView from 'react-native-safe-area-view'

class PrivacyPolicyScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      // textWrongPw: 'Oops! Wrong password. Try again!',
      // textHeader: 'Please type your password here.',
    }
  }

  render () {
    return (
      <ScrollView style={styles.container} contentContainerStyle={{ paddingBottom: 50 }}>
        <Text style={styles.title}>Privacy Policy </Text>
        <Text style={styles.content}>By clicking “accept” below, you agree to Plate’s Terms and Conditions and acknowledge that you have read the Privacy Policy. By clicking “accept” below, you agree to Plate’s Terms and Conditions and acknowledge that you have read the Privacy Policy.By clicking “accept” below, you agree to Plate’s Terms and Conditions and acknowledge that you have read the Privacy Policy.By clicking “accept” below, you agree to Plate’s Terms and Conditions and acknowledge that you have read the Privacy Policy.By clicking “accept” below, you agree to Plate’s Terms and Conditions and acknowledge that you have read the Privacy Policy.By clicking “accept” below, you agree to Plate’s Terms and Conditions and acknowledge that you have read the Privacy Policy.By clicking “accept” below, you agree to Plate’s Terms and Conditions and acknowledge that you have read the Privacy Policy.By clicking “accept” below, you agree to Plate’s Terms and Conditions and acknowledge that you have read the Privacy Policy.By clicking “accept” below, you agree to Plate’s Terms and Conditions and acknowledge that you have read the Privacy Policy.By clicking “accept” below, you agree to Plate’s Terms and Conditions and acknowledge that you have read the Privacy Policy.By clicking “accept” below, you agree to Plate’s Terms and Conditions and acknowledge that you have read the Privacy Policy.By clicking “accept” below, you agree to Plate’s Terms and Conditions and acknowledge that you have read the Privacy Policy.By clicking “accept” below, you agree to Plate’s Terms and Conditions and acknowledge that you have read the Privacy Policy.By clicking “accept” below, you agree to Plate’s Terms and Conditions and acknowledge that you have read the Privacy Policy.</Text>
        <SafeAreaView forceInset={{ bottom: 'always' }} />
      </ScrollView>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PrivacyPolicyScreen)
