import { StyleSheet } from 'react-native'
import { ApplicationStyles, Fonts, Metrics } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  keyboardContainer: {
    flex: 1,
    backgroundColor: '#f2f2f2'
  },
  viewTitle: {
    width: Metrics.screenWidth,
    height: 57,
    justifyContent: 'center',
    paddingLeft: 20,
    backgroundColor: '#FFFFFF',
    marginBottom: 8
  },
  title: {
    color: '#bdbdbd',
    fontFamily: Fonts.type.avenirRoman,
    fontSize: 18
  },
  viewText: {
    flex: 1,
    backgroundColor: '#f7f8fa',
    padding: 20
  },
  viewTextInput: {
    flex: 1
  },
  textInput: {
    color: '#0f1524',
    fontFamily: Fonts.type.avenirRoman,
    fontSize: 18
  },
  detail: {
    color: '#9b9b9b',
    fontFamily: Fonts.type.avenirBookOblique,
    fontSize: 12,
    textAlign: 'center',
    marginBottom: 41
  },
  email: {
    color: '#9b9b9b',
    fontFamily: Fonts.type.avenirMediumOblique,
    fontSize: 12
  },
  buttonSend: {
    width: Metrics.screenWidth - 60,
    marginHorizontal: 10,
    height: 42,
    borderRadius: 21,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#0f1524'
  },
  send: {
    fontFamily: Fonts.type.avenirHeavy,
    fontSize: 14,
    color: '#FFFFFF'
  }
})
