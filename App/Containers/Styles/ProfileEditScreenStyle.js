import { StyleSheet } from 'react-native'
import { ApplicationStyles, Fonts, Colors, Metrics } from '../../Themes/'

export const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    flex: 1,
    fontFamily: Fonts.type.avenirRoman,
    fontSize: Fonts.size.medium,
    paddingTop: 10,
    color: '#0F1524'
  }
})

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  txtSkip: {
    color: '#555',
    fontSize: 14
  },
  container: {
    backgroundColor: Colors.backgroundGrey
  },
  cardAva: {
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 0
    // alignSelf: 'center'
  },
  icAvaMan: {
    height: 120,
    width: 120,
    borderRadius: 60,
    borderColor: Colors.transparent
  },
  txtChangePF: {
    color: '#FF3600',
    fontFamily: Fonts.type.avenirRoman,
    fontSize: Fonts.size.small,
    marginVertical: Metrics.baseMargin,
    marginLeft: 5
  },
  cardName: {
    marginTop: 5,
    borderRadius: 0,
    paddingHorizontal: Metrics.doubleBaseMargin
  },
  rowCardName: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: Metrics.smallMargin
  },
  labelCardName: {
    fontFamily: Fonts.type.avenirRoman,
    fontSize: Fonts.size.medium,
    color: Colors.textGrey,
    flex: 2
  },
  inputCardName: {
    height: 40,
    borderBottomWidth: 1,
    borderBottomColor: Colors.frost,
    width: '100%',
    flex: 3,
    fontFamily: Fonts.type.avenirRoman,
    fontSize: Fonts.size.medium
  },
  rowValue: {
    fontFamily: Fonts.type.avenirRoman,
    fontSize: Fonts.size.medium
  },
  inputWrapText: {
    flex: 1,
    fontFamily: Fonts.type.avenirRoman,
    fontSize: Fonts.size.medium
  },
  inputWrap: {
    height: 40,
    borderBottomWidth: 1,
    borderBottomColor: Colors.frost,
    flex: 3,
    flexDirection: 'row',
    alignItems: 'center'
  },
  privateHeader: {
    color: '#0F1524',
    fontFamily: Fonts.type.avenirHeavy,
    fontSize: Fonts.size.medium,
    marginVertical: Metrics.baseMargin
  },
  title: {
    color: '#0f1524',
    fontFamily: Fonts.type.avenirHeavy,
    fontSize: 14
  },
  rowItem: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 60
  },
  iconSocial: {
    height: 18,
    width: 18
  },
  txtSocialName: {
    marginLeft: Metrics.doubleBaseMargin,
    fontFamily: Fonts.type.avenirRoman,
    fontSize: Fonts.size.input,
    flex: 1,
    color: '#292D44'
  },
  txtSocialStatus: {
    marginLeft: Metrics.doubleBaseMargin,
    fontFamily: Fonts.type.avenirRoman,
    fontSize: Fonts.size.small,
    color: Colors.active,
    margin: 5
  }
})
