import { StyleSheet } from 'react-native'
import { ApplicationStyles, Metrics, Fonts, Colors } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    backgroundColor: Colors.white
  },
  cardStyle: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: '#F2F2F2',
    paddingTop: 16,
    paddingBottom: 24
  },
  icAva: {
    height: 60,
    width: 60,
    borderRadius: 30,
    borderColor: Colors.transparent,
    marginRight: Metrics.doubleBaseMargin
  },
  rowInfo: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    flex: 1
  },
  txtName: {
    fontFamily: Fonts.type.avenirMedium,
    fontSize: Fonts.size.sixteen
  },
  txtTime: {
    fontFamily: Fonts.type.avenirRoman,
    fontSize: Fonts.size.small,
    color: '#636770'
  },
  btnCard: {
    borderWidth: 1,
    padding: Metrics.smallMargin,
    borderRadius: 30,
    width: 120,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#FF4D0D'
  },
  txtBtn: {
    color: '#FF4D0D',
    fontFamily: Fonts.type.avenirRoman
  },
  title: {
    fontFamily: Fonts.type.avenirRoman,
    fontSize: 16,
    color: '#0f1524',
    marginLeft: 20,
    marginTop: 16
  }
})
