import { StyleSheet } from 'react-native'
import { ApplicationStyles, Metrics, Fonts, Colors } from '../../Themes'
export const cardWidth = (Metrics.screenWidth * 0.9)
export const cardHeight = cardWidth * 0.6

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    flex: 1,
    backgroundColor: Colors.snow
  },
  backgroundImage: {
    width: Metrics.screenWidth,
    height: Metrics.screenHeight,
    flex: 1
  },
  listCard: {
    flex: 1,
    paddingTop: 20
  },
  footer: {},
  rowFooter: {
    flexDirection: 'row',
    alignItems: 'center',
    borderTopColor: '#D8D8D8',
    borderTopWidth: 1,
    height: 60,
    paddingHorizontal: 20
  },
  rowFooterIcon: {
    width: 34,
    height: 34,
    alignItems: 'center',
    justifyContent: 'center'
  },
  rowFooterText: {
    fontFamily: Fonts.type.avenirRoman,
    color: '#0F1524',
    fontSize: 18,
    lineHeight: 25,
    flex: 1,
    marginLeft: 10
  },
  rightButtonTextStyle: {
    fontFamily: Fonts.type.avenirRoman,
    color: '#BDBDBD',
    fontSize: 14,
    lineHeight: 19
  },
  rowItem: {
    height: 70,
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: '#F2F2F2',
    paddingRight: 17
  },
  rowItemLeft: {
    flex: 1,
    paddingLeft: 17
  },
  cardName: {
    fontFamily: Fonts.type.avenirRoman,
    color: '#0F1524',
    fontSize: 18,
    lineHeight: 25
  },
  cardLast4: {
    fontFamily: Fonts.type.avenirRoman,
    color: '#636770',
    fontSize: 14,
    lineHeight: 19
  },
  btnDelete: {
    paddingHorizontal: 17,
    paddingVertical: 10
  },
  unpaidContent: {
    backgroundColor: '#FAFAFA',
    padding: 20
  },
  unpaidContentRow: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  unpaidTitle: {
    fontFamily: Fonts.type.avenirRoman,
    fontSize: 18,
    color: '#0F1524',
    lineHeight: 20,
    marginRight: 10
  },
  unpaidInfo: {
    fontFamily: Fonts.type.avenirBookOblique,
    fontSize: 14,
    color: '#9B9B9B',
    lineHeight: 20,
    marginRight: 10,
    marginTop: 10,
    marginBottom: 5
  },
  unpaidPrice: {
    fontFamily: Fonts.type.avenirHeavy,
    fontSize: 18,
    color: '#FF4D0D',
    lineHeight: 20,
    marginRight: 10,
    marginTop: 10,
    flex: 1
  },
  btnPay: {
    width: 168,
    height: 42,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 21,
    backgroundColor: '#0F1524'
  },
  btnPayText: {
    fontFamily: Fonts.type.avenirHeavy,
    fontSize: 14,
    color: Colors.white,
    lineHeight: 20
  },
  footerPaySuccess: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: Colors.white,
    maxHeight: Metrics.screenHeight - Metrics.navBarHeight - 20,
    borderWidth: 1,
    // shadowOpacity: 1,
    // elevation: 1,
    borderColor: Colors.backgroundGrey,
    // height: 108,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    paddingVertical: 15,
    elevation: 4,
    shadowOffset: { width: 5, height: 14 },
    shadowColor: 'grey',
    shadowOpacity: 0.5,
    shadowRadius: 10,
    padding: 20
  },
  modalTitle: {
    fontFamily: Fonts.type.avenirMedium,
    fontSize: 20,
    color: '#292D44',
    lineHeight: 25
  },
  modalInfo: {
    fontFamily: Fonts.type.avenirRoman,
    fontSize: 16,
    color: '#292D44',
    lineHeight: 24
  },
  btnDone: {
    height: 42,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 21,
    backgroundColor: '#3CCFA9',
    marginTop: 30
  },
  btnDoneText: {
    fontFamily: Fonts.type.avenirHeavy,
    fontSize: 14,
    color: Colors.white,
    lineHeight: 20
  }
})
