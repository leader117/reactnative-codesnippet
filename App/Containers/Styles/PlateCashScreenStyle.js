import { StyleSheet } from 'react-native'
import { ApplicationStyles, Colors, Fonts } from '../../Themes/'

export default StyleSheet.create({
  container: {
    backgroundColor: Colors.white
  },
  title: {
    fontFamily: Fonts.type.avenirRoman,
    fontSize: 18,
    color: '#0f1524',
    marginLeft: 20,
    marginTop: 16,
    marginBottom: 28
  },
  icWallet: {
    width: 72,
    height: 72,
    alignSelf: 'center',
    marginBottom: 20
  },
  viewMoney: {
    paddingVertical: 10,
    alignSelf: 'center'
  },
  money: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  label: {
    color: '#ff4d0d',
    fontFamily: Fonts.type.avenirLight,
    fontSize: 20,
    marginBottom: -13,
    marginTop: -20,
    textDecorationLine: 'underline'
  },
  number: {
    color: '#ff4d0d',
    fontFamily: Fonts.type.avenirHeavy,
    fontSize: 48
  },
  viewLine: {
    width: 172,
    height: 3,
    backgroundColor: '#ff4d0d'
  },
  viewDetail: {
    paddingTop: 5,
    alignSelf: 'center'
  },
  detail: {
    fontFamily: Fonts.type.avenirRoman,
    fontSize: 16,
    color: '#0f1524',
    textAlign: 'center'
  },
  date: {
    fontFamily: Fonts.type.avenirBookOblique,
    fontSize: 14,
    color: '#9b9b9b',
    textAlign: 'center'
  }
})
