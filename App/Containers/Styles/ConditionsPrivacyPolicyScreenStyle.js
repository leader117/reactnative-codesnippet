import { StyleSheet } from 'react-native'
import { ApplicationStyles, Colors, Metrics, Fonts } from '../../Themes'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    backgroundColor: Colors.white,
    flex: 1,
    padding: Metrics.doubleBaseMargin
  },
  containerInner: {
    flex: 1,
    borderWidth: 1,
    borderColor: 'green',
    position: 'relative'
  },
  txtHeader: {
    marginTop: Metrics.screenHeight * 0.05,
    fontFamily: Fonts.type.avenirRoman,
    fontSize: 24
  },
  paymentApproved: {
    alignSelf: 'center',
    marginVertical: 30
  },
  squareView: {
    backgroundColor: Colors.white,
    marginTop: 30,
    flex: 1
  },
  txtPlaceHolder: {
    fontSize: 24,
    fontFamily: Fonts.type.avenirRoman
  },
  btnAddPayment: {
    borderRadius: 100,
    height: 42,
    width: 315,
    alignSelf: 'center',
    marginTop: Metrics.doubleBaseMargin * 2,
    backgroundColor: Colors.primaryColor
  },
  btnSkip: {
    marginTop: Metrics.doubleBaseMargin,
    justifyContent: 'center',
    alignItems: 'center'
  },
  txtSkip: {
    textAlign: 'center',
    fontSize: Fonts.size.input,
    fontFamily: Fonts.type.avenirRoman,
    color: '#9B9B9B'
  },
  txtPolicy: {
    marginTop: 20,
    color: '#9B9B9B',
    fontSize: 14,
    fontFamily: Fonts.type.avenirRoman,
    lineHeight: 19
  },
  btnTxtPolicy: {
    color: Colors.active,
    fontSize: 14,
    fontFamily: Fonts.type.avenirRoman,
    lineHeight: 19
  }
})
