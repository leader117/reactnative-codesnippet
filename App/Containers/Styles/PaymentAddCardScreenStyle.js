import { StyleSheet } from 'react-native'
import { ApplicationStyles, Metrics, Fonts, Colors } from '../../Themes'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    flex: 1,
    backgroundColor: Colors.snow
  },
  title: {
    fontSize: 18,
    color: '#161E34',
    margin: 20,
    fontFamily: Fonts.type.avenirRoman
  },
  form: {
    marginVertical: 30,
    paddingHorizontal: 20
  },
  formRow: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 30
  },
  btn: {
    height: 42,
    marginHorizontal: 30,
    marginVertical: 15,
    backgroundColor: '#0F1524',
    borderRadius: 21,
    alignItems: 'center',
    justifyContent: 'center'
  },
  btnText: {
    color: Colors.snow,
    fontSize: 14,
    fontFamily: Fonts.type.avenirHeavy
  }
})
