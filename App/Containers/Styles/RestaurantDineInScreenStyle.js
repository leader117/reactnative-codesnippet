import { StyleSheet } from 'react-native'
import { ApplicationStyles, Metrics, Fonts, Colors } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF'
  },
  rightIcon: {
    width: 24,
    height: 15
  },
  cardStyle: {
    backgroundColor: Colors.white,
    borderBottomWidth: 1,
    borderBottomColor: '#F2F2F2',
    paddingHorizontal: 0,
    paddingTop: 10,
    borderRadius: 0
  },
  borderShadow: {
    borderBottomWidth: 1,
    shadowOpacity: 1,
    elevation: 1,
    borderBottomColor: Colors.backgroundGrey
  },
  scrollView: {
  },
  content: {
    paddingHorizontal: 20
  },
  contentRowUser: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 15
  },
  contentRowUserAvatar: {
    width: 58,
    height: 58,
    borderRadius: 29,
    borderWidth: 2,
    borderColor: Colors.white,
    marginLeft: -10
  },
  contentRowUserBtn: {
    width: 58,
    height: 58,
    borderRadius: 29,
    borderWidth: 2,
    borderColor: Colors.white,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F1F3F7',
    marginLeft: -10
  },
  rowUserText: {
    marginTop: 10,
    color: '#636770',
    fontFamily: Fonts.type.avenirMedium,
    fontSize: 14
  },
  rowTable: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  rowTableText: {
    color: '#0F1524',
    fontSize: 16,
    fontFamily: Fonts.type.avenirRoman
  },
  rowTableTextBold: {
    color: '#0F1524',
    fontSize: 16,
    fontFamily: Fonts.type.avenirHeavy
  },
  itemWrap: {
    paddingHorizontal: 20,
    justifyContent: 'center',
    borderBottomWidth: 1,
    borderBottomColor: Colors.backgroundGrey,
    backgroundColor: '#FAFAFA',
    height: 66
  },
  rowItem: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  rowItemName: {
    color: '#0F1524',
    fontSize: 16,
    fontFamily: Fonts.type.avenirRoman
  },
  rowHeaderIcon: {
    width: 11,
    height: 11
  },
  rowHeaderText: {
    fontSize: 12,
    color: '#161E34',
    fontFamily: Fonts.type.avenirHeavy,
    marginHorizontal: 5
  },
  rowItemPrice: {
    fontSize: 12,
    fontFamily: Fonts.type.avenirHeavy,
    color: '#0F1524'
  },
  textNumber: {
    fontFamily: Fonts.type.avenirLight,
    fontSize: 16,
    color: '#737373',
    marginRight: 10
  },
  rowItemStatus: {
    fontSize: 12,
    fontFamily: Fonts.type.avenirMedium,
    color: '#636770',
    marginTop: 3
  },
  btnBill: {
    height: 42,
    borderRadius: 30,
    marginHorizontal: 25,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#0F1524',
    marginVertical: 10
  },
  btnBillText: {
    fontFamily: Fonts.type.avenirHeavy,
    fontSize: 14,
    color: Colors.white
  },
  bottomModal: {
    justifyContent: 'flex-end',
    margin: 0
  },
  modalContent: {
    backgroundColor: Colors.white,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15
  },
  modalRowHeader: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  modalRowHeaderText: {
    fontSize: 20,
    fontFamily: Fonts.type.avenirMedium,
    color: '#292D44',
    flex: 1,
    marginLeft: 20
  },
  txtContent: {
    marginHorizontal: 20,
    fontFamily: Fonts.type.avenirRoman,
    fontSize: 16,
    color: '#0F1524'
  },
  rowContent: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 20,
    marginTop: 10,
    height: 42,
    justifyContent: 'center'
  },
  rowContentLabel: {
    color: '#222D4F',
    fontFamily: Fonts.type.avenirRoman,
    fontSize: 14,
    flex: 1
  },
  rowContentTime: {
    color: '#222D4F',
    fontFamily: Fonts.type.avenirMedium,
    fontSize: 14
  },
  totalWrap: {
    height: 40,
    borderRadius: 30,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.active,
    paddingHorizontal: 25
  },
  btnCancel: {
    height: 42,
    borderRadius: 30,
    marginHorizontal: 25,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ECEFF4',
    marginVertical: 20,
    marginTop: 50
  },
  btnCancelText: {
    fontFamily: Fonts.type.avenirHeavy,
    fontSize: 14,
    color: '#636770'
  },
  footer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: Colors.white
  },
  footerContent: {
    borderWidth: 1,
    borderColor: '#F2F2F2',
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    paddingVertical: 5,
    elevation: 0,
    shadowOffset: { width: 5, height: 4 },
    shadowColor: 'grey',
    shadowOpacity: 0.5,
    shadowRadius: 10

  },
  rowActions: {
    flexDirection: 'row',
    paddingHorizontal: 20,
    alignItems: 'center',
    marginVertical: 20
  },
  rowActionsWrap: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  rowActionsIcon: {
    width: 42,
    height: 42
  },
  rowActionText: {
    color: '#0F1524',
    fontFamily: Fonts.type.avenirRoman,
    fontSize: 14,
    textAlign: 'center'
  },
  radioIcon: {
    width: 14,
    height: 14,
    marginRight: 8
  },
  rowCard: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 20,
    marginTop: 15
  },
  rowCardText: {
    fontFamily: Fonts.type.avenirRoman,
    fontSize: 14,
    color: '#0F1524'
  },
  rowCardTextNumber: {
    fontFamily: Fonts.type.avenirRoman,
    fontSize: 14,
    color: '#0F1524',
    marginHorizontal: 20,
    marginTop: 10
  },
  header: {
    justifyContent: 'center',
    paddingVertical: 20,
    borderBottomWidth: 1,
    borderBottomColor: '#F2F2F2'
  },
  headerTitle: {
    color: '#636770',
    fontSize: 14,
    lineHeight: 18,
    fontFamily: Fonts.type.avenirRoman,
    marginHorizontal: 20,
    marginTop: 15
  },
  rowUser: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 40,
    paddingRight: 20
  },
  avatar: {
    width: 68,
    height: 68,
    borderRadius: 34,
    borderWidth: 2,
    borderColor: '#FFFFFF',
    backgroundColor: '#F1F3F7',
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: -20
  }
})
