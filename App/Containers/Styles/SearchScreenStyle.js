import { StyleSheet } from 'react-native'
import { ApplicationStyles, Colors, Metrics, Fonts } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    backgroundColor: '#F2F2F2',
    flex: 1
  },
  textInput: {
    height: 40,
    width: '100%'
  },
  icSearch: {
    height: 18,
    width: 18,
    marginRight: Metrics.baseMargin
  },
  cardSearch: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 62,
    paddingHorizontal: Metrics.doubleBaseMargin
  },
  cardBody: {
    marginTop: Metrics.baseMargin
  },
  rowPopularHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 10,
    marginTop: 15
  },
  txtPopular: {
    fontFamily: Fonts.type.avenirMedium,
    fontSize: Fonts.size.input
  },
  icStar: {
    height: 20,
    width: 20,
    marginRight: Metrics.baseMargin
  },
  rowStar: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#FAFAFA',
    height: 50,
    paddingHorizontal: Metrics.baseMargin,
    marginTop: Metrics.smallMargin
  },
  txtContentStar: {
    fontFamily: Fonts.type.avenirRoman,
    fontSize: Fonts.size.medium,
    color: '#0F1524'
  },
  txtHeaderTrend: {
    fontFamily: Fonts.type.avenirHeavy,
    fontSize: Fonts.size.input,
    marginVertical: Metrics.doubleBaseMargin,
    marginLeft: Metrics.doubleBaseMargin
  },
  icTrend: {
    height: 8,
    width: 16,
    marginRight: Metrics.baseMargin
  },
  rowTrend: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: Metrics.doubleBaseMargin
  },
  cardItemTrend: {
    marginTop: 2
  },
  rowCard: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  rowCardTitle: {
    ...Fonts.style.normal,
    fontSize: 12,
    color: '#9B9B9B',
    fontFamily: Fonts.type.avenirLight
  },
  btnType: {
    width: 64,
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginHorizontal: 35,
    marginVertical: 5
  },
  btnTypeText: {
    fontSize: 16,
    color: '#9B9B9B',
    fontFamily: Fonts.type.avenirRoman
  },
  btnTypeTextActive: {
    fontSize: 16,
    color: '#FF4D0D',
    fontFamily: Fonts.type.avenirHeavy
  },
  btnFilter: {
    minWidth: 80,
    height: 35,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 30,
    marginRight: 10,
    backgroundColor: Colors.active
  },
  btnFilterText: {
    ...Fonts.style.normal,
    fontSize: 14,
    color: '#ffffff',
    fontFamily: Fonts.type.avenirRoman
  },
  line: {
    width: 1,
    backgroundColor: '#9B9B9B',
    height: '100%'
  },
  listResult: {
    backgroundColor: '#FAFAFB',
    flex: 1,
    paddingHorizontal: 20,
    marginTop: 10
  },
  cardLocation: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    padding: 10,
    marginTop: 8
  },
  txtAddress: {
    fontFamily: Fonts.type.avenirRoman,
    fontSize: Fonts.size.medium,
    color: '#0F1524',
    marginRight: 6
  },
  txtPlace: {
    fontFamily: Fonts.type.avenirRoman,
    fontSize: Fonts.size.medium,
    color: '#0F1524'
  },
  lineActive: {
    backgroundColor: '#FF4D0D',
    height: 3,
    borderRadius: 1.5,
    width: 64,
    marginTop: 1
  },
  restaurantImage: {
    width: 40,
    height: 40,
    borderRadius: 20,
    marginRight: 10
  },
  percent: {
    fontFamily: Fonts.type.avenirMedium,
    fontSize: 10,
    color: '#FFFFFF'
  },
  viewPercent: {
    width: 30,
    height: 17,
    borderRadius: 9,
    backgroundColor: '#ff4d0d',
    marginRight: 20,
    justifyContent: 'center',
    alignItems: 'center'
  }
})
