import { StyleSheet } from 'react-native'
import { ApplicationStyles, Colors, Metrics, Fonts } from '../../Themes'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    backgroundColor: Colors.white,
    flex: 1,
    padding: Metrics.doubleBaseMargin
  },
  title: {
    fontSize: 24,
    fontFamily: Fonts.type.avenirRoman,
    color: '#292D44',
    lineHeight: 33
  },
  content: {
    marginTop: 20,
    fontSize: 14,
    fontFamily: Fonts.type.avenirRoman,
    color: '#292D44',
    lineHeight: 19
  }
})
