import { StyleSheet } from 'react-native'
import { ApplicationStyles, Metrics, Fonts, Colors } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF'
  },
  scrollView: {
    paddingVertical: 20
  },
  content: {
    marginHorizontal: 20
  },
  itemImage: {
    width: Metrics.screenWidth - 40,
    height: (Metrics.screenWidth - 40) * 0.65
  },
  itemName: {
    fontFamily: Fonts.type.avenirRoman,
    fontSize: 18,
    color: '#0F1524',
    marginTop: 15,
    marginBottom: 10
  },
  itemDescription: {
    color: '#BDBDBD',
    fontSize: 14,
    fontFamily: Fonts.type.avenirRoman
  },
  txtReadMore: {
    fontFamily: Fonts.type.avenirRoman,
    fontSize: 14,
    color: '#0F1524'
  },
  rowItem: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  rowItemIcon: {
    width: 16,
    height: 16
  },
  rowItemText: {
    fontSize: 14,
    color: '#161E34',
    fontFamily: Fonts.type.avenirHeavy,
    marginHorizontal: 5
  },
  itemRowCommentText: {
    fontFamily: Fonts.type.avenirRoman,
    fontSize: 14,
    color: '#0F1524',
    marginHorizontal: 5
  },
  rowItemPrice: {
    fontFamily: Fonts.type.avenirRoman,
    color: '#0F1524',
    fontSize: 16
  },
  listTitle: {
    fontFamily: Fonts.type.avenirRoman,
    color: '#0F1524',
    fontSize: 16,
    marginTop: 15,
    marginBottom: 5
  },
  radioIcon: {
    width: 14,
    height: 14
  },
  listLabel: {
    color: '#0F1524',
    fontSize: 14,
    fontFamily: Fonts.type.avenirRoman,
    flex: 1,
    marginHorizontal: 10
  },
  listPrice: {
    color: '#0F1524',
    fontSize: 14,
    fontFamily: Fonts.type.avenirRoman
  },
  inputWrap: {
    backgroundColor: '#F6F9FD',
    padding: 10,
    borderRadius: 5
  },
  input: {
    textAlignVertical: 'top'
  },
  btnOrder: {
    backgroundColor: '#3CCFA9',
    borderRadius: 30,
    height: 42,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 20,
    marginHorizontal: 25
  },
  btnOrderText: {
    fontFamily: Fonts.type.avenirHeavy,
    fontSize: 14,
    color: Colors.white
  },
  footer: {
    backgroundColor: Colors.white,
    borderWidth: 1,
    // shadowOpacity: 1,
    // elevation: 1,
    borderColor: Colors.backgroundGrey,
    // height: 108,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    elevation: 4,
    shadowOffset: { width: 5, height: 14 },
    shadowColor: 'grey',
    shadowOpacity: 0.5,
    shadowRadius: 10,
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0
  }
})
