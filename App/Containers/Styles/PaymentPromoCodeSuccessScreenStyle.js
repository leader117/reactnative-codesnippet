import { StyleSheet } from 'react-native'
import { Fonts, Colors } from '../../Themes'

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.snow,
    paddingTop: 20
  },
  imageCheck: {
    width: 80,
    height: 80,
    alignSelf: 'center'
  },
  txtSuccess: {
    fontSize: 18,
    fontFamily: Fonts.type.avenirHeavy,
    color: '#86BC27',
    lineHeight: 25,
    marginTop: 5,
    textAlign: 'center'
  },
  txtInfo: {
    fontSize: 14,
    fontFamily: Fonts.type.avenirMedium,
    color: '#636770',
    lineHeight: 19,
    marginTop: 15,
    textAlign: 'center'
  },
  txtCode: {
    fontSize: 52,
    fontFamily: Fonts.type.avenirHeavy,
    color: '#0F1524',
    lineHeight: 71,
    marginTop: 25,
    textAlign: 'center'
  },
  btn: {
    height: 42,
    marginHorizontal: 30,
    marginVertical: 15,
    backgroundColor: '#0F1524',
    borderRadius: 21,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20
  },
  btnText: {
    color: Colors.snow,
    fontSize: 14,
    fontFamily: Fonts.type.avenirHeavy
  }
})
