import { StyleSheet } from 'react-native'
import { ApplicationStyles, Colors, Fonts, Metrics } from '../../Themes/'

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF'
  },
  cardDetail: {
    borderRadius: 0,
    paddingHorizontal: 20,
    flexDirection: 'row',
    paddingBottom: 23,
    paddingTop: 0,
    backgroundColor: '#FFFFFF',
    borderBottomWidth: 1,
    borderBottomColor: '#cccccc',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.06,
    shadowRadius: 5,
    elevation: 2
  },
  icAvaMan: {
    height: 82,
    width: 82,
    borderRadius: 41,
    borderColor: Colors.transparent,
    marginRight: 15
  },
  name: {
    fontSize: 22,
    fontFamily: Fonts.type.avenirHeavy,
    color: '#0f1524'
  },
  viewLocation: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  location: {
    color: '#bdbdbd',
    fontFamily: Fonts.type.avenirRoman,
    fontSize: 14,
    marginLeft: 8.4
  },
  buttonEdit: {
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 4,
    borderWidth: 1,
    borderColor: '#bdbdbd',
    width: 113,
    height: 28,
    marginRight: 24
  },
  edit: {
    color: '#000000',
    fontFamily: Fonts.type.avenirRoman,
    fontSize: 12
  },
  title: {
    fontFamily: Fonts.type.avenirRoman,
    fontSize: 18,
    color: '#0f1524',
    marginBottom: 14
  },
  cardPlate: {
    marginTop: 20,
    borderRadius: 0,
    paddingHorizontal: 20,
    // paddingTop: 16,
    paddingBottom: 22,
    marginBottom: 5,
    backgroundColor: '#FFFFFF'
  },
  viewMoney: {
    marginRight: 23.5
  },
  label: {
    color: '#ff4d0d',
    fontFamily: Fonts.type.avenirLight,
    fontSize: 10,
    marginBottom: -8,
    marginTop: -12,
    textDecorationLine: 'underline'
  },
  number: {
    color: '#ff4d0d',
    fontFamily: Fonts.type.avenirHeavy,
    fontSize: 30
  },
  viewLine: {
    width: 103,
    height: 2,
    backgroundColor: '#ff4d0d'
  },
  viewDetail: {
    flex: 1
  },
  detail: {
    fontFamily: Fonts.type.avenirRoman,
    fontSize: 14,
    color: '#0f1524'
  },
  date: {
    fontFamily: Fonts.type.avenirMediumOblique,
    fontSize: 12,
    color: '#9b9b9b',
    marginRight: 24
  },
  line: {
    height: 5,
    width: Metrics.screenWidth,
    backgroundColor: Colors.backgroundGrey
  },
  cardButton: {
    flex: 1,
    backgroundColor: '#FFFFFF'
    // paddingHorizontal: 20
  },
  button: {
    width: Metrics.screenWidth,
    height: 49,
    paddingHorizontal: 20,
    borderBottomWidth: 1,
    borderBottomColor: '#f2f2f2',
    justifyContent: 'center'
  },
  textButton: {
    fontFamily: Fonts.type.avenirRoman,
    fontSize: 18,
    color: '#0f1524'
  },
  buttonArrow: {
    width: Metrics.screenWidth,
    height: 45,
    alignItems: 'center',
    paddingHorizontal: 20,
    borderBottomWidth: 1,
    borderBottomColor: '#f2f2f2',
    justifyContent: 'space-between',
    flexDirection: 'row'
  },
  textButtonArrow: {
    fontFamily: Fonts.type.avenirMedium,
    fontSize: 14,
    color: '#0f1524'
  }
})
