import { StyleSheet } from 'react-native'
import { ApplicationStyles, Colors, Metrics, Fonts } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    backgroundColor: Colors.white,
    flex: 1,
    padding: Metrics.doubleBaseMargin
  },
  containerInner: {
    flex: 1,
    borderWidth: 1,
    borderColor: 'green',
    position: 'relative'
  },
  txtHeader: {
    marginTop: Metrics.screenHeight * 0.05,
    fontFamily: Fonts.type.avenirRoman,
    fontSize: 24
  },
  squareView: {
    height: 278,
    width: 293,
    // borderWidth: 1,
    // borderColor: Colors.textPlace,
    backgroundColor: Colors.white,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    marginTop: 30
  },
  txtPlaceHolder: {
    textAlign: 'center',
    fontSize: 24,
    fontFamily: Fonts.type.avenirRoman
  },
  btnAddPayment: {
    borderRadius: 100,
    height: 42,
    width: 315,
    alignSelf: 'center',
    marginTop: Metrics.doubleBaseMargin * 2
  },
  btnSkip: {
    marginTop: Metrics.doubleBaseMargin,
    justifyContent: 'center',
    alignItems: 'center'
  },
  txtSkip: {
    textAlign: 'center',
    fontSize: Fonts.size.input,
    fontFamily: Fonts.type.avenirRoman,
    color: '#9B9B9B'
  }
})
