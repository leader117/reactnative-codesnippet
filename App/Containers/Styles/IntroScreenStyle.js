import { StyleSheet } from 'react-native'
import { ApplicationStyles, Metrics, Colors, Fonts } from '../../Themes/'

// const height = Metrics.screenHeight - Metrics.navBarHeight - 150

export default StyleSheet.create({
  // ...ApplicationStyles.screen,
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  lottie: {
    width: Metrics.screenWidth - 40,
    flex: 1,
    alignSelf: 'center'
  },
  containerIntro: {
    marginTop: 20,
    height: 120
    // backgroundColor: 'rgb(243,243,243)'
  },
  wrapper: {
    // height: 200
    // flex: 1
    // width: Metrics.screenWidth
  },
  containerIntroView: {
    width: Metrics.screenWidth * 0.8
    // height: Metrics.screenWidth
    // flexDirection: 'column'
  },
  warpDesc: {
    width: Metrics.screenWidth * 0.75,
    height: 64,
    borderRadius: 4,
    backgroundColor: 'rgb(243,243,243)',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center'
  },
  txtDescription: {
    textAlign: 'center',
    fontSize: 14,
    fontFamily: Fonts.type.avenirRoman,
    alignSelf: 'center',
    marginHorizontal: 15
  },
  btnStart: {
    backgroundColor: '#FF4D0D',
    marginBottom: 10,
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    width: '80%',
    borderRadius: 20,
    alignSelf: 'center'
  },
  txtBtnGetStart: {
    color: Colors.white,
    fontFamily: Fonts.type.avenirHeavy,
    fontSize: Fonts.size.medium
  },
  bottomView: {
    alignItems: 'center',
    marginBottom: 20
  },
  loginButton: {
    width: 100
  },
  loginText: {
    textAlign: 'center',
    color: 'rgb(15, 21, 36)',
    fontSize: 26,
    fontFamily: Fonts.type.avenirMedium,
    textDecorationLine: 'underline'
  },
  startWithPlate: {
    color: 'rgb(15, 21, 36)',
    fontSize: 20,
    fontFamily: Fonts.type.avenirBookOblique
  },
  plateText: {
    fontFamily: Fonts.type.avenirRoman,
    color: 'rgb(255, 77, 13)'
  },
  btnSkip: {
    marginTop: 20,
    marginBottom: 40
  },
  textSkip: {
    color: 'rgb(155, 155, 155)',
    fontSize: 14,
    fontFamily: Fonts.type.avenirRoman
  },
  rowPagination: {
    flexDirection: 'row',
    alignSelf: 'center',
    bottom: 30,
    height: 6,
    width: 36
  },
  pagination: {
    width: 6,
    height: 6,
    backgroundColor: '#D8D8D8',
    borderRadius: 3,
    marginLeft: 3
  }
})
