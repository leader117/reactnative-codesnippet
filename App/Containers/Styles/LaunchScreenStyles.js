import { StyleSheet } from 'react-native'
import { Metrics, ApplicationStyles, Colors, Fonts } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    // paddingBottom: Metrics.baseMargin,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  background3: {
    backgroundColor: '#FF4D0D',
    width: Metrics.screenWidth,
    position: 'absolute',
    bottom: 0
  },
  background2: {
    backgroundColor: '#FF9600',
    width: Metrics.screenWidth,
    position: 'absolute',
    bottom: 0
  },
  background1: {
    backgroundColor: '#FFFFFF',
    width: Metrics.screenWidth,
    position: 'absolute',
    bottom: 0
  },
  title: {
    fontFamily: Fonts.type.mohrRoundedLight,
    fontSize: 68,
    // fontWeight: '500',
    // alignSelf: 'center',
    // marginTop: Metrics.screenHeight * 0.25,
    color: '#000000'
  },
  logo: {
    marginTop: Metrics.doubleSection,
    height: Metrics.images.logo,
    width: Metrics.images.logo,
    resizeMode: 'contain'
  },
  centered: {
    alignItems: 'center'
  }
})
