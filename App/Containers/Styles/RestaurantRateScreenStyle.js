import { StyleSheet } from 'react-native'
import { ApplicationStyles, Metrics, Fonts, Colors } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    flex: 1,
    backgroundColor: '#F2F2F2'
  },
  cardStyle: {
    backgroundColor: Colors.white,
    borderBottomWidth: 1,
    borderBottomColor: '#F2F2F2',
    paddingHorizontal: 0,
    paddingTop: 10,
    borderRadius: 0,
    elevation: 1
  },
  imageItem: {
    width: Metrics.screenWidth - 40,
    height: (Metrics.screenWidth - 40) * 0.6,
    alignSelf: 'center',
    marginVertical: 10
  },
  itemName: {
    color: '#0F1524',
    fontSize: 16,
    fontFamily: Fonts.type.avenirRoman,
    lineHeight: 25,
    marginHorizontal: 10
  },
  restaurantName: {
    color: '#636770',
    fontSize: 14,
    fontFamily: Fonts.type.avenirRoman,
    lineHeight: 18,
    marginHorizontal: 10
  },
  txtQues: {
    color: '#0F1524',
    fontSize: 16,
    fontFamily: Fonts.type.avenirRoman,
    lineHeight: 22,
    marginHorizontal: 10
  },
  rateWrap: {
    // marginHorizontal: 25,
    marginVertical: 10
  },
  inputWrap: {
    backgroundColor: 'rgba(92,155,229,0.05)',
    borderRadius: 2,
    margin: 10,
    padding: 10
  },
  input: {
    color: '#0F1524',
    fontSize: 14,
    fontFamily: Fonts.type.avenirRoman,
    lineHeight: 19,
    textAlignVertical: 'top'
  },
  bottomView: {
    backgroundColor: '#fff'
  },
  btnSubmit: {
    marginHorizontal: 25,
    height: 42,
    backgroundColor: '#0F1524',
    borderRadius: 21,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 20
  },
  btnSubmitText: {
    color: '#FFFFFF',
    fontSize: 14,
    fontFamily: Fonts.type.avenirHeavy,
    lineHeight: 19
  }
})
