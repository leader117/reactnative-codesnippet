import { StyleSheet } from 'react-native'
import { ApplicationStyles, Colors, Metrics, Fonts } from '../../Themes/'

export default StyleSheet.create({
  // ...ApplicationStyles.screen,
  container: {
    backgroundColor: Colors.white,
    flex: 1,
    paddingHorizontal: Metrics.doubleBaseMargin
  },
  txtHeader: {
    marginTop: Metrics.screenHeight * 0.02,
    fontFamily: Fonts.type.avenirRoman,
    fontSize: Fonts.size.tFour
  },
  keyboardContainer: {
    flex: 1
  },
  middleView: {
    flex: 1
  },
  bottomView: {
    height: 50,
    alignItems: 'flex-end'
  },
  btnArrow: {
    // position: 'absolute',
    // bottom: 0,
    // right: 0
  },
  rowInput: {
    marginTop: Metrics.screenHeight * 0.1
  },
  textInput: {
    height: 40,
    borderColor: 'gray',
    width: '100%',
    fontSize: Fonts.size.input,
    fontFamily: Fonts.type.base,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc'
  },
  icFlag: {
    width: 35,
    height: 24
  },
  txtConnectSocial: {
    marginTop: Metrics.baseMargin,
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.type.avenirRoman
  },
  icArrowRight: {
    width: 35,
    height: 22
  },
  phoneInput: {
    flexDirection: 'row',
    height: 40,
    alignItems: 'center'
  }
})
