import { StyleSheet } from 'react-native'
import { ApplicationStyles, Colors, Metrics, Fonts } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    flex: 1,
    backgroundColor: '#F7F8FA',
    width: '100%',
    height: '100%',
    marginTop: Metrics.platform === 'android' ? -78 : 0
  },
  icMenu: {
    height: 32,
    width: 32
  },
  list: {
    flex: 1
    // backgroundColor: '#F7F8FA'
  },
  searchWrap: {
    backgroundColor: '#F7F8FA',
    paddingBottom: 5
  },
  // headerAnimate: {
  //   position: 'absolute',
  //   top: Metrics.navBarHeight,
  //   left: 0,
  //   right: 0,
  //   height: 60,
  //   zIndex: 9
  // },
  header: {
    width: Metrics.screenWidth,
    height: 220,
    backgroundColor: 'transparent',
    paddingTop: 10
  },
  bgHomeHeader: {
    // width: '100%',
    width: Metrics.screenWidth,
    height: 220
  },
  warpHello: {
    paddingHorizontal: Metrics.doubleBaseMargin,
    justifyContent: 'center',
    height: 220
  },
  txtHello: {
    marginTop: 35,
    fontFamily: Fonts.type.avenirBook,
    fontSize: 28,
    color: Colors.white
  },
  txtName: {
    fontFamily: Fonts.type.avenirHeavy
  },
  askEat: {
    color: Colors.white,
    fontFamily: Fonts.type.avenirRoman,
    fontSize: 20
  },
  btnSearch: {
    flexDirection: 'row',
    height: 60,
    backgroundColor: Colors.white,
    alignItems: 'center',
    width: '90%',
    borderRadius: 30,
    paddingHorizontal: Metrics.doubleBaseMargin,
    alignSelf: 'center',
    marginTop: -30
  },
  icSpoonPlate: {
    height: 19,
    width: 28,
    marginRight: Metrics.doubleBaseMargin
  },
  textInput: {
    fontFamily: Fonts.type.avenirRoman,
    fontSize: Fonts.size.medium,
    color: '#BDBDBD'
  },
  rowButtonDinePick: {
    flexDirection: 'row',
    marginTop: 24,
    justifyContent: 'space-between',
    width: '90%',
    alignSelf: 'center'
  },
  btnDine: {
    borderWidth: 1,
    height: 46,
    borderRadius: 23,
    width: '45%',
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: Colors.active,
    backgroundColor: Colors.white
  },
  txtBtnDine: {
    fontFamily: Fonts.type.avenirMedium,
    fontSize: Fonts.size.sixteen,
    color: '#737373'
  },
  warpPlace: {
    width: '90%',
    alignSelf: 'center'
  },
  rowHeaderPlace: {
    marginVertical: Metrics.doubleBaseMargin,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: '5%'
  },
  rowHeaderPlaceWrap: {
    flex: 1
  },
  rowMenu: {
    flexDirection: 'row',
    alignItems: 'center',
    width: 150,
    paddingRight: 10
  },
  rowMenuTitle: {
    fontFamily: Fonts.type.avenirHeavy,
    color: '#BDBDBD',
    fontSize: 16,
    lineHeight: 22,
    flex: 1
  },
  menuItem: {
    width: 158,
    height: 73,
    borderRadius: 20,
    paddingHorizontal: 10
  },
  rowMenuItem: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
    paddingRight: 10
  },
  icLocation: {
    width: 8,
    height: 12,
    marginRight: Metrics.baseMargin
  },
  txtLocation: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  cardWrap: {
    marginHorizontal: '5%',
    backgroundColor: Colors.snow,
    marginTop: Metrics.smallMargin,
    borderRadius: 7
  },
  cardLocation: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: Metrics.doubleBaseMargin,
    paddingHorizontal: 10,
    height: 60
  },
  txtAddress: {
    fontFamily: Fonts.type.avenirRoman,
    fontSize: Fonts.size.medium,
    color: '#0F1524'
  },
  restaurantName: {
    fontFamily: Fonts.type.avenirRoman,
    fontSize: Fonts.size.medium,
    color: '#0F1524',
    flex: 1,
    marginHorizontal: 10
  },
  txtPlace: {
    fontFamily: Fonts.type.avenirRoman,
    fontSize: Fonts.size.medium,
    color: '#0F1524'
  },
  placeNearby: {
    fontFamily: Fonts.type.avenirHeavy,
    fontSize: Fonts.size.sixteen,
    color: Colors.textPlace
  },
  restaurantImage: {
    width: 40,
    height: 40,
    borderRadius: 20
  },
  discountWrap: {
    width: 30,
    height: 17,
    backgroundColor: Colors.active,
    borderRadius: 11.5,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 10
  },
  discountText: {
    fontSize: 10,
    color: Colors.snow,
    lineHeight: 14,
    fontFamily: Fonts.type.avenirMedium
  }
})
