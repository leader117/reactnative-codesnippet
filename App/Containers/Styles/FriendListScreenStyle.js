import { StyleSheet } from 'react-native'
import { ApplicationStyles, Colors, Fonts } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF'
  },
  cardDetail: {
    borderRadius: 0,
    paddingHorizontal: 20,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: '#FFFFFF',
    borderBottomWidth: 1,
    borderBottomColor: '#cccccc',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.06,
    shadowRadius: 5,
    elevation: 1
  },
  textTabActive: {
    marginVertical: 10,
    color: '#0f1524',
    fontFamily: Fonts.type.avenirHeavy,
    fontSize: 16,
    marginRight: 13
  },
  textTab: {
    marginVertical: 10,
    color: '#bdbdbd',
    fontFamily: Fonts.type.avenirRoman,
    fontSize: 16,
    marginRight: 13
  },
  itemFriend: {
    marginHorizontal: 10,
    paddingHorizontal: 10,
    paddingVertical: 16,
    borderBottomWidth: 1,
    borderBottomColor: Colors.backgroundGrey,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#FFFFFF'
  },
  icAvaMan: {
    height: 47,
    width: 47,
    borderRadius: 24,
    borderColor: Colors.transparent,
    marginRight: 16
  },
  name: {
    color: '#0f1524',
    fontFamily: Fonts.type.avenirRoman,
    fontSize: 16
  },
  rowBack: {
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
    paddingVertical: 28,
    paddingLeft: 17,
    paddingRight: 13
  },
  buttonComplete: {
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  list: {
    flex: 1,
    marginTop: 5
  }
})
