import { StyleSheet } from 'react-native'
import { ApplicationStyles, Metrics, Fonts, Colors } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    flex: 1,
    backgroundColor: '#F2F2F2'
  },
  cardStyle: {
    backgroundColor: Colors.white,
    borderBottomWidth: 1,
    borderBottomColor: '#F2F2F2',
    paddingHorizontal: 0,
    paddingTop: 10,
    borderRadius: 0,
    elevation: 1
  },
  imageItem: {
    width: Metrics.screenWidth - 40,
    height: (Metrics.screenWidth - 40) * 0.6,
    alignSelf: 'center',
    marginVertical: 10
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  itemName: {
    flex: 1,
    color: '#0F1524',
    fontSize: 18,
    fontFamily: Fonts.type.avenirRoman,
    lineHeight: 25,
    marginHorizontal: 10
  },
  itemNameInfo: {
    color: '#636770',
    fontSize: 14,
    fontFamily: Fonts.type.avenirRoman,
    lineHeight: 18,
    marginHorizontal: 10
  },
  imageStar: {
    width: 16,
    height: 16
  },
  ratingText: {
    color: '#161E34',
    fontSize: 14,
    fontFamily: Fonts.type.avenirHeavy,
    lineHeight: 19,
    marginHorizontal: 10
  },
  restaurantName: {
    color: '#636770',
    fontSize: 14,
    fontFamily: Fonts.type.avenirRoman,
    lineHeight: 18,
    marginHorizontal: 10
  },
  txtQues: {
    color: '#0F1524',
    fontSize: 16,
    fontFamily: Fonts.type.avenirRoman,
    lineHeight: 22,
    marginHorizontal: 10
  },
  rateWrap: {
    // marginHorizontal: 25,
    marginVertical: 10
  },
  inputWrap: {
    marginHorizontal: 20,
    marginVertical: 20,
    backgroundColor: 'rgba(92,155,229,0.05)',
    borderRadius: 2
  },
  input: {
    color: '#0F1524',
    fontSize: 14,
    fontFamily: Fonts.type.avenirRoman,
    lineHeight: 19,
    textAlignVertical: 'top',
    minHeight: 60,
    paddingHorizontal: 10,
    paddingTop: 5
  },
  bottomView: {
    backgroundColor: '#fff'
  },
  btnSubmit: {
    marginHorizontal: 25,
    height: 42,
    backgroundColor: '#0F1524',
    borderRadius: 21,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 20
  },
  btnSubmitText: {
    color: '#FFFFFF',
    fontSize: 14,
    fontFamily: Fonts.type.avenirHeavy,
    lineHeight: 19
  },
  item: {
    backgroundColor: Colors.snow,
    marginTop: 10,
    padding: 20
  },
  itemAvatar: {
    width: 40,
    height: 40,
    borderRadius: 20
  },
  itemUserInfo: {
    flex: 1,
    marginLeft: 10
  },
  userName: {
    color: '#0F1524',
    fontSize: 16,
    fontFamily: Fonts.type.avenirRoman,
    lineHeight: 22
  },
  userPoint: {
    color: '#636770',
    fontSize: 14,
    fontFamily: Fonts.type.avenirRoman,
    lineHeight: 18
  },
  txtTime: {
    color: '#BDBDBD',
    fontSize: 12,
    fontFamily: Fonts.type.avenirRoman,
    lineHeight: 16
  },
  txtComment: {
    color: '#0F1524',
    fontSize: 14,
    fontFamily: Fonts.type.avenirRoman,
    lineHeight: 19
  },
  txtReadMore: {
    fontFamily: Fonts.type.avenirRoman,
    fontSize: 14,
    color: '#FF4D0D'
  },
  iconAction: {
    width: 18,
    height: 18
  },
  txtAction: {
    color: '#161E34',
    fontSize: 14,
    fontFamily: Fonts.type.avenirRoman,
    lineHeight: 19,
    marginHorizontal: 5
  },
  txtLike: {
    fontFamily: Fonts.type.avenirRoman,
    fontSize: 14,
    color: '#bdbdbd'
  },
  oval: {
    width: 2,
    height: 2,
    borderRadius: 1,
    backgroundColor: '#bdbdbd',
    marginHorizontal: 5
  }
})
