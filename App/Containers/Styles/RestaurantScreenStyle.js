import { StyleSheet } from 'react-native'
import { ApplicationStyles, Metrics, Fonts, Colors } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    flex: 1,
    backgroundColor: '#F2F2F2'
  },
  cardStyle: {
    backgroundColor: Colors.white,
    borderBottomWidth: 1,
    borderBottomColor: '#F2F2F2',
    paddingHorizontal: 0,
    paddingTop: 10,
    borderRadius: 0
    // elevation: 1
  },
  borderShadow: {
    borderBottomWidth: 1,
    borderBottomColor: '#cccccc',
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 8
    },
    shadowRadius: 8,
    shadowOpacity: 0.06
    // elevation: 1
  },
  rowHeader: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  rowHeaderIcon: {
    width: 11,
    height: 11
  },
  rowHeaderText: {
    fontSize: 12,
    color: '#161E34',
    fontFamily: Fonts.type.avenirHeavy,
    marginHorizontal: 5
  },
  headerLabel: {
    fontSize: 12,
    fontFamily: Fonts.type.avenirRoman,
    color: '#AFB5CC',
    marginVertical: 10
  },
  headerButton: {
    height: 30,
    borderRadius: 30,
    backgroundColor: '#FF7E04',
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 10,
    paddingHorizontal: 15
  },
  headerButtonText: {
    fontSize: 12,
    fontFamily: Fonts.type.avenirRoman,
    color: '#FFFFFF'
  },
  headerMenu: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingRight: 20,
    flexWrap: 'wrap'
  },
  btnMenu: {
    paddingLeft: 20
  },
  headerMenuText: {
    fontSize: 16,
    color: '#BDBDBD',
    fontFamily: Fonts.type.avenirRoman
  },
  headerMenuTextActive: {
    fontSize: 16,
    color: '#0F1524',
    fontFamily: Fonts.type.avenirHeavy
  },
  txtCategory: {
    fontSize: 16,
    color: '#0F1524',
    fontFamily: Fonts.type.avenirMedium
  },
  item: {
    marginLeft: 20,
    marginTop: 15
  },
  itemImage: {
    width: Metrics.screenWidth * 0.65,
    height: Metrics.screenWidth * 0.4,
    borderRadius: 5
  },
  itemRatingWrap: {
    position: 'absolute',
    bottom: 0,
    right: 0,
    width: 54,
    height: 23,
    backgroundColor: '#86BC27',
    borderBottomRightRadius: 5,
    borderTopLeftRadius: 5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  itemRatingImage: {
    width: 11,
    height: 11
  },
  itemRatingText: {
    ...Fonts.style.normal,
    fontSize: 13,
    fontWeight: 'bold',
    color: Colors.white,
    marginLeft: 5
  },
  itemTitle: {
    fontFamily: Fonts.type.avenirRoman,
    fontSize: 16,
    color: '#0F1524',
    marginTop: 10,
    width:  Metrics.screenWidth * 0.65,
  },
  featuredMenuDes: {
    fontFamily: Fonts.type.avenirRoman,
    fontSize: 14,
    color: '#636770',
    marginTop: 5,
    width:  Metrics.screenWidth * 0.65,
  },
  MenuDes: {
    fontFamily: Fonts.type.avenirRoman,
    fontSize: 14,
    color: '#636770',
    marginTop: 5,
    flex: 1, 
    marginTop: 5, 
    marginRight: 10
  },
  rowItem: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10
  },
  itemRowComment: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  itemRowCommentIcon: {
    width: 15,
    height: 15
  },
  itemRowCommentText: {
    fontFamily: Fonts.type.avenirRoman,
    fontSize: 12,
    color: '#0F1524',
    marginHorizontal: 10
  },
  itemTxtAed: {
    fontFamily: Fonts.type.avenirHeavy,
    fontSize: 12,
    color: '#0F1524',
    marginHorizontal: 10,
    flex: 1
  },
  footer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: Colors.white,
    maxHeight: Metrics.screenHeight - Metrics.navBarHeight - 20,
    borderWidth: 1,
    // shadowOpacity: 1,
    // elevation: 1,
    borderColor: Colors.backgroundGrey,
    // height: 108,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    paddingVertical: 15,
    elevation: 4,
    shadowOffset: { width: 5, height: 14 },
    shadowColor: 'grey',
    shadowOpacity: 0.5,
    shadowRadius: 10
  },
  btnCheckIn: {
    height: 42,
    borderRadius: 30,
    marginHorizontal: 30,
    backgroundColor: Colors.active,
    alignItems: 'center',
    justifyContent: 'center'
  },
  btnSkip: {
    height: 42,
    borderRadius: 30,
    marginHorizontal: 30,
    backgroundColor: '#0F1524',
    alignItems: 'center',
    justifyContent: 'center'
  },
  btnCheckInText: {
    fontFamily: Fonts.type.avenirHeavy,
    color: Colors.white,
    fontSize: 14
  },
  itemVertical: {
    paddingHorizontal: 20,
    paddingVertical: 15,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: '#D8D8D8'
  },
  itemVerticalRow: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  footerContent: {
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 20
  },
  footerQuestionText: {
    fontSize: 18,
    color: '#0F1524',
    fontFamily: Fonts.type.avenirMedium,
    lineHeight: 25
  },
  footerQuestionTextInfo: {
    fontSize: 14,
    color: 'rgb(99,103,112)',
    fontFamily: Fonts.type.avenirRoman,
    lineHeight: 18,
    textAlign: 'center'
  },
  footerActionText: {
    fontSize: 12,
    color: 'rgb(255,77,13)',
    fontFamily: Fonts.type.avenirLightOblique,
    lineHeight: 18,
    textAlign: 'center',
    textDecorationLine: 'underline'
  },
  cameraWrap: {
    borderRadius: 14,
    height: Metrics.screenHeight - Metrics.navBarHeight - 200,
    marginHorizontal: 22,
    backgroundColor: 'rgb(240,240,240)'
  },
  camera: {
    width: '100%',
    height: '100%',
    borderRadius: 14,
    overflow: 'hidden',
    backgroundColor: 'rgb(240,240,240)'
  },
  tableNumberText: {
    fontSize: 28,
    color: Colors.active,
    fontFamily: Fonts.type.avenirRoman,
    marginTop: 10,
    textAlign: 'center',
    minWidth: 100
  },
  footerContentRowUser: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 15
  },
  footerContentRowUserAvatar: {
    width: 48,
    height: 48,
    borderRadius: 24,
    borderWidth: 2,
    borderColor: Colors.white,
    marginLeft: -10
  },
  footerContentRowUserBtn: {
    width: 48,
    height: 48,
    borderRadius: 24,
    borderWidth: 2,
    borderColor: Colors.white,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F1F3F7',
    marginLeft: -10
  },
  rowTable: {
    flexDirection: 'row',
    paddingHorizontal: 20
  },
  rowTableName: {
    fontSize: 18,
    fontFamily: Fonts.type.avenirMedium,
    color: '#0F1524'
  },
  rowTableNumberUser: {
    fontSize: 14,
    fontFamily: Fonts.type.avenirRoman,
    color: '#636770'
  },
  rowTableBtn: {
    backgroundColor: '#0F1524',
    height: 31,
    borderRadius: 30,
    paddingHorizontal: 15,
    alignItems: 'center',
    justifyContent: 'center'
  },
  rowTableBtnText: {
    fontFamily: Fonts.type.avenirHeavy,
    fontSize: 14,
    color: Colors.white
  },
  txtFooter: {
    fontFamily: Fonts.type.avenirMedium,
    fontSize: 18,
    color: '#0F1524',
    lineHeight: 25,
    textAlign: 'left',
    marginHorizontal: 20,
    marginBottom: 15
  },
  rowFooter: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
    paddingHorizontal: 10,
    marginTop: 2,
    marginBottom: Metrics.isIphoneX ? 0 : 20
  },
  rowNotAvailableTxt: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
    marginTop: 3,
    paddingHorizontal: 10,
  },
  btnAvailableServices: {
    height: 42,
    borderRadius: 30,
    marginHorizontal: 10,
    borderColor: Colors.active,
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1
  },
  btnServiceText: {
    fontFamily: Fonts.type.avenirMedium,
    color: Colors.active,
    fontSize: 14
  },
  txtNotAvailable: {
    fontFamily: Fonts.type.avenirMediumOblique,
    color: '#9B9B9B',
    fontSize: 10,
    flex: 1,
    textAlign: 'center',
  },
  rowTableActions: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 30,
    marginVertical: 20,
    justifyContent: 'center'
  },
  rowTableActionWrap: {
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 30
  },
  rowTableActionWrapIcon: {
    width: 58,
    height: 58
  },
  rowTableActionWrapTitle: {
    color: '#0F1524',
    fontFamily: Fonts.type.avenirRoman,
    fontSize: 14,
    lineHeight: 19
  },
  bottomModal: {
    justifyContent: 'flex-end',
    margin: 0
  },
  modalContent: {
    backgroundColor: Colors.white,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15
  },
  modalRowHeader: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  modalRowHeaderText: {
    fontSize: 14,
    fontFamily: Fonts.type.avenirRoman,
    color: '#636770',
    flex: 1,
    marginLeft: 20,
    lineHeight: 19
  },
  modalRowHeaderTextBold: {
    fontSize: 20,
    fontFamily: Fonts.type.avenirMedium,
    color: '#292D44',
    flex: 1,
    marginLeft: 20,
    lineHeight: 25,
    marginTop: 8
  },
  txtContent: {
    marginHorizontal: 20,
    fontFamily: Fonts.type.avenirMedium,
    fontSize: 20,
    lineHeight: 25,
    color: '#0F1524'
  },
  rowContent: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 20,
    marginTop: 10
  },
  rowContentLabel: {
    color: '#222D4F',
    fontFamily: Fonts.type.avenirRoman,
    fontSize: 14,
    flex: 1
  },
  btnBill: {
    height: 42,
    borderRadius: 30,
    marginHorizontal: 25,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#0F1524',
    marginVertical: 10
  },
  btnBillText: {
    fontFamily: Fonts.type.avenirHeavy,
    fontSize: 14,
    color: Colors.white
  },
  btnConfirmOrder: {
    minHeight: 42,
    marginHorizontal: 30,
    backgroundColor: '#3CCFA9',
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 21,
    marginTop: 10,
    paddingRight: 18,
    paddingLeft: 10
  },
  btnConfirmOrderTitle: {
    color: '#ffffff',
    fontSize: 14,
    fontFamily: Fonts.type.avenirHeavy,
    lineHeight: 19,
    flex: 1,
    textAlign: 'center'
  },
  btnConfirmOrderLeft: {
    width: 28,
    height: 28,
    borderRadius: 19,
    backgroundColor: 'rgba(255,255,255,0.25)',
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 15,
    marginVertical: 7
  },
  btnConfirmOrderLeftTitle: {
    color: '#ffffff',
    fontSize: 14,
    fontFamily: Fonts.type.avenirHeavy,
    textAlign: 'center'
  },
  btnConfirmOrderRightTitle: {
    color: '#ffffff',
    fontSize: 12,
    fontFamily: Fonts.type.avenirMedium,
    textAlign: 'center',
    marginVertical: 13
  },
  icDropdownBill: {
    width: 14,
    height: 7,
    alignSelf: 'center',
    top: 0,
    left: Metrics.screenWidth / 2 - 7,
    position: 'absolute'
  },
  iconMinimize: {
    width: 20,
    height: 4,
    backgroundColor: Colors.white,
    position: 'absolute',
    top: -25,
    alignSelf: 'center',
    elevation: 4,
    shadowOffset: { width: 2, height: 2 },
    shadowColor: 'grey',
    shadowOpacity: 0.15,
    shadowRadius: 10,
    zIndex: 1,
    borderRadius: 2
  }
})
