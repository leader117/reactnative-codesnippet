import { StyleSheet } from 'react-native'
import { ApplicationStyles, Metrics, Fonts, Colors } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF'
  },
  cardStyle: {
    backgroundColor: Colors.white,
    borderBottomWidth: 1,
    borderBottomColor: '#F2F2F2',
    paddingHorizontal: 0,
    borderRadius: 0,
    marginTop: Metrics.platform === 'ios' ? -20 : 0
  },
  borderShadow: {
    borderBottomWidth: 1,
    borderBottomColor: '#cccccc',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.06,
    shadowRadius: 5,
    elevation: 2
  },
  list: {
    flex: 1
  },
  rowHeader: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  headerMenu: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingRight: 20
  },
  btnMenu: {
    paddingLeft: 20
  },
  headerMenuText: {
    fontSize: 16,
    color: '#BDBDBD',
    fontFamily: Fonts.type.avenirRoman
  },
  headerMenuTextActive: {
    fontSize: 16,
    color: '#0F1524',
    fontFamily: Fonts.type.avenirHeavy
  },
  icSearch: {
    width: 18,
    height: 18,
    marginHorizontal: 20
  },
  rowUser: {
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: '#D8D8D8',
    paddingLeft: 20,
    paddingVertical: 15
  },
  rowUserAvatar: {
    width: 47,
    height: 47,
    borderRadius: 47 / 2
  },
  rowUserContent: {
    marginHorizontal: 15,
    flex: 1
  },
  rowUserName: {
    fontFamily: Fonts.type.avenirRoman,
    color: '#0F1524',
    fontSize: 16
  },
  rowUserPlate: {
    fontFamily: Fonts.type.avenirRoman,
    color: '#636770',
    fontSize: 14
  },
  rowUserIconAdd: {
    padding: 20
  }
})
