import { StyleSheet } from 'react-native'
import { ApplicationStyles, Colors, Fonts } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF'
  },
  item: {
    borderBottomWidth: 5,
    borderBottomColor: Colors.backgroundGrey
  },
  viewTop: {
    paddingTop: 30,
    paddingBottom: 16,
    borderBottomWidth: 1,
    borderBottomColor: Colors.backgroundGrey,
    paddingHorizontal: 20
  },
  name: {
    fontFamily: Fonts.type.avenirMedium,
    fontSize: 16,
    color: '#0f1524'
  },
  date: {
    fontFamily: Fonts.type.avenirRoman,
    fontSize: 12,
    color: '#bdbdbd'
  },
  rowHeader: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  rowHeaderIcon: {
    width: 11,
    height: 11
  },
  rowHeaderText: {
    fontSize: 12,
    color: '#161E34',
    fontFamily: Fonts.type.avenirHeavy,
    marginHorizontal: 5
  },
  viewInfo: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  viewBottom: {
    paddingVertical: 14,
    paddingHorizontal: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  textTotal: {
    fontFamily: Fonts.type.avenirHeavy,
    fontSize: 14,
    color: '#0f1524'
  }
})
