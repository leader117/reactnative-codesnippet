import { StyleSheet } from 'react-native'
import { ApplicationStyles, Colors, Fonts, Metrics } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    backgroundColor: Colors.white,
    flex: 1,
    paddingHorizontal: Metrics.doubleBaseMargin
  },
  txtHeader: {
    marginTop: Metrics.screenHeight * 0.02,
    fontFamily: Fonts.type.avenirRoman,
    fontSize: Fonts.size.tFour
  },
  keyboardContainer: {
    flex: 1
  },
  middleView: {
    flex: 1,
    paddingTop: 50
  },
  bottomView: {
    height: 50,
    alignItems: 'flex-end'
  },
  btnArrow: {
    // position: 'absolute',
    // bottom: 0,
    // right: 0
  },
  rowInput: {
    flexDirection: 'row',
    marginTop: Metrics.screenHeight * 0.1,
    height: 40,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
    alignItems: 'center'
  },
  textInput: {
    height: 40,
    borderColor: 'gray',
    width: '100%',
    // marginLeft: Metrics.doubleBaseMargin,
    fontSize: Fonts.size.input,
    fontFamily: Fonts.type.base
  },
  icFlag: {
    width: 35,
    height: 24
  },
  txtConnectSocial: {
    marginTop: Metrics.baseMargin,
    fontSize: 14,
    fontFamily: Fonts.type.avenirBook,
    color: '#0f1524'
  },
  icArrowRight: {
    width: 35,
    height: 22
  }
})
