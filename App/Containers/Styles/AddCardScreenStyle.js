import { StyleSheet } from 'react-native'
import { ApplicationStyles, Colors, Metrics, Fonts } from '../../Themes/'

export default StyleSheet.create({
  // ...ApplicationStyles.screen,
  container: {
    backgroundColor: Colors.white,
    height: Metrics.screenHeight - Metrics.navBarHeight,
    paddingHorizontal: Metrics.doubleBaseMargin
  },
  txtHeader: {
    marginTop: 20,
    fontFamily: Fonts.type.avenirLight,
    fontSize: Fonts.size.input,
    alignSelf: 'center',
    textAlign: 'center'
  },
  txtLabelInput: {
    color: Colors.textPlace,
    fontFamily: Fonts.type.avenirLight,
    fontSize: Fonts.size.small
  },
  keyboardContainer: {
    flex: 1
  },
  middleView: {
    flex: 1
  },
  bottomView: {
    height: 50,
    alignItems: 'flex-end',
    marginBottom: 20
  },
  rowInput: {
    marginTop: Metrics.screenHeight * 0.1,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc'
  },
  textInput: {
    height: 40,
    borderColor: 'gray',
    width: '100%',
    // marginLeft: Metrics.doubleBaseMargin,
    fontSize: Fonts.size.input,
    fontFamily: Fonts.type.base,
    marginBottom: 5
  },
  rowDateCCV: {
    flexDirection: 'row',
    marginTop: Metrics.doubleBaseMargin * 2
  },
  colDate: {
    flex: 1,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc'
  },
  btnNext: {
    borderRadius: 50,
    backgroundColor: '#0F1524'
  }
})
