import { StyleSheet } from 'react-native'
import { ApplicationStyles, Metrics, Fonts, Colors } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    paddingBottom: 42
  },
  imageCheck: {
    width: 80,
    height: 80,
    alignSelf: 'center',
    marginTop: 20
  },
  txtSuccess: {
    fontSize: 18,
    fontFamily: Fonts.type.avenirHeavy,
    color: '#86BC27',
    lineHeight: 25,
    marginTop: 5,
    textAlign: 'center'
  },
  txtInfo: {
    fontSize: 14,
    fontFamily: Fonts.type.avenirMedium,
    color: '#636770',
    lineHeight: 19,
    marginTop: 15,
    textAlign: 'center'
  },
  txtCode: {
    fontSize: 52,
    fontFamily: Fonts.type.avenirHeavy,
    color: '#0F1524',
    lineHeight: 71,
    marginTop: 25,
    textAlign: 'center'
  },
  rowHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingVertical: 10
  },
  rowHeaderTitle: {
    fontFamily: Fonts.type.avenirRoman,
    fontSize: 16,
    lineHeight: 22,
    color: '#0F1524',
    marginRight: 5
  },
  rowHeaderValue: {
    fontFamily: Fonts.type.avenirHeavy,
    fontSize: 12,
    lineHeight: 16,
    color: '#BDBDBD'
  },
  item: {
    height: 55,
    justifyContent: 'center',
    paddingHorizontal: 30
  },
  rowItem: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  rowItemName: {
    fontFamily: Fonts.type.avenirRoman,
    fontSize: 14,
    lineHeight: 19,
    color: '#0F1524',
    marginRight: 10,
    flex: 1
  },
  rowItemAed: {
    fontFamily: Fonts.type.avenirHeavy,
    fontSize: 14,
    lineHeight: 19,
    color: '#0F1524',
    marginRight: 10
  },
  itemQty: {
    fontFamily: Fonts.type.avenirRoman,
    fontSize: 12,
    lineHeight: 16,
    color: '#BDBDBD',
    marginLeft: 24
  },
  footer: {
    paddingBottom: 20
  },
  rowFooter: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 20,
    marginTop: 10
  },
  rowFooterLabel: {
    fontFamily: Fonts.type.avenirRoman,
    fontSize: 14,
    lineHeight: 18,
    color: '#636770',
    flex: 1
  },
  rowFooterTotal: {
    fontFamily: Fonts.type.avenirHeavy,
    fontSize: 16,
    lineHeight: 18,
    color: '#0F1524',
    flex: 1
  },
  rowFooterValue: {
    fontFamily: Fonts.type.avenir,
    fontSize: 14,
    lineHeight: 18,
    color: '#636770',
    fontWeight: '500'
  },
  btnBill: {
    height: 42,
    borderRadius: 30,
    marginHorizontal: 25,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#3ccfa9',
    marginVertical: 10
  },
  btnBillText: {
    fontFamily: Fonts.type.avenirHeavy,
    fontSize: 14,
    color: Colors.white
  },
  footers: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: Colors.white
  },
  footerContent: {
  }
})
