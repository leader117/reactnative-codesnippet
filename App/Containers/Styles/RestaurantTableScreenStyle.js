import { StyleSheet } from 'react-native'
import { ApplicationStyles, Metrics, Fonts, Colors } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF'
  },
  rightIcon: {
    width: 24,
    height: 15
  },
  cardStyle: {
    backgroundColor: Colors.white,
    borderBottomWidth: 1,
    borderBottomColor: '#F2F2F2',
    paddingHorizontal: 0,
    paddingTop: 10,
    borderRadius: 0
  },
  borderShadow: {
    borderBottomWidth: 1,
    shadowOpacity: 1,
    elevation: 1,
    borderBottomColor: Colors.backgroundGrey
  },
  scrollView: {
    paddingVertical: 10
  },
  content: {
    paddingHorizontal: 20
  },
  contentRowUser: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 15
  },
  contentRowUserAvatar: {
    width: 58,
    height: 58,
    borderRadius: 29,
    borderWidth: 2,
    borderColor: Colors.white,
    marginLeft: -10
  },
  contentRowUserBtn: {
    width: 58,
    height: 58,
    borderRadius: 29,
    borderWidth: 2,
    borderColor: Colors.white,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F1F3F7',
    marginLeft: -10
  },
  rowUserText: {
    marginTop: 10,
    color: '#636770',
    fontFamily: Fonts.type.avenirMedium,
    fontSize: 14
  },
  rowTable: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  rowTableText: {
    color: '#0F1524',
    fontSize: 16,
    fontFamily: Fonts.type.avenirRoman
  },
  rowTableTextBold: {
    color: '#0F1524',
    fontSize: 16,
    fontFamily: Fonts.type.avenirHeavy
  },
  itemWrap: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: Colors.backgroundGrey,
    backgroundColor: '#FAFAFA'
  },
  rowItem: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  rowItemName: {
    flex: 1,
    color: '#0F1524',
    fontSize: 16,
    fontFamily: Fonts.type.avenirRoman
  },
  rowHeaderIcon: {
    width: 11,
    height: 11
  },
  rowHeaderText: {
    fontSize: 12,
    color: '#161E34',
    fontFamily: Fonts.type.avenirHeavy,
    marginHorizontal: 5
  },
  rowItemPrice: {
    flex: 1,
    fontSize: 12,
    fontFamily: Fonts.type.avenirMedium,
    color: '#0F1524',
    marginTop: 3,
    marginRight: 5
  },
  rowItemStatus: {
    fontSize: 12,
    fontFamily: Fonts.type.avenirMedium,
    color: '#636770',
    marginTop: 3
  },
  btnBill: {
    height: 36,
    borderRadius: 30,
    marginHorizontal: 25,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#0F1524',
    marginVertical: 20
  },
  btnBillText: {
    fontFamily: Fonts.type.avenirHeavy,
    fontSize: 14,
    color: Colors.white
  },
  bottomModal: {
    justifyContent: 'flex-end',
    margin: 0
  },
  modalContent: {
    backgroundColor: Colors.white,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15
  },
  modalRowHeader: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  modalRowHeaderText: {
    fontSize: 20,
    fontFamily: Fonts.type.avenirMedium,
    color: '#292D44',
    flex: 1,
    marginLeft: 20
  },
  txtContent: {
    marginHorizontal: 20,
    fontFamily: Fonts.type.avenirRoman,
    fontSize: 16,
    color: '#0F1524'
  },
  rowContent: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 20,
    marginTop: 10
  },
  rowContentLabel: {
    color: '#222D4F',
    fontFamily: Fonts.type.avenirRoman,
    fontSize: 14,
    flex: 1
  },
  rowContentTime: {
    color: '#222D4F',
    fontFamily: Fonts.type.avenirMedium,
    fontSize: 14
  },
  btnCancel: {
    height: 42,
    borderRadius: 30,
    marginHorizontal: 25,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ECEFF4',
    marginVertical: 20,
    marginTop: 50
  },
  btnCancelText: {
    fontFamily: Fonts.type.avenirHeavy,
    fontSize: 14,
    color: '#636770'
  }
})
