import React, { Component } from 'react'
import {
  ScrollView,
  Text,
  KeyboardAvoidingView,
  View,
  TouchableOpacity,
  Image,
  FlatList,
  Platform, PermissionsAndroid
} from 'react-native'
import { connect } from 'react-redux'
import _ from 'lodash'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'
import Ionicons from 'react-native-vector-icons/Ionicons'
import SwipeListView from 'react-native-swipe-list-view/components/SwipeListView'

// Styles
import styles from './Styles/FriendListScreenStyle'
import { Images } from '../Themes'
import Contacts from 'react-native-contacts'
import { FriendActions, bindActionCreators } from '../Redux/Actions'

class FriendListScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      index: 0,
      contacts: [],
      dataContacts: []
    }
  }

  findPhone (phones, phone) {
    for (let i = 0; i < phones.length; i++) {
      if (phones[i] === phone) {
        return false
      }
    }
    return true
  }

  componentDidMount (){
    if (Platform.OS === 'ios') {
      Contacts.getAll((err, contacts) => {
        if (err === 'denied') {
          // error
        } else {
          __DEV__ && console.log('contacts', contacts)
          // contacts returned in Array
          let data = []
          let dataContacts = []
          if (contacts !== []) {
            for (let i = 0; i < contacts.length; i++) {
              let phones = []
              for (let j = 0; j < contacts[i].phoneNumbers.length; j++) {
                let phone = contacts[i].phoneNumbers[j].number.replace(/ /g, '')
                phone = phone.replace(/-/g, '')
                phone = phone.replace(/\)/g, '')
                phone = phone.replace(/\(/g, '')
                console.log('phone', phone)
                if (phone[0] !== '0') {
                  if (phone[0] === '+') {
                    if (this.findPhone(phones, phone)) {
                      phones.push(phone)
                      dataContacts.push({
                        phone_number: phone,
                        thumbnailPath: contacts[i].thumbnailPath,
                        contactName: contacts[i].givenName && contacts[i].familyName ? contacts[i].givenName + ' ' + contacts[i].familyName : contacts[i].givenName ? contacts[i].givenName : contacts[i].familyName
                      })
                    }
                  } else if (phone[0] === '1' || phone[0] === '2' || phone[0] === '3' || phone[0] === '4' || phone[0] === '5' || phone[0] === '6' || phone[0] === '7' || phone[0] === '8' || phone[0] === '9') {
                    phone = '+' + phone
                    if (this.findPhone(phones, phone)) {
                      phones.push(phone)
                      dataContacts.push({
                        phone_number: phone,
                        thumbnailPath: contacts[i].thumbnailPath,
                        contactName: contacts[i].givenName && contacts[i].familyName ? contacts[i].givenName + ' ' + contacts[i].familyName : contacts[i].givenName ? contacts[i].givenName : contacts[i].familyName
                      })
                    }
                  }
                } else {
                  phone = this.props.country && this.props.country.location
                    ? phone.replace('0', `+${this.props.country.location.calling_code}`)
                    : phone.replace('0', `+33`)
                  if (this.findPhone(phones, phone)) {
                    phones.push(phone)
                    dataContacts.push({
                      phone_number: phone,
                      thumbnailPath: contacts[i].thumbnailPath,
                      contactName: contacts[i].givenName && contacts[i].familyName ? contacts[i].givenName + ' ' + contacts[i].familyName : contacts[i].givenName ? contacts[i].givenName : contacts[i].familyName
                    })
                  }
                }
              }
              if (phones.length !== 0) {
                data.push(phones)
              }
            }
          }
          this.setState({ contacts: data, dataContacts })
          this.props.syncContacts(data, dataContacts)
        }
      })
    } else {
      PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
        {
          'title': 'Contacts',
          'message': 'This app would like to view your contacts.'
        }
      ).then(async () => {
        await Contacts.getAll((err, contacts) => {
          if (err === 'denied') {
            // error
          } else {
            __DEV__ && console.log('contacts', contacts)
            // contacts returned in Array
            let data = []
            let dataContacts = []
            if (contacts !== []) {
              for (let i = 0; i < contacts.length; i++) {
                let phones = []
                for (let j = 0; j < contacts[i].phoneNumbers.length; j++) {
                  let phone = contacts[i].phoneNumbers[j].number.replace(/ /g, '')
                  phone = phone.replace(/-/g, '')
                  phone = phone.replace(/\)/g, '')
                  phone = phone.replace(/\(/g, '')
                  console.log('phone', phone)
                  if (phone[0] !== '0') {
                    if (phone[0] === '+') {
                      if (this.findPhone(phones, phone)) {
                        phones.push(phone)
                        dataContacts.push({
                          phone_number: phone,
                          thumbnailPath: contacts[i].thumbnailPath,
                          contactName: contacts[i].givenName && contacts[i].familyName ? contacts[i].givenName + ' ' + contacts[i].familyName : contacts[i].givenName ? contacts[i].givenName : contacts[i].familyName
                        })
                      }
                    } else if (phone[0] === '1' || phone[0] === '2' || phone[0] === '3' || phone[0] === '4' || phone[0] === '5' || phone[0] === '6' || phone[0] === '7' || phone[0] === '8' || phone[0] === '9') {
                      phone = '+' + phone
                      if (this.findPhone(phones, phone)) {
                        phones.push(phone)
                        dataContacts.push({
                          phone_number: phone,
                          thumbnailPath: contacts[i].thumbnailPath,
                          contactName: contacts[i].givenName && contacts[i].familyName ? contacts[i].givenName + ' ' + contacts[i].familyName : contacts[i].givenName ? contacts[i].givenName : contacts[i].familyName
                        })
                      }
                    }
                  } else {
                    phone = this.props.country && this.props.country.location
                      ? phone.replace('0', `+${this.props.country.location.calling_code}`)
                      : phone.replace('0', `+33`)
                    if (this.findPhone(phones, phone)) {
                      phones.push(phone)
                      dataContacts.push({
                        phone_number: phone,
                        thumbnailPath: contacts[i].thumbnailPath,
                        contactName: contacts[i].givenName && contacts[i].familyName ? contacts[i].givenName + ' ' + contacts[i].familyName : contacts[i].givenName ? contacts[i].givenName : contacts[i].familyName
                      })
                    }
                  }
                }
                if (phones.length !== 0) {
                  data.push(phones)
                }
              }
            }
            this.setState({ contacts: data, dataContacts })
            this.props.syncContacts(data, dataContacts)
          }
        })
      })
    }
  }

  renderItem (item) {
    return (
      <View style={styles.itemFriend}>
        <Image source={item.meta.avatar_url ? { uri: item.meta.avatar_url } : Images.profile} defaultSource={Images.profile} style={styles.icAvaMan} resizeMode='cover' />
        <Text style={styles.name}>{item.meta.first_name} {item.meta.last_name}</Text>
      </View>
    )
  }

  renderHiddenItem (item) {
    return (
      <View style={styles.rowBack}>
        <TouchableOpacity
          style={styles.buttonComplete} onPress={() => {
            this.props.removeFriend(item.uid, this.state.contacts, this.state.dataContacts)
          }}>
          <Ionicons name='md-close-circle' size={24} color='#ee4935' />
        </TouchableOpacity>
      </View>
    )
  }

  render () {
    const { friends } = this.props
    return (
      <View style={styles.container}>
        <View style={styles.cardDetail}>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <TouchableOpacity
              onPress={() => this.setState({ index: 0 })}
            >
              <Text style={this.state.index === 0 ? styles.textTabActive : styles.textTab}>Friends</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.setState({ index: 1 })}
            >
              <Text style={this.state.index === 1 ? styles.textTabActive : styles.textTab}>Contacts</Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity style={{ paddingVertical: 10 }}>
            <Image source={Images.icSearch} style={{ width: 18, height: 18 }} resizeMode={'contain'} />
          </TouchableOpacity>
        </View>
        {this.state.index === 0 &&
          <SwipeListView
            style={styles.list}
            useFlatList
            data={friends}
            keyExtractor={(item, index) => index.toString()}
            extraData={this.state}
            renderItem={(rowData, rowMap) => this.renderItem(rowData.item)}
            renderHiddenItem={(rowData, rowMap) => this.renderHiddenItem(rowData.item)}
            leftOpenValue={30}
            closeOnRowPress
            closeOnScroll
            closeOnRowBeginSwipe
            previewOpenValue={-44}
          />
        }
        {this.state.index === 1 && <View style={{ flex: 1 }}>
          <FlatList
            data={this.props.contacts}
            style={styles.list}
            keyExtractor={(item, index) => index.toString()}
            extraData={this.state}
            renderItem={({ item, index }) => (
              <View style={[styles.itemFriend, { justifyContent: 'space-between', paddingHorizontal: 0, marginLeft: 20 }]}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <Image source={item.thumbnailPath ? { uri: item.thumbnailPath } : Images.profile} defaultSource={Images.profile} style={styles.icAvaMan} resizeMode='cover' />
                  <Text style={styles.name}>{item.contactName}</Text>
                </View>
                <TouchableOpacity
                  onPress={() => {
                    if (!item.add) {
                      this.props.addFriend(item.uid, this.state.contacts, this.state.dataContacts)
                    }
                  }}
                >
                  {item.add ? <Ionicons name='ios-checkmark-circle' size={24} color='#0f1524' style={{ marginHorizontal: 16, marginVertical: 6 }} />
                    : <Ionicons name='ios-add' size={24} color='#bdbdbd' style={{ marginHorizontal: 16, marginVertical: 6 }} />
                  }
                </TouchableOpacity>
              </View>
            )}
          />
        </View>}
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    contacts: state.friend.contacts,
    friends: state.friend.friends,
    country: state.location.country
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    ...bindActionCreators(FriendActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(FriendListScreen)
