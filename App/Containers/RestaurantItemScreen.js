import React, { Component } from 'react'
import {
  Text,
  Image,
  View,
  TouchableOpacity,
  TextInput,
  Platform,
  KeyboardAvoidingView
} from 'react-native'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/RestaurantItemScreenStyle'
import { Hr } from '../Components'
import { Images } from '../Themes'
import SafeAreaView from 'react-native-safe-area-view'
import { Actions } from 'react-native-router-flux'
import ReadMore from 'react-native-read-more-text'
import _ from 'lodash'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import firebase from 'react-native-firebase'

class RestaurantItemScreen extends Component {
  constructor(props){
    super(props)
    this.state = {
      dish: this.props.dish[1],
      optionStatus: [],
      additional_notes: ''
    }
  }

  componentDidMount(){
    firebase.database().ref(`restaurants/${this.props.res_doc_key}/dishes/${this.props.dish[0]}`).on('value', snapshot => {
      if(snapshot.exists()){
        this.setState({ dish: snapshot.val() })
      }
    })
  }

  componentWillUnmount(){
    firebase.database().ref(`restaurants/${this.props.res_doc_key}/dishes/${this.props.dish[0]}`).off()
  }

  render () {
    const options = this.state.optionStatus
    return (
      <View style={styles.container}>
        <KeyboardAwareScrollView
          style={{ flex: 1 }}
          contentContainerStyle={{ paddingTop: 20, paddingBottom: 120 }}
          extraScrollHeight={140}
        >
          <View style={styles.content}>
            {
              this.state.dish.image_url? 
              <Image borderRadius={2} source={{ uri: this.state.dish.image_url }} style={styles.itemImage} />
              : null
            }
            
            <Text style={styles.itemName}>{this.state.dish.name}</Text>
            <ReadMore
              numberOfLines={2}
              renderTruncatedFooter={(onPress) => this.renderViewMore(onPress)}
              renderRevealedFooter={(onPress) => this.renderViewLess(onPress)}
            >
              <Text style={styles.itemDescription}>
                {this.state.dish.description}
              </Text>
            </ReadMore>
            <View style={[styles.rowItem, { marginTop: 10 }]}>
              <View style={[styles.rowItem]}>
                <Image source={Images.starYellow} style={styles.rowItemIcon} />
                <Text style={styles.rowItemText}>{this.state.dish.testimonial.avg_rate_mark}</Text>
              </View>
              <View style={[styles.rowItem, { marginLeft: 10 }]}>
                <Image source={Images.comment} style={styles.rowItemIcon} />
                <Text style={[styles.itemRowCommentText, { marginRight: 0 }]}>{this.state.dish.testimonial.total_rated_count}</Text>
              </View>
            </View>
            <View style={[styles.rowItem, { marginTop: 10 }]}>
              <View style={[styles.rowItem, { flex: 1 }]}>
                <Image source={Images.clock} style={styles.rowItemIcon} />
                <Text style={styles.rowItemText}>{this.state.dish.preparing_time} min.</Text>
              </View>
              <Text style={styles.rowItemPrice}>{this.state.dish.price.currency} {this.state.dish.price.amount}</Text>
            </View>
          </View>
          {
            Object.entries(this.state.dish.modules).map((item) => {
              const module_id = item[0]
              const module_limit = item[1].limit_selection>0?item[1].limit_selection:1
              
              if(options[module_id]==undefined)
                options[module_id] = []

              let sum = 0
              for( let i = 0 ; i < options[module_id].length ; i++)
                sum += options[module_id][i]
              
              return (
              <View key={item[0]}>
                <Hr marginTop={20} marginLeft={0} marginRight={0} />
                <View style={styles.content}>
                  <Text style={styles.listTitle}>{item[1].name}</Text>
                  {
                    Object.entries(item[1].options).map((item) => {
                      if(options[module_id][item[0]]==undefined)
                        options[module_id][item[0]] = 0
                      return (
                      <TouchableOpacity
                        key={item[0]}
                        style={[styles.rowItem, { marginTop: 10 }]}
                        activeOpacity={0.7}
                        onPress={() => {
                          options[module_id][item[0]] = 1 - options[module_id][item[0]]
                          if(options[module_id][item[0]] == module_limit == 1)
                          {
                            for( let i = 0 ; i < options[module_id].length ; i++)
                              if (i != item[0])
                                options[module_id][i] = 0
                          }
                          this.setState({ optionStatus: options })
                        }}
                        disabled={options[module_id][item[0]]==0 && module_limit>1 && sum == module_limit ?true:false}
                      >
                        <View style={[styles.rowItem, options[module_id][item[0]]==0 && module_limit>1 && sum == module_limit ? { opacity: 0.25 } : { }]}>
                          <Image source={options[module_id][item[0]] === 1 ? Images.radioSelected : Images.radio} style={styles.radioIcon} />
                          <Text style={styles.listLabel}>{item[1].name}</Text>
                          <Text style={styles.listPrice}>{item[1].price.amount>0?"+":"-"} {item[1].price.currency} {item[1].price.amount>0?item[1].price.amount:-item[1].price.amount}</Text>
                        </View>
                      </TouchableOpacity>
                      )
                    })
                  }
                </View>
              </View>
            )})
          }
          <View>
            <Hr marginTop={20} marginLeft={0} marginRight={0} />
            <View style={styles.content}>
              <Text style={styles.listTitle}>Additional Notes</Text>
              <View style={styles.inputWrap}>
                <TextInput
                  style={styles.input}
                  value={String(this.state.additional_notes)}
                  onChangeText={(additional_notes) => this.setState({ additional_notes })}
                  underlineColorAndroid='transparent'
                  placeholder='Please add your comments here.'
                  placeholderTextColor='#BDBDBD'
                  multiline
                  minHeight={50}
                />
              </View>
            </View>
          </View>
        </KeyboardAwareScrollView>
        <KeyboardAvoidingView
          style={styles.footer}
          behavior='padding'
          enabled={Platform.select({ ios: true, android: false })}
          keyboardVerticalOffset={90}
        >
          <TouchableOpacity
            style={styles.btnOrder}
            activeOpacity={0.7}
            onPress={() => {
              var total_price = this.state.dish.price.amount
              var options = []
              if(this.state.dish.modules)
                Object.entries(this.state.dish.modules).map((item) => {
                  const module_id = item[0]
                  Object.entries(item[1].options).map((item) => {
                    if(this.state.optionStatus[module_id][item[0]] == 1){
                      options.push(this.state.dish.modules[module_id].options[item[0]])
                      total_price += this.state.dish.modules[module_id].options[item[0]].price.amount
                    }
                  })
                })
              
              const orderData = {
                name: this.state.dish.name,
                price: this.state.dish.price,
                note: this.state.additional_notes,
                options: options,
                total_price: total_price
              }
              this.props.orderSuccess(orderData)
              Actions.pop()
            }}
          >
            <Text style={styles.btnOrderText}>Add to Order</Text>
          </TouchableOpacity>
          <SafeAreaView forceInset={{ bottom: 'always' }} />
        </KeyboardAvoidingView>
      </View>
    )
  }

  renderViewMore (onPress) {
    return (
      <Text style={styles.txtReadMore} suppressHighlighting onPress={onPress}>read more</Text>
    )
  }
  renderViewLess (onPress) {
    return (
      <Text style={styles.txtReadMore} suppressHighlighting onPress={onPress}>view less</Text>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RestaurantItemScreen)
