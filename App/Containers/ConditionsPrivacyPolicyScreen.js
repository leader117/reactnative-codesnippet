import React, { Component } from 'react'
import {
  Text,
  View,
  TouchableOpacity,
  ScrollView
} from 'react-native'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/ConditionsPrivacyPolicyScreenStyle'
import { Actions } from 'react-native-router-flux'
import { Hr, FullButton } from '../Components'
import { Images } from '../Themes'
import { Image } from 'react-native-animatable'
import SafeAreaView from 'react-native-safe-area-view'

class ConditionsPrivacyPolicyScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      // textWrongPw: 'Oops! Wrong password. Try again!',
      // textHeader: 'Please type your password here.',
    }
  }

  render () {
    return (
      <View style={styles.container} contentContainerStyle={{ paddingBottom: 50 }}>
        <Image source={Images.paymentApproved} style={styles.paymentApproved} />
        <View style={styles.squareView}>
          <Text style={styles.txtPlaceHolder}>
            By clicking “accept” below, you agree to Plate’s Terms {'\n'}and Conditions and acknowledge that you have read the Privacy Policy.
          </Text>
          <Text style={styles.txtPolicy}>
            To learn more, see our
            <Text
              onPress={() => Actions.termsAndConditions()}
              suppressHighlighting style={styles.btnTxtPolicy}> Terms and Conditions </Text>
            and
            <Text
              onPress={() => Actions.privacyPolicy()}
              suppressHighlighting style={styles.btnTxtPolicy}> Privacy Policy. </Text>
          </Text>
        </View>
        <FullButton
          text='Accept'
          styles={styles.btnAddPayment}
          onPress={Actions.addPayment}
        />
        <SafeAreaView forceInset={{ bottom: 'always' }} />
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ConditionsPrivacyPolicyScreen)
