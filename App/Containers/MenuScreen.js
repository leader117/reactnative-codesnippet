import React, { Component } from 'react'
import { ScrollView, Text, View, TextInput, KeyboardAvoidingView, TouchableOpacity, Image } from 'react-native'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/LoginScreenStyle'
import { Images } from '../Themes'
import Ionicons from 'react-native-vector-icons/Ionicons'
import { Actions } from 'react-native-router-flux'

class MenuScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      // textWrongPw: 'Oops! Wrong password. Try again!',
      // textHeader: 'Please type your password here.',
      phoneNumber: 'x'
    }
  }
  render () {
    return (
      <View style={styles.container}>
        <Text style={styles.discover}>Discover</Text>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MenuScreen)
