import React, { Component } from 'react'
import { ScrollView, Text, Image, View, TouchableOpacity, FlatList } from 'react-native'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/RestaurantTableScreenStyle'
import { Hr, CardView } from '../Components'
import { Images, Metrics, Colors } from '../Themes'
import SafeAreaView from 'react-native-safe-area-view'
import Ionicons from 'react-native-vector-icons/Ionicons'
import Feather from 'react-native-vector-icons/Feather'
import { Actions } from 'react-native-router-flux'
import Accordion from 'react-native-collapsible/Accordion'
import Modal from 'react-native-modal'

class RestaurantTableScreen extends Component {
  state = {
    activeSections: [0]
  }

  componentDidMount () {

  }

  setSections = sections => {
    this.setState({
      activeSections: sections.includes(undefined) ? [] : sections
    })
  };

  renderHeader = (section, _, isActive) => {
    return (
      <View>
        <View style={[styles.content]}>
          <View style={[styles.rowTable, { marginVertical: 15 }]}>
            <Text style={[section.user ? styles.rowTableText : styles.rowTableTextBold, { flex: 1 }]}>
              {section.user || 'Your orders'}:
            </Text>
            <Text style={[styles.rowTableText, { marginHorizontal: 5 }]}>AED {section.total}</Text>
            <Ionicons name={isActive ? 'ios-arrow-down' : 'ios-arrow-forward'} size={20} color='#0F1524' />
          </View>
        </View>
        <Hr marginTop={0} marginLeft={0} marginRight={0} lineHeight={isActive ? 5 : 1} />
      </View>
    )
  }

  renderContent (section, _, isActive) {
    return (
      <View>
        <FlatList
          scrollEnabled={false}
          data={section.items}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item }) => {
            return (
              <View style={styles.itemWrap}>
                <View style={styles.rowItem}>
                  <Text style={styles.rowItemName}>{item.name}</Text>
                  {item.status === 'served' ? <Feather name='check' size={20} color='#8ABE2E' /> : <Feather name='clock' size={20} color={'#0F1524'} />}
                </View>
                <View style={[styles.rowItem, { marginTop: 5 }]}>
                  <Image source={Images.starYellow} style={styles.rowHeaderIcon} />
                  <Text style={styles.rowHeaderText}>{item.rating}</Text>
                </View>
                <View style={[styles.rowItem, { marginTop: 5 }]}>
                  <Text style={styles.rowItemPrice}>AED {item.price}</Text>
                  <Text style={styles.rowItemStatus}>
                    {item.status === 'served' ? 'Served' : item.status === 'being_prepared' ? 'Being Prepared' : ''}
                  </Text>
                </View>
              </View>
            )
          }}
        />
        <Hr marginTop={0} marginLeft={0} marginRight={0} lineHeight={isActive ? 4 : 1} />
      </View>
    )
  }

  render () {
    const users = [
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSKoh_wxk-fkGGHm4pP_Mwe6v-P6weOYRpuchqAu0K0VYoDj4AVQg',
      'https://www.microsoft.com/en-us/research/wp-content/uploads/2017/11/avatar_user_36692_1511968805.jpg',
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTSqDHn4or8swOCuG_1Ic3Z65t__sXp8NfeD8y-aVH-wbOZSh6p'
    ]
    const sections = [
      {
        user: null,
        total: 69.71,
        items: [
          {
            name: 'Dynamite Shrimp',
            price: 56.80,
            rating: 4.2,
            status: 'being_prepared'
          },
          {
            name: 'Crap Wontons',
            price: 56.80,
            rating: 4.2,
            status: 'served'
          }
        ]
      },
      {
        user: 'Brain',
        total: 169.71,
        items: [
          {
            name: 'Dynamite Shrimp',
            price: 56.80,
            rating: 4.2,
            status: 'being_prepared'
          },
          {
            name: 'Crap Wontons',
            price: 56.80,
            rating: 4.2,
            status: 'served'
          }
        ]
      }
    ]
    return (
      <View style={styles.container}>
        <ScrollView contentContainerStyle={styles.scrollView}>
          <View style={[styles.content]} >
            <TouchableOpacity
              style={[styles.contentRowUser, { marginLeft: 10 }]}
              activeOpacity={0.7}
              onPress={() => Actions.restaurantInviteFriend()}
            >
              {
                users.map((item, index) => (
                  <Image key={index} source={{ uri: item }} style={styles.contentRowUserAvatar} />
                ))
              }
              <View style={styles.contentRowUserBtn}>
                <Ionicons name='md-add' color='#696E82' size={25} />
              </View>
            </TouchableOpacity>
            <Text style={styles.rowUserText}>You and {users.length} other</Text>
          </View>
          <Hr marginTop={20} marginLeft={0} marginRight={0} />
          <Accordion
            activeSections={this.state.activeSections}
            sections={sections}
            touchableComponent={TouchableOpacity}
            renderHeader={this.renderHeader}
            renderContent={this.renderContent}
            duration={400}
            onChange={this.setSections}
          />
        </ScrollView>
        <TouchableOpacity
          style={styles.btnBill}
          activeOpacity={0.7}
          onPress={() => Actions.restaurantBillScreen({ title: this.props.title })}
        >
          <Text style={styles.btnBillText}>The Bill</Text>
        </TouchableOpacity>
        <SafeAreaView forceInset={{ bottom: 'always' }} />
        <Modal
          isVisible={this.state.showModalOrderTime}
          style={styles.bottomModal}
          onBackdropPress={() => this.setState({ showModalOrderTime: false })}
        >
          <View style={styles.modalContent}>
            <View style={styles.modalRowHeader}>
              <Text style={styles.modalRowHeaderText}>Polpeti Di Carne</Text>
              <TouchableOpacity
                activeOpacity={0.7}
                onPress={() => this.setState({ showModalOrderTime: false })}
              >
                <Ionicons name='ios-close' size={25} color='#292D44' style={{ marginHorizontal: 20, marginVertical: 10 }} />
              </TouchableOpacity>
            </View>
            <Text style={styles.txtContent}>Your order is being prepaired and will be served to you soon.</Text>
            <View style={styles.rowContent}>
              <Text style={styles.rowContentLabel}>Order time:</Text>
              <Text style={styles.rowContentTime}>15:23</Text>
            </View>
            <View style={styles.rowContent}>
              <Text style={styles.rowContentLabel}>Serving time:</Text>
              <Text style={styles.rowContentTime}>15:43</Text>
            </View>
            <TouchableOpacity
              style={styles.btnCancel}
              activeOpacity={0.7}
            >
              <Text style={styles.btnCancelText}>Cancel my order</Text>
            </TouchableOpacity>
            <SafeAreaView forceInset={{ bottom: 'always' }} />
          </View>
        </Modal>
        <Modal
          isVisible={this.state.showModalOrderAdd}
          style={styles.bottomModal}
          onBackdropPress={() => this.setState({ showModalOrderAdd: false })}
        >
          <View style={styles.modalContent}>
            <View style={[styles.modalRowHeader, { marginBottom: 20 }]}>
              <Text style={styles.modalRowHeaderText}>Polpeti Di Carne</Text>
              <TouchableOpacity
                activeOpacity={0.7}
                onPress={() => this.setState({ showModalOrderAdd: false })}
              >
                <Ionicons name='ios-close' size={25} color='#292D44' style={{ marginHorizontal: 20, marginVertical: 10 }} />
              </TouchableOpacity>
            </View>
            {
              ['Me', 'Bruce O’connor', 'Jennifer Oquendo'].map((item, index) => (
                <TouchableOpacity
                  key={index}
                  style={[styles.rowContent]}
                  activeOpacity={0.7}
                  onPress={() => {}}
                >
                  <Image source={index === 0 ? Images.radioSelected : Images.radio} style={styles.radioIcon} />
                  <Text style={[styles.rowContentLabel, { marginHorizontal: 8 }]}>{item}</Text>
                </TouchableOpacity>
              ))
            }
            <TouchableOpacity
              style={[styles.btnBill, { marginTop: 50 }]}
              activeOpacity={0.7}
            >
              <Text style={[styles.btnBillText]}>Add</Text>
            </TouchableOpacity>
            <SafeAreaView forceInset={{ bottom: 'always' }} />
          </View>
        </Modal>
      </View>
    )
  }

  renderViewMore (onPress) {
    return (
      <Text style={styles.txtReadMore} suppressHighlighting onPress={onPress}>read more</Text>
    )
  }
  renderViewLess (onPress) {
    return (
      <Text style={styles.txtReadMore} suppressHighlighting onPress={onPress}>view less</Text>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RestaurantTableScreen)
