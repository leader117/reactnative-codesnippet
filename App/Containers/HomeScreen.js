import React, { Component } from 'react'
import {
  Text,
  TouchableOpacity,
  Image,
  ImageBackground,
  View,
  Alert,
  SectionList,
  Animated
} from 'react-native'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/HomeScreenStyle'
import { Images, Colors, Metrics } from '../Themes'
import { Actions } from 'react-native-router-flux'
import { CardView } from '../Components'
import RNGooglePlaces from 'react-native-google-places'
import Permissions from 'react-native-permissions'
import { bindActionCreators } from 'redux'
import ProfileActions from '../Redux/ProfileRedux'
import SafeAreaView from 'react-native-safe-area-view'
import Api from '../Services/Api'
import Menu from 'react-native-material-menu'
import { FriendActions, RestaurantActions } from '../Redux/Actions'
import firebase from 'react-native-firebase'

const api = Api.create()

class HomeScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      text: '',
      service_type: 'dine-in',
      sort_type: 'plate-cash',
      scrollY: new Animated.Value(0),
      restaurants: [],
      geo_loc: null
    }
  }

  componentDidMount = async () => {
    await this.getGeocode()
    if (!this.props.profile.location || !this.props.profile.location.city) {
      await this.getUserPlace()
    }
    if( this.state.geo_loc ){
      await this.getRestaurantData()
    }

    await this.props.getFriends()
  }

  async getRestaurantData(){
    const token = await firebase.auth().currentUser.getIdToken()
    await api.setToken(token)
    if( !this.state.geo_loc )
      await this.getGeocode()
    if( this.state.geo_loc ){
    const response = await api.searchRestaurant({
      'service_type': this.state.service_type,
      'sort_type': this.state.sort_type,
      '_loc': {
        lat: this.state.geo_loc.latitude,
        lon: this.state.geo_loc.longitude
      }
    })
    if(response && response.data && response.data.data)
      this.setState({restaurants: response.data.data})
    return true
    }
  }

  async getGeocode () {
    if(this.state.geo_loc && this.state.geo_loc.latitude && this.state.geo_loc.longitude)
      return false
    else{
      let result = await Permissions.check('location', { type: 'whenInUse' })
      console.log("location check result: ", result)
      if (result === 'undetermined') {
        result = await Permissions.request('location', { type: 'whenInUse' })
        console.log("location request result: ", result)
        if (result !== 'authorized') {
          return false
        }
      } else if (result === 'denied') {
        Alert.alert('Plate requires to access to your location')
        return false
      }
      
      const places = await RNGooglePlaces.getCurrentPlace()
      if (!places.length) {
        return false
      }
      const place = places[0]
      this.setState({geo_loc: place})
      return true
    }
  }

  async getUserPlace () {
    const place = this.state.geo_loc
    const placeResponse = await api.getLocation(place)
    __DEV__ && console.log('placeResponse', placeResponse)
    if (!placeResponse.ok) {
      Alert.alert('Can not get your location')
      return false
    }
    let city = ''
    let neighborhood = ''
    let country = ''
    for (let result of placeResponse.data.results) {
      for (let addressComponent of result.address_components) {
        if (addressComponent.types.includes('country') && !country) {
          country = addressComponent.short_name
        }
        if (addressComponent.types.includes('locality') && !city) {
          city = addressComponent.short_name
        }
        if (addressComponent.types.includes('neighborhood') && !neighborhood) {
          neighborhood = addressComponent.short_name
        }
      }
    }
    const address = []
    neighborhood && address.push(neighborhood)
    city && address.push(city)
    if (city || neighborhood) {
      this.props.updateProfile({
        geo_loc: {
          lat: neighborhood.latitude,
          lon: neighborhood.longitude
        },
        location: {
          city,
          neighborhood,
          country
        }
      })
    }
  }

  onInputFocus = () => {
    this.refs.search && this.refs.search.blur()
    Actions.search({geo_loc: this.state.geo_loc})
  }

  _showRestaurant (res_data) {
    this.props.profile ? Actions.restaurant({data: res_data, title: res_data.name.toUpperCase()}) : Actions.introduce()
  }

  _renderHeaderAnimate () {
    if (this.state.showHeaderAnimate) {
      return (
        <View animation='slideInDown' style={styles.headerAnimate}>
          <ImageBackground source={Images.backgroundHomeHeader} style={[styles.bgHomeHeader, { height: 50 }]}>
            <TouchableOpacity style={[styles.btnSearch, { bottom: 10, height: 35 }]} activeOpacity={1} onPress={this.onInputFocus}>
              <Text style={[styles.textInput, { marginLeft: 10 }]}>
                Search
              </Text>
            </TouchableOpacity>
          </ImageBackground>
        </View>
      )
    }
  }

  hideMenu = () => {
    this._menu.hide()
  };

  showMenu = () => {
    this._menu.show()
  };

  render () {
    const data = [{ type: 'header' }].concat(this.state.restaurants)
    const sections = [
      { title: '', data: data }
    ]
    const { profile } = this.props
    const dayTime = new Date().getHours() < 12 ? 'morning' : new Date().getHours() < 18 ? 'afternoon' : 'Evening'
    const location = profile && profile.location && (profile.location.neighborhood || profile.location.city || profile.location.country || 'Detect your location')
    const opacity = this.state.scrollY.interpolate({
      inputRange: [0, 60],
      outputRange: [1, 0],
      extrapolate: 'clamp'
    })
    return (

      <View style={styles.container}>
        <Image source={Images.backgroundHomeHeader} style={[styles.bgHomeHeader, { height: Metrics.navBarHeight }]} />
        <View style={[styles.header, { position: 'absolute' }]}>
          <ImageBackground source={Images.backgroundHomeHeader} style={styles.bgHomeHeader}>
            <Animated.View style={[styles.warpHello, { opacity }]}>
              {profile && profile.user_meta
                ? <Text style={styles.txtHello}>Good {dayTime.toLowerCase()}, <Text style={styles.txtName}>{profile.user_meta.first_name}!</Text></Text>
                : <Text style={styles.txtHello}>Good {dayTime.toLowerCase()}!</Text>
              }
              <Text style={styles.askEat}>What would you like to eat today?</Text>
            </Animated.View>
          </ImageBackground>
        </View>
        <SectionList
          onScroll={Animated.event(
            [{ nativeEvent: { contentOffset: { y: this.state.scrollY } } }]
          )}
          contentContainerStyle={{ paddingBottom: 10 }}
          bounces={false}
          stickySectionHeadersEnabled
          sections={sections}
          keyExtractor={(item, index) => index.toString()}
          ListHeaderComponent={() => (
            <View style={[styles.header, { height: 220 - Metrics.navBarHeight }]}>
              {/* <ImageBackground source={Images.backgroundHomeHeader} style={styles.bgHomeHeader}>
                    <Animated.View style={[styles.warpHello, { opacity }]}>
                      <Text style={styles.txtHello}>Good {dayTime}, <Text style={styles.txtName}>{profile.user_meta.first_name}!</Text></Text>
                      <Text style={styles.askEat}>What would you like to eat today?</Text>
                    </Animated.View>
                  </ImageBackground> */}
            </View>
          )}
          renderSectionHeader={({ section }) => (
            <View style={styles.searchWrap}>
              <Image source={Images.backgroundHomeHeader} style={[styles.bgHomeHeader, { height: 30 }]} />
              <TouchableOpacity style={styles.btnSearch} activeOpacity={1} onPress={this.onInputFocus}>
                <Image source={Images.icSpoonPlate} style={styles.icSpoonPlate} />
                <Text style={styles.textInput}>
                  Search Restaurants
                </Text>
              </TouchableOpacity>
            </View>
          )}
          renderItem={({ item }) => {
            if (item.type === 'header') {
              return (
                <View style={{ backgroundColor: '#F7F8FA' }}>
                  <View style={styles.rowButtonDinePick}>
                    <TouchableOpacity
                      style={[styles.btnDine, this.state.service_type !== 'dine-in' ? { borderColor: '#737373' } : null]}
                      onPress={() => {
                        this.setState({ service_type: 'dine-in' })
                        this.getRestaurantData()
                      }}
                    >
                      <Text
                        style={[styles.txtBtnDine, this.state.service_type === 'dine-in' ? { color: Colors.active } : null]}
                      >
                        Dine-in
                      </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={[styles.btnDine, this.state.service_type === 'dine-in' ? { borderColor: '#737373' } : null]}
                      onPress={() => {
                        this.setState({ service_type: 'pick-up' })
                        this.getRestaurantData()
                      }}
                    >
                      <Text
                        style={[styles.txtBtnDine, this.state.service_type === 'pick-up' ? { color: Colors.active } : null]}
                      >
                        Pick-up
                      </Text>
                    </TouchableOpacity>
                  </View>
                  <View style={styles.rowHeaderPlace}>
                    <View style={styles.rowHeaderPlaceWrap}>
                      <Menu
                        ref={ref => { this._menu = ref }}
                        button={
                          <TouchableOpacity
                            style={styles.rowMenu}
                            onPress={this.showMenu}
                          >
                            <Text style={[styles.rowMenuTitle, { color: '#0F1524', paddingLeft: 5 }]}>
                              {this.state.sort_type === 'plate-cash' ? 'Plate Cash' : 'Places Nearby'}
                            </Text>
                            <Image source={Images.arrowDropdown} />
                          </TouchableOpacity>
                        }
                        style={{ borderRadius: 14 }}
                      >
                        <View style={styles.menuItem}>
                          <TouchableOpacity
                            style={styles.rowMenuItem}
                            onPress={() => {
                              this.setState({ sort_type: 'plate-cash' })
                              this.hideMenu()
                              this.getRestaurantData()
                            }}
                          >
                            <Text style={[styles.rowMenuTitle, this.state.sort_type === 'plate-cash' ? { color: '#0F1524' } : {}]}>
                              Plate Cash
                            </Text>
                            <Image source={Images.arrowDropdown} style={{
                              transform: [{
                                rotate: '180deg'
                              }]
                            }} />
                          </TouchableOpacity>
                          <TouchableOpacity
                            style={[styles.rowMenuItem]}
                            onPress={() => {
                              this.setState({ sort_type: 'near-by' })
                              this.hideMenu()
                              this.getRestaurantData()
                            }}
                          >
                            <Text style={[styles.rowMenuTitle, this.state.sort_type === 'near-by' ? { color: '#0F1524' } : {}]}>
                              Places Nearby
                            </Text>
                          </TouchableOpacity>
                        </View>
                      </Menu>
                    </View>
                    <View style={styles.rowHeaderPlaceWrap}>
                      {!!location && <View style={styles.txtLocation}>
                        <Image source={Images.icLocationSmall} style={styles.icLocation} />
                        <TouchableOpacity onPress={() => this.getUserPlace()}>
                          <Text numberOfLines={1} style={styles.txtPlace}>{location}</Text>
                        </TouchableOpacity>
                      </View>}
                    </View>
                  </View>
                </View>
              )
            } else {
              return (
                <View style={{ backgroundColor: '#F7F8FA' }}>
                  <View style={styles.cardWrap}>
                    <TouchableOpacity onPress={() => this._showRestaurant(item)} style={styles.cardLocation}>
                      <Image style={styles.restaurantImage} source={{uri: item.logo_url}} />
                      <Text style={styles.restaurantName}>{item.name}</Text>
                      {
                        item.discount ? (
                          <View style={styles.discountWrap}>
                            <Text style={styles.discountText}>{item.discount}%</Text>
                          </View>
                        ) : null
                      }
                      <Text style={[styles.txtAddress, { color: '#BDBDBD' }]}>{(item.distance||0).toFixed(1)}km</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              )
            }
          }}
          ListFooterComponent={() => <SafeAreaView forceInset={{ bottom: 'always' }} />}
        />

      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    profile: state.profile.profile,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    ...bindActionCreators(ProfileActions, dispatch),
    ...bindActionCreators(RestaurantActions, dispatch),
    ...bindActionCreators(FriendActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen)
