import React, { Component } from 'react'
import { ScrollView, Text, Image, View, TouchableOpacity, FlatList } from 'react-native'
import { connect } from 'react-redux'
import styles from './Styles/RestaurantBillScreenStyle'
import { Hr } from '../Components'
import { Images,  Colors, Fonts } from '../Themes'
import SafeAreaView from 'react-native-safe-area-view'
import Ionicons from 'react-native-vector-icons/Ionicons'
import { Actions } from 'react-native-router-flux'
import Accordion from 'react-native-collapsible/Accordion'
import Modal from 'react-native-modal'
import * as Animatable from 'react-native-animatable'
import firebase from 'react-native-firebase'

const db = firebase.database()

class RestaurantBillScreen extends Component {

  constructor(props) {
    super(props)
    
    var table_users = []

    if(this.props.order && this.props.order.host_user_id)
      table_users.push( [this.props.order.host_user_id , {user_meta: null}])
    if(this.props.order && this.props.order.users){
      const arr = Object.entries(this.props.order.users)
      for(let i = 0; i < arr.length ; i++){
        if(arr[i][1].status == "accepted")
          table_users.push(arr[i])
      }
    }

    this.state = {
      activeSections: [0],
      indexAdd: 0,
      stateCard: true,
      order: this.props.order,
      order_id: this.props.order_id,
      promotion: null,
      plate_cash: null,
      selectedBillingType: 0,
      table_users: table_users,
      discount: this.props.discount,
      billed: false
    }
  }

  componentDidMount () {
    if(this.state.order_id){
      db.ref(`/orders/${this.state.order_id}`).on("value", (snapshot) => {
        if(snapshot.exists()){
          this.setState({order: snapshot.val()})
          var table_users = []

          if(this.state.order && this.state.order.host_user_id)
            table_users.push( [this.state.order.host_user_id , {user_meta: null}])
          if(this.state.order && this.state.order.users){
            const arr = Object.entries(this.state.order.users)
            for(let i = 0; i < arr.length ; i++){
              if(arr[i][1].status == "accepted")
                table_users.push(arr[i])
            }
          }
          this.setState({table_users: table_users})
        }
      })
    }
    const uid = firebase.auth().currentUser.uid
    db.ref(`/users/${uid}/promotion`).on("value", snapshot => {
      const val = snapshot.val()
      if(val && val.promotion_id && val.status == "active"){
        db.ref(`/promotions/${val.promotion_id}`).once("value").then(snapshot => {
          this.setState({promotion: snapshot.val()})
        })
      } else
      this.setState({promotion: null})
    })
    db.ref(`/users/${uid}/plate_cash`).on("value", snapshot => {
      this.setState({plate_cash: snapshot.val()})
    })
  }

  onPayYourOrderPressed(){
    this.setState({ showModalPayOrder: true, selectedBillingType:1 })
  }

  onPaySplitPressed(){
    const uid = firebase.auth().currentUser.uid
    if(this.state.order.bill_info && this.state.order.bill_info.method == 2){
      if(this.state.order.bill_info.confirm[uid] == true){
        var all_confirmed = true
        for(let i = 0; i < this.state.table_users.length; i++){
          if(this.state.order.bill_info.confirm[this.state.table_users[i][0]] != true){
            all_confirmed = false
            break
          }
        }
        if(all_confirmed)
          this.setState({selectedBillingType : 2, showModalPayOrder: true })
        else
          this.setState({ showModalWaiting: true })
      } else
        this.setState({ showModalSplitOther: true })
    }
    else{
      this.setState({ showModalSplit: true })
    }
  }

  onPayEntirePressed(){
    const uid = firebase.auth().currentUser.uid
    if(this.state.order.bill_info && this.state.order.bill_info.method == 3 && this.state.order.bill_info.paid_user_id != uid){
      this.setState({ showModalPaidByOther: true })
    } else{
      this.setState({ showModalPayOrder: true, selectedBillingType:3 })
    }
  }

  onPay(total_price, promo_price, available_plate_cash){
    const uid = firebase.auth().currentUser.uid
    var plate_cash = 0
    if(this.state.stateCard){
      if(total_price - promo_price > available_plate_cash)
        plate_cash = available_plate_cash
      else
        plate_cash = total_price - promo_price
    }
    var plate_cash_earned = 0
    if(this.state.discount){
      plate_cash_earned = total_price*this.state.discount/100
      this.setState({plate_cash_earned: plate_cash_earned})
    }
    db.ref(`users/${uid}/plate_cash`).once('value', (snapshot) => {
      if(snapshot.val())
        db.ref(`users/${uid}/plate_cash`).update({amount: snapshot.val().amount - plate_cash + plate_cash_earned})
    })
    const payment_id = db.ref('payments').push({
      order_id: this.state.order_id,
      total_price: total_price,
      promo_price, promo_price,
      plate_cash: plate_cash,
      billed: total_price - promo_price - plate_cash,
      created_at: (new Date()).getTime(),
      user_id: uid
    }).key

    if(this.state.promotion && this.state.stateCard){
      db.ref(`/users/${uid}/promotion`).update({status: "applied"})
    }
    
    var data = {}
    data["method"] = this.state.selectedBillingType
    if(this.state.selectedBillingType == 3){
      data["paid_user_id"] = uid
      data["paid_user_meta"] = this.props.profile.user_meta
    }
      
    db.ref(`orders/${this.state.order_id}/bill_info`).update(data)
    db.ref(`users/${uid}/order_history/${this.state.order_id}`).update({status: "closed", ended_at: (new Date()).getTime(), payment_id: payment_id})
    this.setState({billed: true})
  }

  onConfirm(){
    const uid = firebase.auth().currentUser.uid
    var data = {}
    data["method"] = 2
    db.ref(`orders/${this.state.order_id}/bill_info`).update(data)
    db.ref(`orders/${this.state.order_id}/bill_info/confirm/${uid}`).set(true)
  }

  onCancel(){
    db.ref(`orders/${this.state.order_id}/bill_info`).remove()
  }

  componentWillUnmount() {
    const uid = firebase.auth().currentUser.uid
    db.ref(`/orders/${this.state.order_id}`).off()
    db.ref(`/users/${uid}/promotion`).off()
  }

  setSections = sections => {
    this.setState({
      activeSections: sections.includes(undefined) ? [] : sections
    })
  };
  
  renderHeader = (section, _, isActive) => {
    const bill = section[1]
    const user_id = section[1]
    return (
      <View>
        <View style={[styles.content]}>
          <View style={[styles.rowTable, { marginVertical: 15 }]}>
            <Text style={[section.user ? styles.rowTableText : styles.rowTableTextBold, { flex: 3 }]}>
              {section.user? section.user.first_name + " " + section.user.last_name: 'Your orders'}:
            </Text>
            <View style={styles.rightHeader}>
              <Text style={[styles.rowTableText, { marginHorizontal: 5 }]}>AED {section.total}</Text>
              <Ionicons name={isActive ? 'ios-arrow-down' : 'ios-arrow-forward'} size={20} color='#0F1524' />
            </View>
          </View>
        </View>
        <Hr marginTop={0} marginLeft={0} marginRight={0} lineHeight={5} />
      </View>
    )
  }

  renderContent = (section, _, isActive) => {
    return (
      <View>
        <FlatList
          style={{ height: section.items.length * 66 }}
          scrollEnabled={false}
          data={section.items}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item, index }) => {
            const dish = item[1]
            return (
              <TouchableOpacity
                style={styles.itemWrap}
                activeOpacity={0.7}
              >
                <View style={[styles.rowItem, { justifyContent: 'space-between' }]}>
                  <View style={styles.rowItem}>
                    <Text style={styles.textNumber}>1x</Text>
                    <Text style={styles.rowItemName}>{dish.name}</Text>
                  </View>
                  <Text style={styles.rowItemPrice}>AED {(dish.total_price|0).toFixed(2)}</Text>
                </View>
              </TouchableOpacity>
            )
          }}
        />
        <Hr marginTop={0} marginLeft={0} marginRight={0} lineHeight={isActive ? 4 : 1} />
      </View>
    )
  }

  render () {
    const uid = firebase.auth().currentUser.uid
    var current_user_bill
    var sections = []
    if(this.state.order.bill){
      if(this.state.order.bill[uid]){
        sections.push({ total: this.state.order.bill[uid].total_price, user: null, items: Object.entries(this.state.order.bill[uid].dishes) })
        current_user_bill = this.state.order.bill[uid]
      }
      const billArray = Object.entries(this.state.order.bill)
      for( let i = 0 ; i < billArray.length ; i++){
        const user_id = billArray[i][0]
        const user_bill = billArray[i][1]
        if(user_id != uid){
          sections.push({
            total: user_bill.total_price,
            user: user_id==this.state.order.host_user_id?this.state.order.host_user_meta:this.state.order.users[user_id].user_meta,
            items: Object.entries(user_bill.dishes)
          })
        }
      }
    }

    var bill_method = 0
    var total_price = 0

    total_price = current_user_bill && current_user_bill.total_price ? current_user_bill.total_price : 0
    if(this.state.selectedBillingType == 3)
      total_price =  this.state.order.bill_total_price
    if(this.state.order.bill_info && this.state.order.bill_info.method){
      bill_method = this.state.order.bill_info.method
      if( bill_method == 2 && this.state.selectedBillingType == 2){
        total_price = (this.state.order.bill_total_price / this.state.table_users.length).toFixed(2)
      }
    }

    var promotion_amount = 0
    if(this.state.promotion && this.state.promotion.status=="active" && this.state.promotion.duration.start < (new Date()).getTime() &&  this.state.promotion.duration.end > (new Date()).getTime() ){
      if (this.state.promotion.type == 1)
        promotion_amount = this.state.promotion.amount
      else if(this.state.promotion.type == 2)
        promotion_amount = total_price * this.state.promotion.amount / 100
    }

    var plate_cash_amount = 0
    if(this.state.plate_cash && this.state.plate_cash.amount)
      plate_cash_amount = this.state.plate_cash.amount

    return (
      <View style={styles.container}>
        <ScrollView contentContainerStyle={[styles.scrollView, { paddingBottom: this.state.footerHeight || 0 }]}>
          <Accordion
            activeSections={this.state.activeSections}
            sections={sections}
            touchableComponent={TouchableOpacity}
            renderHeader={this.renderHeader}
            renderContent={this.renderContent}
            duration={400}
            onChange={this.setSections}
          />
          <View style={styles.content}>
            <View style={[styles.rowTable, { marginTop: 25 }]}>
              <Text style={[styles.rowTableText, { flex: 1 }]}>Total</Text>
              <View style={styles.totalWrap}>
                <Text style={[styles.rowTableText, { marginHorizontal: 5, color: Colors.white }]}>AED {this.state.order.bill_total_price}</Text>
              </View>
            </View>
          </View>
        </ScrollView>
        <Animatable.View animation='slideInUp' style={styles.footer} onLayout={(e) => this.setState({ footerHeight: e.nativeEvent.layout.height })}>
          <SafeAreaView forceInset={{ bottom: 'always' }}>
            <View style={styles.footerContent}>
              <Text style={styles.footerContentTitle}>For what would like to pay?</Text>
              <View style={styles.rowActions}>
                <TouchableOpacity
                  style={[styles.rowActionsWrap, (bill_method!==1 && bill_method !==0) || this.state.billed? {opacity: 0.3}: {}]}
                  activeOpacity={0.7}
                  disabled={(bill_method!==1 && bill_method !==0) || this.state.billed}
                  onPress={() => this.onPayYourOrderPressed()}
                >
                  <Image style={styles.rowActionsIcon} source={Images.iconYourOrder} />
                  <Text style={styles.rowActionText}>Your Order</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={[styles.rowActionsWrap, (bill_method!==2 && bill_method !==0) || this.state.billed? {opacity: 0.3}: {}]}
                  activeOpacity={0.7}
                  disabled={(bill_method!==2 && bill_method !==0) || this.state.billed}
                  onPress={() => this.onPaySplitPressed()}
                >
                  <Image style={styles.rowActionsIcon} source={Images.iconSplit} />
                  <Text style={styles.rowActionText}>Split</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={[styles.rowActionsWrap, (bill_method!==3 && bill_method !==0) || this.state.billed? {opacity: 0.3}: {}]}
                  activeOpacity={0.7}
                  disabled={(bill_method!==3 && bill_method !==0) || this.state.billed}
                  onPress={() => this.onPayEntirePressed()}
                >
                  <Image style={styles.rowActionsIcon} source={Images.iconEntireBill} />
                  <Text style={styles.rowActionText}>Entire Bill</Text>
                </TouchableOpacity>
              </View>
            </View>
          </SafeAreaView>
        </Animatable.View>
        {/* Split Bill Modal */}
        <Modal
          isVisible={this.state.showModalSplit}
          style={styles.bottomModal}
          onBackdropPress={() => this.setState({ showModalSplit: false })}
        >
          <View style={styles.modalContent}>
            <View style={[styles.modalRowHeader]}>
              <Text style={styles.modalRowHeaderText}>Split</Text>
              <TouchableOpacity
                activeOpacity={0.7}
                onPress={() => this.setState({ showModalSplit: false })}
              >
                <Ionicons name='ios-close' size={25} color='#292D44' style={{ marginHorizontal: 20, marginVertical: 10 }} />
              </TouchableOpacity>
            </View>
            <Text style={styles.txtContent}>Split the bill equally with <Text style={[styles.txtContent, { fontSize: 18, fontFamily: Fonts.type.avenirHeavy }]}>{this.state.table_users.length-1}</Text> other friends on your table.</Text>
            <Hr marginTop={15} marginLeft={20} marginRight={20} />
            <View style={[styles.rowCard, { justifyContent: 'space-between' }]}>
              <Text style={[styles.txtContent, { marginHorizontal: 0 }]}>Total:</Text>
              <Text style={[styles.txtContent, { fontSize: 18, fontFamily: Fonts.type.avenirHeavy, marginHorizontal: 0 }]}>AED {this.state.order.bill_total_price.toFixed(2)}</Text>
            </View>
            <TouchableOpacity
              style={[styles.btnBill, { marginTop: 30 }]}
              activeOpacity={0.7}
              onPress={() => {
                this.setState({ showModalSplit: false })
                this.onConfirm()
              }}
            >
              <Text style={[styles.btnBillText]}>Confirm</Text>
            </TouchableOpacity>
            <SafeAreaView forceInset={{ bottom: 'always' }} />
          </View>
        </Modal>
        {/* Get Confirmation from Others for Split Bill */}
        <Modal
          isVisible={this.state.showModalSplitOther}
          style={styles.bottomModal}
          onBackdropPress={() => this.setState({ showModalSplitOther: false })}
        >
          <View style={styles.modalContent}>
            <View style={[styles.modalRowHeader]}>
              <Text style={styles.modalRowHeaderText}>Split</Text>
              <TouchableOpacity
                activeOpacity={0.7}
                onPress={() => this.setState({ showModalSplitOther: false })}
              >
                <Ionicons name='ios-close' size={25} color='#292D44' style={{ marginHorizontal: 20, marginVertical: 10 }} />
              </TouchableOpacity>
            </View>
            <Text style={styles.txtContent}>Looks like your table wants to split the bill.</Text>
            <Hr marginTop={15} marginLeft={20} marginRight={20} />
            <Text style={styles.txtContent}>If you’d like to do so, please click confirm to proceed.</Text>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <TouchableOpacity
                style={[styles.btnBill, { marginTop: 30, flex: 1, marginRight: 19 }]}
                activeOpacity={0.7}
                onPress={() => {
                  this.setState({ showModalSplitOther: false })
                  this.onConfirm()
                }}
              >
                <Text style={[styles.btnBillText]}>Confirm</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[styles.btnBill, { marginTop: 30, flex: 1, marginLeft: 0, backgroundColor: '#eceff4' }]}
                activeOpacity={0.7}
                onPress={() => {
                  this.setState({ showModalSplitOther: false })
                  this.onCancel()
                }}
              >
                <Text style={[styles.btnBillText, { color: '#9b9b9b' }]}>Cancel</Text>
              </TouchableOpacity>
            </View>
            <SafeAreaView forceInset={{ bottom: 'always' }} />
          </View>
        </Modal>
        {/* Pay Order Modal */}
        <Modal
          isVisible={this.state.showModalPayOrder}
          style={styles.bottomModal}
          onBackdropPress={() => this.setState({ showModalPayOrder: false })}
        >
          <View style={styles.modalContent}>
            <View style={[styles.modalRowHeader]}>
              <Text style={styles.modalRowHeaderText}>Payment</Text>
              <TouchableOpacity
                activeOpacity={0.7}
                onPress={() => this.setState({ showModalPayOrder: false })}
              >
                <Ionicons name='ios-close' size={25} color='#292D44' style={{ marginHorizontal: 20, marginVertical: 10 }} />
              </TouchableOpacity>
            </View>
            {
              plate_cash_amount>0? 
              <View style={[styles.rowCard, { justifyContent: 'space-between' }]}>
                <View>
                  <Text style={[styles.txtContent, { marginHorizontal: 0 }]}>Use my <Text style={styles.textPlate}>Plate Cash</Text></Text>
                  <Text style={styles.textAed}>AED {plate_cash_amount.toFixed(2)}</Text>
                </View>
                {this.state.stateCard && <TouchableOpacity
                  onPress={() => this.setState({ stateCard: false })}
                  style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <Image source={Images.icYup} style={{ width: 62, height: 62 }} resizeMode={'contain'} />
                  <Text style={styles.textState}>Yup!</Text>
                </TouchableOpacity>}
                {!this.state.stateCard && <TouchableOpacity
                  onPress={() => this.setState({ stateCard: true })}
                  style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <Image source={Images.icNah} style={{ width: 62, height: 62 }} resizeMode={'contain'} />
                  <Text style={styles.textState}>Nah.</Text>
                </TouchableOpacity>}
              </View> : null
            }
            {
              promotion_amount > 0 ?[
              <Hr marginTop={14} marginLeft={20} marginRight={20} />,
              <View style={[styles.rowCard, { marginTop: 17 }]}>
                <View>
                  <Text style={[styles.txtContent, { marginVertical: 0, marginHorizontal: 0, lineHeight:24 }]}>Promo code <Text style={[styles.txtContent, { fontFamily: Fonts.type.bold }]}>{this.state.promotion?this.state.promotion.code:null}</Text> has been applied.</Text>
                  <Text style={[styles.txtContent, { marginVertical: 0, marginHorizontal: 0, lineHeight:24 }]}>New Total: <Text style={[styles.txtContent, {textDecorationLine:"line-through", color:"#979797" }]}>AED {total_price.toFixed(2)}</Text> <Text style={[styles.txtContent, { fontFamily: Fonts.type.bold }]}>AED {(total_price - promotion_amount).toFixed(2)}</Text></Text>
                </View>
              </View>]
              : null
            }
            <Hr marginTop={14} marginLeft={20} marginRight={20} />
            <View style={styles.rowCard}>
              <Text style={[styles.rowCardText, { flex: 1 }]}>Credit Card</Text>
              <Text
                onPress={() => {
                  // this.setState({ showModalPayOrder: false })
                  // setTimeout(() => {
                  //   Actions.paymentScreen()
                  // }, 500)
                }}
                suppressHighlighting
                style={styles.rowCardText}>
                Change
              </Text>
            </View>
            <Text style={[styles.rowCardTextNumber]}>•••• •••• •••• 0042</Text>
            <Text style={[styles.txtContent, { marginVertical: 15 }]}>You are about to pay <Text style={[styles.txtContent, { fontSize: 18, fontFamily: Fonts.type.avenirHeavy }]}>AED {total_price.toFixed(2)}</Text> with your selected payment option.</Text>
            <TouchableOpacity
              style={[styles.btnBill, { marginTop: 30 }]}
              activeOpacity={0.7}
              onPress={() => {
                this.setState({ showModalPayOrder: false})
                this.onPay(total_price, promotion_amount, plate_cash_amount)
                setTimeout(() => {
                  this.setState({ showModalPayOrderSuccess: true })
                }, 500)
              }}
            >
              <Text style={[styles.btnBillText]}>Pay</Text>
            </TouchableOpacity>
            <SafeAreaView forceInset={{ bottom: 'always' }} />
          </View>
        </Modal>
        {/* Payment Success */}
        <Modal
          isVisible={this.state.showModalPayOrderSuccess}
          style={styles.bottomModal}
          onBackdropPress={() => this.setState({ showModalPayOrderSuccess: false })}
        >
          <View style={styles.modalContent}>
            <View style={[styles.modalRowHeader]}>
              <Text style={styles.modalRowHeaderText}>Success</Text>
              <TouchableOpacity
                activeOpacity={0.7}
                onPress={() => this.setState({ showModalPayOrderSuccess: false })}
              >
                <Ionicons name='ios-close' size={25} color='#292D44' style={{ marginHorizontal: 20, marginVertical: 10 }} />
              </TouchableOpacity>
            </View>
            <Text style={styles.txtContent}>You have sucessfully paid for your meal.</Text>
            {
              !this.state.plate_cash_earned? null: 
              <View>
                <Hr marginTop={15} marginLeft={20} marginRight={20} />
                <Text style={styles.txtContent}>Sweet! You just earned some <Text style={[styles.txtContent, { fontSize: 18, color: '#ff4d0d' }]}>Plate Cash!</Text></Text>
                <Image source={Images.icWallet} style={styles.icWallet} resizeMode={'contain'} />
                <View style={styles.viewMoney}>
                  <Text style={styles.label}>AED <Text style={styles.number}>{this.state.plate_cash.amount.toFixed(2)}</Text></Text>
                  <View style={styles.viewLine} />
                </View>
              </View>
            }
            <TouchableOpacity
              style={[styles.btnBill, { marginTop: 30, backgroundColor: '#3CCFA9' }]}
              activeOpacity={0.7}
              onPress={() => {
                this.setState({ showModalPayOrderSuccess: false })
                if(this.state.order.bill[uid]){
                  setTimeout(() => {
                    Actions.restaurantRateScreen({dishes: this.state.order.bill[uid].dishes})
                  }, 500)
                }
              }}
            >
              <Text style={[styles.btnBillText]}>Done</Text>
            </TouchableOpacity>
            <SafeAreaView forceInset={{ bottom: 'always' }} />
          </View>
        </Modal>
        {/* Paid By Others for Entire Billing */}
        <Modal
          isVisible={this.state.showModalPaidByOther}
          style={styles.bottomModal}
          onBackdropPress={() => this.setState({ showModalPaidByOther: false })}
        >
          <View style={styles.modalContent}>
            <View style={[styles.modalRowHeader]}>
              <Text style={styles.modalRowHeaderText}>Success</Text>
              <TouchableOpacity
                activeOpacity={0.7}
                onPress={() => this.setState({ showModalPaidByOther: false })}
              >
                <Ionicons name='ios-close' size={25} color='#292D44' style={{ marginHorizontal: 20, marginVertical: 10 }} />
              </TouchableOpacity>
            </View>

              <Text style={styles.txtContent}>Your meal has been paid for by {this.state.order.bill_info&&this.state.order.bill_info.paid_user_meta?this.state.order.bill_info.paid_user_meta.first_name:null} {this.state.order.bill_info&&this.state.order.bill_info.paid_user_meta?this.state.order.bill_info.paid_user_meta.last_name:null}!</Text>

            
            <TouchableOpacity
              style={[styles.btnBill, { marginTop: 30, backgroundColor: '#3CCFA9' }]}
              activeOpacity={0.7}
              onPress={() => {
                this.setState({ showModalPaidByOther: false })
                db.ref(`users/${uid}/order_history/${this.state.order_id}`).update({status: "closed", ended_at: (new Date()).getTime()})
                if(this.state.order.bill[uid]){
                  setTimeout(() => {
                    Actions.restaurantRateScreen({dishes: this.state.order.bill[uid].dishes})
                  }, 500)
                }
              }}
            >
              <Text style={[styles.btnBillText]}>Done</Text>
            </TouchableOpacity>
            <SafeAreaView forceInset={{ bottom: 'always' }} />
          </View>
        </Modal>
        <Modal
          isVisible={this.state.showModalWaiting}
          style={styles.bottomModal}
          onBackdropPress={() => this.setState({ showModalWaiting: false })}
        >
          <View style={styles.modalContent}>
            <View style={[styles.modalRowHeader]}>
              <Text style={styles.modalRowHeaderText}>Split</Text>
              <TouchableOpacity
                activeOpacity={0.7}
                onPress={() => this.setState({ showModalWaiting: false })}
              >
                <Ionicons name='ios-close' size={25} color='#292D44' style={{ marginHorizontal: 20, marginVertical: 10 }} />
              </TouchableOpacity>
            </View>
            <Text style={styles.txtContent}>Waiting for others to confirm their payment option</Text>
            <SafeAreaView forceInset={{ bottom: 'always' }} />
          </View>
        </Modal>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RestaurantBillScreen)


{/* Payment Failure
  <Modal
  isVisible={this.state.showModalPayFailure}
  style={styles.bottomModal}
  onBackdropPress={() => this.setState({ showModalPayFailure: false })}
>
  <View style={styles.modalContent}>
    <View style={[styles.modalRowHeader]}>
      <Text style={styles.modalRowHeaderText}>Failure</Text>
      <TouchableOpacity
        activeOpacity={0.7}
        onPress={() => this.setState({ showModalPayFailure: false })}
      >
        <Ionicons name='ios-close' size={25} color='#292D44' style={{ marginHorizontal: 20, marginVertical: 10 }} />
      </TouchableOpacity>
    </View>
    <Text style={styles.txtContent}>We couldn’t process your payment.</Text>
    <Text style={[styles.txtContent, { color: '#ff4d0d' }]}>Please update you card details and try again.</Text>
    <Hr marginTop={15} marginLeft={20} marginRight={20} />
    <View style={styles.rowCard}>
      <Text style={[styles.rowCardText, { flex: 1 }]}>Credit Card</Text>
      <Text
        onPress={() => {
          this.setState({ showModalPayFailure: false })
          setTimeout(() => {
            Actions.paymentScreen()
          }, 500)
        }}
        suppressHighlighting
        style={styles.rowCardText}>
        Change
      </Text>
    </View>
    <Text style={[styles.rowCardTextNumber]}>•••• •••• •••• 0042</Text>
    <Text style={[styles.txtContent, { marginVertical: 15 }]}>You are about to pay <Text style={[styles.txtContent, { fontSize: 18, fontFamily: Fonts.type.avenirHeavy }]}>AED 70.83</Text> with your selected payment option.</Text>
    <TouchableOpacity
      style={[styles.btnBill, { marginTop: 30 }]}
      activeOpacity={0.7}
      onPress={() => {
        this.setState({ showModalPayFailure: false })
        setTimeout(() => {
          this.setState({ showModalPayOrderSuccess: true })
        }, 500)
      }}
    >
      <Text style={[styles.btnBillText]}>Pay</Text>
    </TouchableOpacity>
    <SafeAreaView forceInset={{ bottom: 'always' }} />
  </View>
</Modal> */}