import React, { Component } from 'react'
import { Text, Image, View, TouchableOpacity, FlatList, Platform, PermissionsAndroid } from 'react-native'
import { connect } from 'react-redux'
import styles from './Styles/RestaurantInviteFriendScreenStyle'
import { CardView } from '../Components'
import { Images } from '../Themes'
import SafeAreaView from 'react-native-safe-area-view'
import Ionicons from 'react-native-vector-icons/Ionicons'
import Contacts from 'react-native-contacts'
import { FriendActions, bindActionCreators } from '../Redux/Actions'

class RestaurantInviteFriendScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      index: 0,
      contacts: [],
      dataContacts: []
    }
  }

  findPhone (phones, phone) {
    for (let i = 0; i < phones.length; i++) {
      if (phones[i] === phone) {
        return false
      }
    }
    return true
  }
  componentDidMount (){
    if (Platform.OS === 'ios') {
      Contacts.getAll((err, contacts) => {
        if (err === 'denied') {
          // error
        } else {
          __DEV__ && console.log('contacts', contacts)
          // contacts returned in Array
          let data = []
          let dataContacts = []
          if (contacts !== []) {
            for (let i = 0; i < contacts.length; i++) {
              let phones = []
              for (let j = 0; j < contacts[i].phoneNumbers.length; j++) {
                let phone = contacts[i].phoneNumbers[j].number.replace(/ /g, '')
                phone = phone.replace(/-/g, '')
                phone = phone.replace(/\)/g, '')
                phone = phone.replace(/\(/g, '')
                console.log('phone', phone)
                if (phone[0] !== '0') {
                  if (phone[0] === '+') {
                    if (this.findPhone(phones, phone)) {
                      phones.push(phone)
                      dataContacts.push({
                        phone_number: phone,
                        thumbnailPath: contacts[i].thumbnailPath,
                        contactName: contacts[i].givenName && contacts[i].familyName ? contacts[i].givenName + ' ' + contacts[i].familyName : contacts[i].givenName ? contacts[i].givenName : contacts[i].familyName
                      })
                    }
                  } else if (phone[0] === '1' || phone[0] === '2' || phone[0] === '3' || phone[0] === '4' || phone[0] === '5' || phone[0] === '6' || phone[0] === '7' || phone[0] === '8' || phone[0] === '9') {
                    phone = '+' + phone
                    if (this.findPhone(phones, phone)) {
                      phones.push(phone)
                      dataContacts.push({
                        phone_number: phone,
                        thumbnailPath: contacts[i].thumbnailPath,
                        contactName: contacts[i].givenName && contacts[i].familyName ? contacts[i].givenName + ' ' + contacts[i].familyName : contacts[i].givenName ? contacts[i].givenName : contacts[i].familyName
                      })
                    }
                  }
                } else {
                  phone = this.props.country && this.props.country.location
                    ? phone.replace('0', `+${this.props.country.location.calling_code}`)
                    : phone.replace('0', `+33`)
                  if (this.findPhone(phones, phone)) {
                    phones.push(phone)
                    dataContacts.push({
                      phone_number: phone,
                      thumbnailPath: contacts[i].thumbnailPath,
                      contactName: contacts[i].givenName && contacts[i].familyName ? contacts[i].givenName + ' ' + contacts[i].familyName : contacts[i].givenName ? contacts[i].givenName : contacts[i].familyName
                    })
                  }
                }
              }
              if (phones.length !== 0) {
                data.push(phones)
              }
            }
          }
          this.setState({ contacts: data, dataContacts })
          this.props.syncContacts(data, dataContacts)
        }
      })
    } else {
      PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
        {
          'title': 'Contacts',
          'message': 'This app would like to view your contacts.'
        }
      ).then(async () => {
        await Contacts.getAll((err, contacts) => {
          if (err === 'denied') {
            // error
          } else {
            __DEV__ && console.log('contacts', contacts)
            // contacts returned in Array
            let data = []
            let dataContacts = []
            if (contacts !== []) {
              for (let i = 0; i < contacts.length; i++) {
                let phones = []
                for (let j = 0; j < contacts[i].phoneNumbers.length; j++) {
                  let phone = contacts[i].phoneNumbers[j].number.replace(/ /g, '')
                  phone = phone.replace(/-/g, '')
                  phone = phone.replace(/\)/g, '')
                  phone = phone.replace(/\(/g, '')
                  console.log('phone', phone)
                  if (phone[0] !== '0') {
                    if (phone[0] === '+') {
                      if (this.findPhone(phones, phone)) {
                        phones.push(phone)
                        dataContacts.push({
                          phone_number: phone,
                          thumbnailPath: contacts[i].thumbnailPath,
                          contactName: contacts[i].givenName && contacts[i].familyName ? contacts[i].givenName + ' ' + contacts[i].familyName : contacts[i].givenName ? contacts[i].givenName : contacts[i].familyName
                        })
                      }
                    } else if (phone[0] === '1' || phone[0] === '2' || phone[0] === '3' || phone[0] === '4' || phone[0] === '5' || phone[0] === '6' || phone[0] === '7' || phone[0] === '8' || phone[0] === '9') {
                      phone = '+' + phone
                      if (this.findPhone(phones, phone)) {
                        phones.push(phone)
                        dataContacts.push({
                          phone_number: phone,
                          thumbnailPath: contacts[i].thumbnailPath,
                          contactName: contacts[i].givenName && contacts[i].familyName ? contacts[i].givenName + ' ' + contacts[i].familyName : contacts[i].givenName ? contacts[i].givenName : contacts[i].familyName
                        })
                      }
                    }
                  } else {
                    phone = this.props.country && this.props.country.location
                      ? phone.replace('0', `+${this.props.country.location.calling_code}`)
                      : phone.replace('0', `+33`)
                    if (this.findPhone(phones, phone)) {
                      phones.push(phone)
                      dataContacts.push({
                        phone_number: phone,
                        thumbnailPath: contacts[i].thumbnailPath,
                        contactName: contacts[i].givenName && contacts[i].familyName ? contacts[i].givenName + ' ' + contacts[i].familyName : contacts[i].givenName ? contacts[i].givenName : contacts[i].familyName
                      })
                    }
                  }
                }
                if (phones.length !== 0) {
                  data.push(phones)
                }
              }
            }
            this.setState({ contacts: data, dataContacts })
            this.props.syncContacts(data, dataContacts)
          }
        })
      })
    }
  }

  _renderUsers (data, renderMode) {
    return (
      <FlatList
        style={styles.list}
        data={data}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item }) => {
          var user
          if(renderMode == "friends")
            user = item.meta
          else
            user = {
              avatar_url: item.thumbnailPath,
              first_name: item.contactName,
              last_name: ''
            }
          return (
            <View style={styles.rowUser}>
              <Image source={user.avatar_url ? { uri: user.avatar_url } : Images.profile} style={styles.rowUserAvatar} />
              <View style={styles.rowUserContent}>
                <Text style={styles.rowUserName}>{user.first_name} {user.last_name}</Text>
                <Text style={styles.rowUserPlate}>0 Plate</Text>
              </View>
              <TouchableOpacity
                style={styles.rowUserIconAdd}
                activeOpacity={0.7}
                onPress={() => this.props.inviteToTable(item.uid)}
              >
                <Ionicons name='md-add' size={25} color='#BDBDBD' />
              </TouchableOpacity>
            </View>
          )
        }}
        ListFooterComponent={() => <SafeAreaView forceInset={{ bottom: 'always' }} />}
      />
    )
  }

  render () {
    return (
      <View style={styles.container}>
        <CardView style={[styles.cardStyle, styles.borderShadow]}>
          <View style={styles.rowHeader}>
            <View style={styles.headerMenu}>
              <TouchableOpacity
                style={styles.btnMenu}
                activeOpacity={0.7}
                onPress={() => {
                  this.setState({ index: 0 })
                }}
              >
                <Text style={this.state.index === 0 ? styles.headerMenuTextActive : styles.headerMenuText}>
                  Friends
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.btnMenu}
                activeOpacity={0.7}
                onPress={() => {
                  this.setState({ index: 1 })
                }}
              >
                <Text style={this.state.index === 1 ? styles.headerMenuTextActive : styles.headerMenuText}>
                  Contacts
                </Text>
              </TouchableOpacity>
            </View>
            <View flex={1} />
            <TouchableOpacity
              activeOpacity={0.7}
            >
              <Image source={Images.icSearch} style={styles.icSearch} />
            </TouchableOpacity>
          </View>
        </CardView>
        <CardView style={[styles.cardStyle, { marginTop: 10, padding: 0, flex: 1 }]}>
          {this.state.index === 0 ? this._renderUsers(this.props.friends, "friends") : this._renderUsers(this.props.contacts, "contacts")}
        </CardView>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    contacts: state.friend.contacts,
    friends: state.friend.friends,
    country: state.location.country
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    ...bindActionCreators(FriendActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RestaurantInviteFriendScreen)
