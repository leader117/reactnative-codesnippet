import React, { Component } from 'react'
import {
  TouchableWithoutFeedback,
  Text,
  View,
  TextInput,
  KeyboardAvoidingView,
  TouchableOpacity,
  Image,
  Keyboard,
  Platform,
  Alert
} from 'react-native'
import { connect } from 'react-redux'
import { Images } from '../Themes'
import { Actions } from 'react-native-router-flux'
import firebase from 'react-native-firebase'
import { showError } from '../Utilities/utils'
import { bindActionCreators } from '../Redux/Actions'
import ProfileActions from '../Redux/ProfileRedux'
import LoadingActions from '../Redux/LoadingRedux'
import TimerCountdown from 'react-native-timer-countdown'
// Styles
import styles from './Styles/UpdatePhoneConfirmCodeScreenStyle'

class UpdatePhoneConfirmCodeScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      confirmResult: props.confirmResult,
      phoneNumber: props.phoneNumber,
      code: '',
      isResend: false
    }
  }

  componentDidMount () {
    if (Platform.OS === 'android') {
      this.unsubscribe = firebase.auth().onAuthStateChanged((user) => {
        if (user) {
          this.updateProfile()
        }
      })
    }
  }

  componentWillUnmount () {
    if (this.unsubscribe) {
      this.unsubscribe()
    }
  }

  async onPressConfirm () {
    const { code, confirmResult } = this.state
    try {
      this.props.showLoading()
      await confirmResult.confirm(code)
      this.updateProfile()
      this.props.hideLoading()
    } catch (error) {
      this.props.hideLoading()
      showError('Invalid code ' + error.message)
    }
  }

  updateProfile () {
    const { profile, username } = this.props
    const { phoneNumber } = this.state
    this.props.updateProfile({
      user_meta: { ...profile, ...{ phone_number: phoneNumber } },
      username
    })
  }

  resend = async () => {
    const { phoneNumber } = this.state
    try {
      this.props.showLoading()
      const confirmResult = await firebase.auth().signInWithPhoneNumber(phoneNumber)
      this.props.hideLoading()
      this.setState({ confirmResult, isResend: false })
    } catch (error) {
      this.props.hideLoading()
      Alert.alert(error.message)
    }
  }

  render () {
    const { code } = this.state
    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={styles.container}>
          <KeyboardAvoidingView style={styles.keyboardContainer} enabled={Platform.OS === 'ios'} keyboardVerticalOffset={70} behavior='padding'>
            <Text style={styles.txtHeader}>Please type your confirmation code here.</Text>
            <View style={styles.middleView}>
              <View style={styles.rowInput}>
                <TextInput
                  style={[styles.textInput, { marginLeft: 0 }]}
                  onChangeText={(text) => this.setState({ code: text })}
                  value={code}
                  placeholder='Confirmation Code'
                  underlineColorAndroid={'transparent'}
                  keyboardType='phone-pad'
                />
              </View>
              {
                this.state.isResend ? (
                  <TouchableOpacity onPress={this.resend}>
                    <Text style={styles.txtConnectSocial}>Send again</Text>
                  </TouchableOpacity>
                ) : (
                  <View style={styles.rowCountdown}>
                    <Text style={styles.txtCountdown}>Resend code in </Text>
                    <TimerCountdown
                      initialMilliseconds={1000 * 60}
                      // onTick={(milliseconds) => __DEV__ && console.log('tick', milliseconds)}
                      onExpire={() => this.setState({ isResend: true })}
                      formatMilliseconds={(milliseconds) => {
                        const remainingSec = Math.round(milliseconds / 1000)
                        const seconds = parseInt((remainingSec % 60).toString(), 10)
                        const minutes = parseInt(((remainingSec / 60) % 60).toString(), 10)
                        const hours = parseInt((remainingSec / 3600).toString(), 10)
                        const s = seconds < 10 ? '0' + seconds : seconds
                        const m = minutes < 10 ? '0' + minutes : minutes
                        let h = hours < 10 ? '0' + hours : hours
                        h = h === '00' ? '' : h + ':'
                        return h + m + ':' + s
                      }}
                      style={styles.txtCountdown}
                    />
                  </View>
                )
              }
              <TouchableOpacity onPress={() => Actions.pop()}>
                <Text style={styles.txtConnectSocial}>Edit my mobile number.</Text>
              </TouchableOpacity>
            </View>

            <View style={styles.bottomView}>
              {!!code && (
                <TouchableOpacity style={styles.btnArrow} onPress={() => this.onPressConfirm()}>
                  <Image source={Images.icArrowRight} style={styles.icArrowRight} />
                </TouchableOpacity>
              )}
            </View>
          </KeyboardAvoidingView>
        </View>
      </TouchableWithoutFeedback>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    country: state.location.country
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    showLoading: bindActionCreators(LoadingActions.show, dispatch),
    hideLoading: bindActionCreators(LoadingActions.hide, dispatch),
    ...bindActionCreators(ProfileActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(UpdatePhoneConfirmCodeScreen)
