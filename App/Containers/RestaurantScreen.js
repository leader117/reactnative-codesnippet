import React, { Component } from 'react'
import {
  ScrollView,
  Text,
  Image,
  View,
  TouchableOpacity,
  FlatList,
  ImageBackground,
  SectionList,
  Alert
} from 'react-native'
import { connect } from 'react-redux'
import firebase from 'react-native-firebase'

// Styles
import styles from './Styles/RestaurantScreenStyle'
import { CardView } from '../Components'
import { Images, Metrics, Colors } from '../Themes'
import SafeAreaView from 'react-native-safe-area-view'
import Ionicons from 'react-native-vector-icons/Ionicons'
import { Actions } from 'react-native-router-flux'
import KeyboardSpacer from '../Components/KeyboardSpacer'
import { TextInputMask } from 'react-native-masked-text'
import * as Animatable from 'react-native-animatable'
import { RNCamera } from 'react-native-camera'


class RestaurantScreen extends Component {
  constructor(props){
    super(props)
    const uid = firebase.auth().currentUser.uid
    this.state = {
      index: 0,                   //index for selected category

      bShowFooterCard: false,     //show footer card at first

      showScanTable: false,       //show qr code card
      showTableNumber: false,     //show table number input card
      showInviteFriend: false,    //show invite friend card
      showTable: this.props.order?true:false,           //show main card
      showTableDetail: false,     //show expanded card

      order_id: this.props.order ?this.props.order.order_id:null,
      tableNumber: this.props.order ?this.props.order.table_number:1,             //table number
      order: this.props.order,       //order created or not
      current_user_order: this.props.order&&this.props.order.current_order?this.props.order.current_order[uid]:null,
      lastScannedData: null,
      selectedServiceType: this.props.order ?this.props.order.service_type:null,
      invitedUsers: this.props.order&&this.props.order.users?this.props.order.users:{},
      isWaitingConfirmation: false,
      remindToPay: false,

      distance: this.props.data.distance,
      restaurant_key: this.props.order?this.props.order.restaurant_id:this.props.data.restaurant_doc_key,
      resData : this.props.data,
      uid : uid
    }
  }

  componentDidMount () {
    setTimeout(() => {
      this.props.profile && this.setState({ bShowFooterCard: true })
    }, 300)
    firebase.database().ref(`restaurants/${this.state.restaurant_key}`).on('value', snapshot => {
      if(snapshot.exists()){
        this.setState({ resData: snapshot.val() })
      }
    })
    if(this.state.order_id){
      firebase.database().ref(`orders/${this.state.order_id}`).on('value', snapshot => {
        if(snapshot.exists()){
          const order = snapshot.val()
          this.setState({ order: order})
          if(order.users)
            this.setState({invitedUsers: order.users})
          else
            this.setState({invitedUsers: {}})
        }
        else
          this.setState({ order: null }) 
      })
      firebase.database().ref(`orders/${this.state.order_id}/current_order/${this.state.uid}`).on('value', (snapshot) => {
        var state = {}
        if(snapshot.val()){
          if(this.state.current_user_order && this.state.current_user_order.status == "Waiting" && snapshot.val().status != "Waiting")
            state['remindToPay'] = true
        }
        else{
          firebase.database().ref(`orders/${this.state.order_id}/current_order/${this.state.uid}`).off()
          
          if(this.state.current_user_order && this.state.current_user_order.status == "Waiting")
          {
            state['remindToPay'] = true
          }
        }
        state['current_user_order'] = snapshot.val()
        this.setState(state)
      })
      firebase.database().ref(`orders/${this.state.order_id}/current_order/${this.state.uid}`).off()
    }
  }

  componentWillUnmount(){
    firebase.database().ref(`restaurants/${this.state.restaurant_key}`).off()
    if(this.state.order_id){
      firebase.database().ref(`orders/${this.state.order_id}`).off()
      firebase.database().ref(`orders/${this.state.order_id}/current_order/${this.state.uid}`).off()
    }
    this.props.getActiveOrders && this.props.getActiveOrders()
  }

  _showItem (item) {
    if( (this.state.selectedServiceType == 'dine-in' && this.state.order && !this.state.isWaitingConfirmation) || (this.state.selectedServiceType == 'pick-up' && this.state.order)){
      Actions.restaurantItemScreen({
        title: this.props.title,
        dish: item,
        res_doc_key: this.state.restaurant_key,
        orderSuccess: (data) => {
          // firebase.database().ref(`orders/${this.state.order_id}/current_order/${this.state.uid}/dishes`).push(data)
          // const total_price = this.state.current_user_order && this.state.current_user_order.total_price?this.state.current_user_order.total_price:0
          // firebase.database().ref(`orders/${this.state.order_id}/current_order/${this.state.uid}`).update({status: "Ordering", total_price: data.total_price + total_price})
          firebase.database().ref(`orders/${this.state.order_id}/bill/${this.state.uid}/dishes`).push(data)
          var total_price = this.state.order.bill_total_price?this.state.order.bill_total_price:0
          firebase.database().ref(`orders/${this.state.order_id}`).update({bill_total_price: data.total_price + total_price})
          total_price = this.state.order&&this.state.order.bill&&this.state.order.bill[this.state.uid]&&this.state.order.bill[this.state.uid].total_price?this.state.order.bill[this.state.uid].total_price:0
          firebase.database().ref(`orders/${this.state.order_id}/bill/${this.state.uid}`).update({total_price: data.total_price + total_price})
          this.setState({remindToPay: false})
          if(!this.state.current_user_order){
            firebase.database().ref(`orders/${this.state.order_id}/current_order/${this.state.uid}`).on('value', (snapshot) => {
              var state = {}
              if(snapshot.val()){
                if(this.state.current_user_order && this.state.current_user_order.status == "Waiting" && snapshot.val().status != "Waiting")
                  state['remindToPay'] = true
              }
              else{
                firebase.database().ref(`orders/${this.state.order_id}/current_order/${this.state.uid}`).off()
                
                if(this.state.current_user_order && this.state.current_user_order.status == "Waiting")
                {
                  state['remindToPay'] = true
                }
              }
              state['current_user_order'] = snapshot.val()
              this.setState(state)
            })
          }
        }
      })
    }
  }

  _onQrCodeRead (result) {
    if (result.data !== this.state.lastScannedData) {
      this.setState({ lastScannedData: result.data })
    }
  }

  onPressMenu (itemIndex) {
    this.setState({ index: itemIndex })
  }

  checkInTableNumber(){
    var tables = this.state.resData.tables
    if(tables){
      var table = null, key = null
      for(let i = 0 ; i < tables.length ; i++){
        if(tables[i].number == this.state.tableNumber){
          table = tables[i]
          // if(table.isActive === true)
          //   Alert.alert("Please input valid table number.");
          // else
          {
            table.isActive = true
            firebase.database().ref(`restaurants/${this.state.restaurant_key}/tables/${i}`).update(table)
            this.setState({ showInviteFriend: true, showTableNumber: false })
          }
          break;
        }
      }
      if(!table)
        Alert.alert("Please input valid table number.");
    } else
      Alert.alert("Please input valid table number.");
  }

  createOrder(){
    const ref = firebase.database().ref('orders')
    const orderData = {
      restaurant_id: this.state.restaurant_key,
      order_id: this.state.resData.order_index+1,
      host_user_id : this.state.uid,
      host_user_meta: this.props.profile.user_meta,
      restaurant_name: this.state.resData.name,
      table_number: this.state.tableNumber,
      service_type: this.state.selectedServiceType,
      status: "Opened",
      users: this.state.invitedUsers,
      created_at: (new Date()).getTime()
    }
    firebase.database().ref(`restaurants/${this.state.restaurant_key}`).update({order_index: this.state.resData.order_index+1})
    const order_id = ref.push(orderData).key
    firebase.database().ref(`users/${this.state.uid}/order_history/${order_id}`).set({
      status: "active",
      created_at: (new Date()).getTime(),
      restaurant_id: this.state.restaurant_key
    })
    if(this.state.invitedUsers){
      const arr = Object.entries(this.state.invitedUsers)
      for( let i = 0; i < arr.length ; i++)
        firebase.database().ref(`users/${arr[i][0]}/order_history/${order_id}`).set({
          status: "invited",
          created_at: (new Date()).getTime(),
          restaurant_id: this.state.restaurant_key
        })
    }
    firebase.database().ref(`orders/${order_id}`).on('value', snapshot => {
      if(snapshot.exists()){
        const order = snapshot.val()
        this.setState({ order: order})
        if(order.users)
          this.setState({invitedUsers: order.users})
        else
          this.setState({invitedUsers: {}})
      }
      else
        this.setState({ order: null }) 
    })
    this.setState({order_id: order_id, showInviteFriend: false, showTable: true})
  }

  inviteFriendScreen(){
    Actions.restaurantInviteFriend({inviteToTable: (uid) => {
      var invitedUsers = this.state.invitedUsers? this.state.invitedUsers: {}
      if(invitedUsers[uid])
        Alert.alert("This user is already invited.")
      else{
        firebase.database().ref(`users/${uid}/user_meta`).once('value').then(snapshot => {
          if(snapshot.exists()){
            if(this.state.order){
              firebase.database().ref(`orders/${this.state.order_id}/users/${uid}`).set({
                status: 'invited',
                user_meta: snapshot.val()
              })
              firebase.database().ref(`users/${uid}/order_history/${this.state.order_id}`).set({
                status: "invited",
                created_at: (new Date()).getTime(),
                restaurant_id: this.state.restaurant_key
              })
            } else{
              invitedUsers[uid] = {
                status: 'invited',
                user_meta: snapshot.val()
              }
              this.setState({invitedUsers: invitedUsers})
            }
          } else
            Alert.alert("Cannot find user.")
        })
      }
    }})
  }

  render () {
    var featured_dishes = [], hasFeaturedDishes = false
    var dish_list_by_category = {}
    if(this.state.resData.dishes){
      Object.entries(this.state.resData.dishes).map(item => {
        if((this.state.selectedServiceType == "dine-in" || this.state.selectedServiceType == "pick-up") && item[1].service_type[this.state.selectedServiceType]!=true )
          return null
        if(!this.state.resData.no_featured_dishes && item[1].is_featured && item[1].status == "active" && item[1].image_url)
        {
          hasFeaturedDishes = true
          featured_dishes.push(item)
        }
        if(!dish_list_by_category[item[1].category_id])
          dish_list_by_category[item[1].category_id] = []
        dish_list_by_category[item[1].category_id].push(item)
      })
    }
    
    return (
      <View style={styles.container}>
        <SectionList
          contentContainerStyle={{ paddingBottom: this.state.footerHeight || 0 }}
          bounces={false}
          ref={ref => (this.scrollView = ref)}
          stickySectionHeadersEnabled
          sections={[
            {
              isHeader: true,
              data: [0]
            },
            {
              data: [1]
            }
          ]}
          keyExtractor={(item, index) => index.toString()}
          renderSectionHeader={({ section }) => !section.isHeader && (
            <CardView style={[styles.cardStyle, styles.borderShadow, { marginBottom: 1 }]}>
              <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                <View style={styles.headerMenu}>
                  {!this.state.resData.menu_categories?null:Object.entries(this.state.resData.menu_categories).map((data, index) => (
                    <TouchableOpacity
                      key={data[0]}
                      style={styles.btnMenu}
                      activeOpacity={0.7}
                      onPress={() => this.onPressMenu(index)}
                    >
                      <Text style={this.state.index === index ? styles.headerMenuTextActive : styles.headerMenuText}>
                        {data[1].name}
                      </Text>
                    </TouchableOpacity>
                  ))}
                </View>
              </ScrollView>
            </CardView>
          )}
          renderItem={({ item }) => {
            if (item === 0) {
              return (
                <View style={{ padding: 20, backgroundColor: '#fff' }}>
                  <View style={[styles.rowHeader]}>
                    {
                      this.state.distance?
                      <View style={styles.rowHeader}>
                        <Image source={Images.location} style={styles.rowHeaderIcon} />
                        <Text style={styles.rowHeaderText}>{(this.state.distance||0).toFixed(1)}m</Text>
                      </View>
                      :null
                    }                    
                    <View style={[styles.rowHeader, { marginLeft: 10 }]}>
                      <Image source={Images.starYellow} style={styles.rowHeaderIcon} />
                      <Text style={styles.rowHeaderText}>{this.state.resData.testimonial && this.state.resData.testimonial.avg_rate_mark?(this.state.resData.testimonial.avg_rate_mark||0).toFixed(1):"0.0"}</Text>
                    </View>
                    <View style={[styles.rowHeader, { marginLeft: 10 }]}>
                      <Image source={Images.comment} style={styles.rowHeaderIcon} />
                      <Text style={styles.rowHeaderText}>{this.state.resData.testimonial && this.state.resData.testimonial.comments_count?this.state.resData.testimonial.comments_count:0}</Text>
                    </View>
                    <View flex={1} />
                  </View>
                  <Text style={styles.headerLabel}>{this.state.resData.location?this.state.resData.location:"Unknown Location"}</Text>
                  <View style={[styles.rowHeader]}>
                    {
                      !this.state.resData.tags?null:this.state.resData.tags.map(value =>(
                        <TouchableOpacity
                          style={[styles.headerButton, value.type == "restaurant" && { backgroundColor: Colors.active }]}
                          activeOpacity={0.7}
                        >
                          <Text style={styles.headerButtonText}>{value.name}</Text>
                        </TouchableOpacity>
                      ))
                    }
                  </View>
                </View>
              )
            } else {
              return (
                <View>
                  {hasFeaturedDishes?
                  <CardView style={[styles.cardStyle, { paddingVertical: 25 }]}>
                    <Text style={[styles.headerMenuTextActive, { marginHorizontal: 20 }]}>Featured</Text>
                    <FlatList
                      contentContainerStyle={{ paddingRight: 20 }}
                      data={featured_dishes}
                      keyExtractor={(item, index) => { return index.toString() }}
                      horizontal
                      showsHorizontalScrollIndicator={false}
                      renderItem={({ item }) => {
                        const dish = item[1]
                        return (
                          <TouchableOpacity
                            style={styles.item}
                            activeOpacity={0.7}
                            onPress={() => this._showItem(item)}
                          >
                            <View style={dish.is_available==true ? {} : { opacity: 0.25 }}>
                              <ImageBackground borderRadius={5} source={{ uri: dish.image_url }} style={styles.itemImage}>
                                <View style={styles.itemRatingWrap}>
                                  <Image source={Images.starWhite} style={styles.itemRatingImage} />
                                  <Text style={styles.itemRatingText}>{(dish.testimonial.avg_rate_mark||0).toFixed(1)}</Text>
                                </View>
                              </ImageBackground>
                              <Text style={styles.itemTitle}  numberOfLines = { 1 }>{dish.name}</Text>
                              <Text style={styles.featuredMenuDes}  numberOfLines = { 1 }>{dish.description}</Text>
                              <View style={styles.rowItem}>
                                <TouchableOpacity
                                  activeOpacity={0.7}
                                  style={[styles.itemRowComment]}
                                  onPress={() => Actions.restaurantReviewScreen()}
                                >
                                  <Image source={Images.comment} style={styles.itemRowCommentIcon} />
                                  <Text style={styles.itemRowCommentText}>{(dish.testimonial.avg_rate_mark||0).toFixed(1)}</Text>
                                </TouchableOpacity>
                                <Text style={styles.itemTxtAed}>{dish.price.currency} {dish.price.amount}</Text>
                              </View>
                            </View>
                          </TouchableOpacity>
                        )
                      }}
                    />
                  </CardView>
                  :null
                  }
                  {!dish_list_by_category?null:Object.entries(dish_list_by_category).map((data) => { 
                    console.log(data)
                    return (
                    <CardView style={[styles.cardStyle, { marginBottom: 8, paddingTop: 20, paddingBottom: 0, flex: 1 }]} key={data[0]}>
                      <Text style={[styles.headerMenuTextActive, { marginHorizontal: 20 }]}>{this.state.resData.menu_categories[data[0]].name}</Text>
                      {
                        data[1].map((item_data) => {
                            return (
                              <TouchableOpacity
                                key={item_data[0]}
                                style={styles.itemVertical}
                                activeOpacity={0.7}
                                onPress={() => this._showItem(item_data)}
                              >
                                <View style={item_data[1].is_available==true ? {} : { opacity: 0.25 }}>
                                  <View style={styles.itemVerticalRow}>
                                    <Text style={[styles.itemTitle, { flex: 1, marginTop: 0, marginRight: 10 }]}>{item_data[1].name}</Text>
                                    <Text style={[styles.itemTxtAed, { flex: 0, marginHorizontal: 0 }]}>{item_data[1].price.currency} {item_data[1].price.amount}</Text>
                                  </View>
                                  <View style={styles.itemVerticalRow}>
                                    <Text style={[styles.MenuDes, {  }]} numberOfLines = { 1 }>{item_data[1].description}</Text>
                                    <View style={styles.itemVerticalRow}>
                                      <View style={[styles.rowHeader, { marginLeft: 10 }]}>
                                        <Image source={Images.starYellow} style={styles.rowHeaderIcon} />
                                        <Text style={styles.rowHeaderText}>{(item_data[1].testimonial.avg_rate_mark||0).toFixed(1)}</Text>
                                      </View>
                                      <View style={[styles.rowHeader, { marginLeft: 10 }]}>
                                        <Image source={Images.comment} style={styles.rowHeaderIcon} />
                                        <Text style={[styles.itemRowCommentText, { marginRight: 0 }]}>{item_data[1].testimonial.comments_count}</Text>
                                      </View>
                                    </View>
                                  </View>
                                </View>
                              </TouchableOpacity>
                            )
                          }
                        )
                      }
                    </CardView>
                  )
                  })}
                </View>
              )
            }
          }}
        />
        {
          this.state.bShowFooterCard && (
            <Animatable.View animation='slideInUp' style={styles.footer} onLayout={(e) => this.setState({ footerHeight: e.nativeEvent.layout.height })}>
              {this._renderFooterContent()}
            </Animatable.View>
          )
        }
      </View>
    )
  }

  _renderFooterContent () {
    if (this.state.showScanTable) {
      return (
        <SafeAreaView forceInset={{ bottom: 'always', backgroundColor: Colors.white }}>

          <View style={styles.footerContent}>
            <Text style={styles.footerQuestionText}>Scan your table’s QR code</Text>
            <Text style={styles.footerQuestionTextInfo}>Can’t find it? Ask your host for more information.</Text>
            <Text
              style={[styles.footerActionText]}
              suppressHighlighting
              onPress={() => this.setState({ showScanTable: false, showTableNumber: true })}
            >
              Enter manually.
            </Text>
          </View>
          <View style={styles.cameraWrap}>
            <RNCamera
              style={styles.camera}
              ref={ref => {
                this.camera = ref
              }}
              type={RNCamera.Constants.Type.back}
              // flashMode={flashMode}
              // barCodeTypes={[RNCamera.Constants.BarCodeType.qr, RNCamera.Constants.BarCodeType.aztec]}
              onBarCodeRead={(data) => this._onQrCodeRead(data)}
            />
          </View>
        </SafeAreaView>
      )
    }
    if (this.state.showTableNumber) {
      return (
        <SafeAreaView forceInset={{ bottom: 'always', backgroundColor: Colors.white }}>

          <View style={styles.footerContent}>
            <Text style={styles.footerQuestionText}>What’s your table number?</Text>
            <Text
              style={[styles.footerActionText]}
              suppressHighlighting
              onPress={() => this.setState({ showScanTable: true, showTableNumber: false })}
            >
              Switch to QR Code.
            </Text>
            <TextInputMask
              type={'only-numbers'}
              style={styles.tableNumberText}
              value={String(this.state.tableNumber)}
              onChangeText={(tableNumber) => this.setState({ tableNumber })}
              underlineColorAndroid='transparent'
              keyboardType='numeric'
              maxLength={4}
              placeholder='Table Number'
              autoFocus
            />
          </View>
          <TouchableOpacity
            style={styles.btnCheckIn}
            activeOpacity={0.7}
            onPress={() => this.checkInTableNumber()}
          >
            <Text style={styles.btnCheckInText}>Check-in</Text>
          </TouchableOpacity>
          {Metrics.platform === 'ios' && <KeyboardSpacer />}
        </SafeAreaView>
      )
    }
    if (this.state.showInviteFriend) {
      return (
        <SafeAreaView forceInset={{ bottom: 'always', backgroundColor: Colors.white }}>
          <View style={styles.footerContent}>
            <Text style={styles.footerQuestionText}>Do you want to invite some friends?</Text>
            <TouchableOpacity
              style={styles.footerContentRowUser}
              activeOpacity={0.7}
              onPress={() => this.inviteFriendScreen()}
            >
              { !this.state.invitedUsers? null : 
                Object.entries(this.state.invitedUsers).map(item => (
                  <Image source={item[1].user_meta.avatar_url ? { uri: item[1].user_meta.avatar_url } : Images.profile} style={styles.footerContentRowUserAvatar} />
                ))
              }
              <View style={styles.footerContentRowUserBtn}>
                <Ionicons name='md-add' color='#696E82' size={25} />
              </View>
            </TouchableOpacity>
          </View>
          <TouchableOpacity
            style={styles.btnSkip}
            activeOpacity={0.7}
            onPress={() => this.createOrder()}
          >
            <Text style={styles.btnCheckInText}>Skip</Text>
          </TouchableOpacity>
        </SafeAreaView>
      )
    }

    if (this.state.showTable) {
      return (
        <SafeAreaView forceInset={{ flex: 1, bottom: 'always' }}>
          <TouchableOpacity
            onPress={() => {
              if(this.state.current_user_order && this.state.current_user_order.status == "Waiting")
                return
              else{
                this.bottomView.transitionTo({ height: this.state.current_user_order && this.state.current_user_order.status == "Ordering" && !this.state.remindToPay ? 148 : 161 }, 300)
                setTimeout(() => this.setState({ showTable: false, showTableDetail: true }), 350)
              }
            }}
            activeOpacity={1}>
            <Image source={Images.moreCard} style={{ position: 'absolute', top: -40, alignSelf: 'center', zIndex: 1 }} />
            <Animatable.View ref={ref => (this.bottomView = ref)} style={styles.rowTable}>
              <View flex={1}>
                <Text style={styles.rowTableName}>Table {this.state.tableNumber}</Text>
                {
                  this.state.current_user_order && this.state.current_user_order.status == "Waiting" ? 
                  [<Text style={styles.rowTableNumberUser}>Please wait for order confirmation.</Text>,
                    <Text style={styles.rowTableNumberUser}>Waiting for the restaurant’s confirmation.</Text>]
                  : <Text style={styles.rowTableNumberUser}>Tap here to expand for more options.</Text>
                }
                
              </View>
            </Animatable.View>
            {
              (this.state.current_user_order && this.state.current_user_order.status == "Ordering" && !this.state.remindToPay) && (
                <TouchableOpacity
                  style={styles.btnConfirmOrder}
                  activeOpacity={0.7}
                  onPress={() => Actions.restaurantDineInConfirmScreen({ current_order: this.state.current_user_order, sendToKitchen: () => {
                    firebase.database().ref(`orders/${this.state.order_id}/current_order/${this.state.uid}`).update({status: "Waiting"})
                  } })}
                >
                  <View style={styles.btnConfirmOrderLeft}>
                    <Text style={styles.btnConfirmOrderLeftTitle}>{Object.entries(this.state.current_user_order.dishes).length}</Text>
                  </View>
                  <Text style={styles.btnConfirmOrderTitle}>Confirm Order</Text>
                  <Text style={styles.btnConfirmOrderRightTitle}>AED {this.state.current_user_order.total_price}</Text>
                </TouchableOpacity>
              )
            }
            {
              (this.state.remindToPay) && (
                <TouchableOpacity
                  style={styles.btnConfirmOrder}
                  activeOpacity={0.7}
                  onPress={() => Actions.restaurantBillScreen({ title: 'TABLE '+ this.state.tableNumber , discount: this.state.resData.discount, order_id: this.state.order_id, order: this.state.order })}
                >
                  <Text style={styles.btnConfirmOrderTitle}>The Bill</Text>
                </TouchableOpacity>
              )
            }
          </TouchableOpacity>
        </SafeAreaView>
      )
    }

    if (this.state.showTableDetail) {
      return (
        <SafeAreaView forceInset={{ bottom: 'always', backgroundColor: Colors.white }}>
          <TouchableOpacity
            activeOpacity={1}
            onPress={() => {
              this.setState({ onClose: true })
              this.topView.transitionTo({ height: 45 }, 300)
              setTimeout(() => this.setState({ showTable: true, showTableDetail: false, onClose: false }), 350)
            }}
          >
            <View style={styles.iconMinimize} />
            <Animatable.View ref={ref => (this.topView = ref)}>
              <View style={styles.rowTable}>
                <View flex={1}>
                  <Text style={styles.rowTableName}>Table {this.state.tableNumber}</Text>
                  <Text style={styles.rowTableNumberUser}>
                    Tap here to minimize.
                  </Text>
                </View>
                {(this.state.order.bill && this.state.order.bill[this.state.uid]) && (
                  <View style={[styles.btnConfirmOrder, { marginHorizontal: 0, marginTop: 0, paddingLeft: 20, paddingRight: 20 }]}>
                    <Text style={styles.btnConfirmOrderRightTitle}>AED {this.state.order.bill[this.state.uid].total_price}</Text>
                  </View>
                )
                }
              </View>
              {!this.state.onClose && <View style={styles.rowTableActions}>
                <TouchableOpacity
                  activeOpacity={0.7}
                  style={[styles.rowTableActionWrap, !this.state.order.bill ? { opacity: 0.3 } : {}]}
                  disabled={!this.state.order.bill}
                  onPress={() => Actions.restaurantBillScreen({ title: 'TABLE '+ this.state.tableNumber, discount: this.state.resData.discount, order_id: this.state.order_id, order: this.state.order })}
                >
                  <Image style={styles.rowTableActionWrapIcon} source={Images.iconTheBill} />
                  <Text style={styles.rowTableActionWrapTitle}>The Bill</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  activeOpacity={0.7}
                  style={[styles.rowTableActionWrap, !Object.entries(this.state.invitedUsers).length ? { opacity: 0.3 } : {}]}
                  disabled={!Object.entries(this.state.invitedUsers).length}
                  onPress={() => Actions.restaurantFriend({friends: this.state.invitedUsers})}
                >
                  <Image style={styles.rowTableActionWrapIcon} source={Images.cardFriend} />
                  <Text style={styles.rowTableActionWrapTitle}>Friends</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  activeOpacity={0.7}
                  style={styles.rowTableActionWrap}
                  onPress={() => this.inviteFriendScreen()}
                >
                  <Image style={styles.rowTableActionWrapIcon} source={Images.iconInvite} />
                  <Text style={styles.rowTableActionWrapTitle}>Invite</Text>
                </TouchableOpacity>
              </View>}
            </Animatable.View>
            {(this.state.current_user_order && this.state.current_user_order.status == "Ordering" && !this.state.remindToPay) && (
              <TouchableOpacity
                style={styles.btnConfirmOrder}
                activeOpacity={0.7}
                onPress={() => Actions.restaurantDineInConfirmScreen({ current_order: this.state.current_user_order, sendToKitchen: () => {
                  this.setState({ showTable: true, showTableDetail: false, onClose: false })
                  firebase.database().ref(`orders/${this.state.order_id}/current_order/${this.state.uid}`).update({status: "Waiting"})
                } })}
              >
                <View style={styles.btnConfirmOrderLeft}>
                  <Text style={styles.btnConfirmOrderLeftTitle}>{Object.entries(this.state.current_user_order.dishes).length}</Text>
                </View>
                <Text style={styles.btnConfirmOrderTitle}>Confirm Order</Text>
                <Text style={styles.btnConfirmOrderRightTitle}>AED {this.state.current_user_order.total_price}</Text>
              </TouchableOpacity>
            )}
          </TouchableOpacity>
        </SafeAreaView>
      )
    }

    if (this.state.selectedServiceType == 'pick-up' && this.state.current_user_order) {
      return (
        <SafeAreaView forceInset={{ bottom: 'always', backgroundColor: Colors.white }}>
          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() => Actions.restaurantPickUpScreen({ title: 'PICK-UP', isPickUp: true })}
            disabled
          >
            <View style={styles.rowTable}>
              <View flex={1}>
                <Text style={styles.rowTableName}>Pick-up</Text>
              </View>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.btnConfirmOrder}
            activeOpacity={0.7}
            onPress={() => Actions.restaurantPickUpScreen({ title: 'PICK-UP', isPickUp: true })}
          >
            <View style={styles.btnConfirmOrderLeft}>
              <Text style={styles.btnConfirmOrderLeftTitle}>3</Text>
            </View>
            <Text style={styles.btnConfirmOrderTitle}>Confirm Order</Text>
            <Text style={styles.btnConfirmOrderRightTitle}>AED 220</Text>
          </TouchableOpacity>
        </SafeAreaView>
      )
    }
    if (!this.state.selectedServiceType) {
      return (
        <SafeAreaView forceInset={{ bottom: 'always', backgroundColor: Colors.white }}>
          <Text style={styles.txtFooter}>What would you like to do?</Text>
          <View style={styles.rowNotAvailableTxt}>
            <Text style={styles.txtNotAvailable}>{this.state.resData.service_type['dine-in']?"": "(Not available)"}</Text>
            <Text style={styles.txtNotAvailable}>{this.state.resData.service_type['pick-up']?"": "(Not available)"}</Text>
          </View>
          <View style={styles.rowFooter}>
            <TouchableOpacity
              style={[styles.btnAvailableServices, this.state.resData.service_type['dine-in']?{}:{borderColor: '#9B9B9B'}]}
              disabled={!this.state.resData.service_type['dine-in']}
              activeOpacity={0.7}
              onPress={() => this.setState({ showScanTable: true, selectedServiceType: 'dine-in' })}
            >
              <Text style={[styles.btnServiceText, this.state.resData.service_type['dine-in']?{}:{textDecorationLine: "line-through",color: '#9B9B9B'}]}>Dine-in</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.btnAvailableServices, this.state.resData.service_type['pick-up']?{}:{borderColor: '#9B9B9B'}]}
              activeOpacity={0.7}
              disabled={!this.state.resData.service_type['pick-up']}
              onPress={() => this.setState({ bShowFooterCard: false, selectedServiceType: "pick-up", footerHeight: 0 })}
            >
              <Text style={[styles.btnServiceText, this.state.resData.service_type['pick-up']?{}:{textDecorationLine: "line-through",color: '#9B9B9B'}]}>Pick-up</Text>
            </TouchableOpacity>
          </View>
        </SafeAreaView>
      )
    }
  }
}

const mapStateToProps = (state) => {
  return {
    profile: state.profile.profile
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RestaurantScreen)
