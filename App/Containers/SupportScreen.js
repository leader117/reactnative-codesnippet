import React, { Component } from 'react'
import { ScrollView, Text, TouchableWithoutFeedback, View, TextInput, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'
import SafeAreaView from 'react-native-safe-area-view'

// Styles
import styles from './Styles/SupportScreenStyle'

class SupportScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      subject: '',
      content: ''
    }
  }

  render () {
    return (
      <View style={styles.keyboardContainer}>
        <TouchableWithoutFeedback onPress={() => this.refs.title.focus()}>
          <View style={styles.viewTitle}>
            <TextInput
              style={styles.textInput}
              placeholder={'Subject'}
              placeholderTextColor={'#bdbdbd'}
              value={this.state.subject}
              onChangeText={(text) => this.setState({ subject: text })}
              ref={'title'}
              onSubmitEditing={() => this.refs.textInput.focus()}
            />
          </View>
        </TouchableWithoutFeedback>
        <View style={{ flex: 1, backgroundColor: '#f7f8fa' }}>
          <SafeAreaView
            style={{ flex: 1 }}
            forceInset={{ top: 'never', bottom: 'always' }}>
            <View style={styles.viewText}>
              <TouchableWithoutFeedback onPress={() => this.refs.textInput.focus()}>
                <View style={styles.viewTextInput}>
                  <TextInput
                    style={styles.textInput}
                    placeholder={'How can we help?'}
                    placeholderTextColor={'#bdbdbd'}
                    value={this.state.content}
                    onChangeText={(text) => this.setState({ content: text })}
                    multiline
                    ref={'textInput'}
                  />
                </View>
              </TouchableWithoutFeedback>
              {this.state.content === '' && <Text style={styles.detail}>If you like you can also reach us at{'\n'}<Text style={styles.email}>complaints@plateapp.io</Text></Text>}
              <TouchableOpacity style={styles.buttonSend}>
                <Text style={styles.send}>Send</Text>
              </TouchableOpacity>
            </View>
          </SafeAreaView>
        </View>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SupportScreen)
