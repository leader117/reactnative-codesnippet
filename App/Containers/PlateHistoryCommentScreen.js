import React, { Component } from 'react'
import { TouchableOpacity, Text, FlatList, Image, View } from 'react-native'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/PlateHistoryCommentScreenStyle'
// import { KeyboardAwareFlatList } from 'react-native-keyboard-aware-scroll-view'
// import { CardView } from '../Components'
import { Images } from '../Themes'
import numeral from 'numeral'
import ReadMore from 'react-native-read-more-text'
import { Actions } from 'react-native-router-flux'
// import { Actions } from 'react-native-router-flux'

class PlateHistoryCommentScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {

    }
  }

  componentDidMount (): void {
    const { item } = this.props
    setTimeout(() => Actions.refresh({
      title: item.name.toUpperCase()
    }), 300)
  }

  render () {
    const comments = [
      {
        user: {
          name: 'Peter O’Conor',
          avatar: 'http://endlesstheme.com/simplify1.0/images/profile/profile4.jpg',
          point: 4822
        },
        comment: 'Was very tasteful and flavorful! A bit spicy though!',
        rating: 5
      }
    ]
    return (
      <View style={styles.container}>
        <View style={styles.viewTop}>
          <View style={[styles.viewInfo, { marginBottom: 4 }]}>
            <Text style={styles.name}>Dali Chicken</Text>
            <View style={{ flexDirection: 'row' }}>
              <View style={[styles.rowHeader, { marginLeft: 10 }]}>
                <Image source={Images.starYellow} style={styles.rowHeaderIcon} />
                <Text style={styles.rowHeaderText}>4.6</Text>
              </View>
            </View>
          </View>
          <Text style={styles.restaurant}>{this.props.item.name}</Text>
        </View>
        <FlatList
          data={comments}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item }) => {
            return (
              <View style={styles.item}>
                <View style={styles.row}>
                  <Image source={{ uri: item.user.avatar }} style={styles.itemAvatar} />
                  <View style={styles.itemUserInfo}>
                    <Text style={styles.userName}>{item.user.name}</Text>
                    <Text style={styles.userPoint}>{numeral(item.user.point).format('0,0')} Points</Text>
                  </View>
                </View>
                <View style={[styles.row, { marginTop: 10 }]}>
                  <View style={[styles.row, { flex: 1 }]}>
                    <Image style={[styles.imageStar, { marginRight: 3 }]} source={Images.starYellow} />
                    <Image style={[styles.imageStar, { marginRight: 3 }]} source={Images.starYellow} />
                    <Image style={[styles.imageStar, { marginRight: 3 }]} source={Images.starYellow} />
                    <Image style={[styles.imageStar, { marginRight: 3 }]} source={Images.starYellow} />
                  </View>
                  <Text style={styles.txtTime}>2 days ago.</Text>
                </View>
                <View style={{ height: 15 }} />
                <ReadMore
                  numberOfLines={3}
                  renderTruncatedFooter={(onPress) => this.renderViewMore(onPress)}
                  renderRevealedFooter={(onPress) => this.renderViewLess(onPress)}
                >
                  <Text style={styles.txtComment}>{item.comment}</Text>
                </ReadMore>
                <TouchableOpacity style={{ paddingTop: 16, paddingRight: 16 }}>
                  <Text style={styles.delete}>Delete</Text>
                </TouchableOpacity>
              </View>
            )
          }}
        />
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PlateHistoryCommentScreen)
