import React, { Component } from 'react'
import {
  ScrollView,
  Text,
  KeyboardAvoidingView,
  View,
  TouchableWithoutFeedback,
  Keyboard,
  Platform, TouchableOpacity, Image, Alert
} from 'react-native'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/UpdatePhoneNumberScreenStyle'
import PhoneInput from 'react-native-phone-input'
import { Actions } from 'react-native-router-flux'
import { Images } from '../Themes'
import CountryPicker from 'react-native-country-picker-modal'
import firebase from 'react-native-firebase'
import { bindActionCreators } from 'redux'
import LoadingActions from '../Redux/LoadingRedux'

class UpdatePhoneNumberScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      // textWrongPw: 'Oops! Wrong password. Try again!',
      // textHeader: 'Please type your password here.',
      phoneNumber: props.phoneNumber || '',
      cca2: 'US'
    }
  }

  onPressFlag () {
    this.countryPicker.openModal()
  }

  selectCountry (country) {
    this.phone.selectCountry(country.cca2.toLowerCase())
    this.setState({ cca2: country.cca2 })
  }

  async onNextPress () {
    const { phoneNumber } = this.state
    const { profile, username } = this.props
    // var regex = /^\+[0-9]{8,16}$/
    if (this.phone.isValidNumber()) {
      if (phoneNumber !== profile.phone_number) {
        try {
          this.props.showLoading()
          const confirmResult = await firebase.auth()
            .signInWithPhoneNumber(phoneNumber)
          this.props.hideLoading()
          Actions.updatePhoneConfirmCode(
            { confirmResult, phoneNumber, profile, username })
        } catch (err) {
          this.props.hideLoading()
          Alert.alert('Error', err.message)
        }
      } else {
        Alert.alert(
          'Notification:',
          'The number phone is same'
        )
      }
    } else {
      Alert.alert(
        'Notification:',
        'The number phone is invalid'
      )
    }
  }

  render () {
    const { phoneNumber } = this.state
    const { country } = this.props
    const initialCountry = country ? country.country_code.toLowerCase() : 'fr'
    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={styles.container}>
          <KeyboardAvoidingView style={styles.keyboardContainer} enabled={Platform.OS === 'ios'} keyboardVerticalOffset={70} behavior='padding'>
            <View style={styles.middleView}>
              <View style={styles.rowInput}>
                <PhoneInput
                  initialCountry={initialCountry}
                  style={styles.textInput}
                  ref={(ref) => (this.phone = ref)}
                  onPressFlag={() => this.onPressFlag()}
                  // value={phoneNumber}
                  onChangePhoneNumber={(phoneNumber) => this.setState({ phoneNumber })}
                  underlineColorAndroid={'transparent'}
                />
                <CountryPicker
                  ref={(ref) => {
                    this.countryPicker = ref
                  }}
                  onChange={value => this.selectCountry(value)}
                  translation='eng'
                  cca2={this.state.cca2}
                >
                  <View />
                </CountryPicker>
              </View>
              <Text style={styles.txtConnectSocial}>A verification code will be sent to this number.</Text>
            </View>

            <View style={styles.bottomView}>
              {!!phoneNumber && (
                <TouchableOpacity style={styles.btnArrow} onPress={() => this.onNextPress()}>
                  <Image source={Images.icArrowRight} style={styles.icArrowRight} />
                </TouchableOpacity>
              )}
            </View>
          </KeyboardAvoidingView>
        </View>
      </TouchableWithoutFeedback>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    country: state.location.country
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    showLoading: bindActionCreators(LoadingActions.show, dispatch),
    hideLoading: bindActionCreators(LoadingActions.hide, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(UpdatePhoneNumberScreen)
