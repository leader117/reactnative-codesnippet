import React, { Component } from 'react'
import {
  View,
  Text,
  TouchableOpacity,
  SafeAreaView
} from 'react-native'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/IntroScreenStyle'
import Swiper from 'react-native-swiper'
import { Actions } from 'react-native-router-flux'
import LottieView from 'lottie-react-native'
import * as Animatable from 'react-native-animatable'
import Carousel from 'react-native-snap-carousel'
import { Metrics } from '../Themes'

const IntroData = [
  { id: 1, desc: 'Explore amazing restaurants near you and browse interactive menus.' },
  { id: 2, desc: 'Order directly from your smartphone while at the restaurant or for pick-up!' },
  { id: 3, desc: 'Pay for your meal or split the bill with friends!' },
  { id: 4, desc: 'Earn Plate Cash to use on your next meal!' }
]

class IntroScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      index: 0,
      snapIndex: 0
    }
  }

  renderStart = () => {
    return (
      <TouchableOpacity style={styles.btnStart} onPress={Actions.root}>
        <Text style={styles.txtBtnGetStart}>Get Started</Text>
      </TouchableOpacity>
    )
  }

  render () {
    return (
      <Animatable.View style={styles.container} animation='fadeInRight' duration={500}>
        <SafeAreaView style={styles.container}>
          <LottieView
            style={styles.lottie}
            source={require('../Fixtures/animation.json')}
            autoPlay
            loop
          />
          <View style={styles.containerIntro}>
            <Carousel
              onSnapToItem={(snapIndex) => this.setState({ snapIndex })}
              ref={(c) => { this._carousel = c }}
              data={IntroData}
              sliderWidth={Metrics.screenWidth}
              itemWidth={Metrics.screenWidth * 0.8}
              activeSlideAlignment='center'
              inactiveSlideScale={1}
              renderItem={({ item, index }) => (
                <View styles={styles.containerIntroView}>
                  <View style={styles.warpDesc}>
                    <Text style={styles.txtDescription}>{item.desc}</Text>
                  </View>
                </View>
              )}
            />
            <View style={styles.rowPagination}>
              {IntroData.map((item, index) => {
                return (
                  <View key={index} style={[styles.pagination, this.state.snapIndex === index ? { backgroundColor: '#FF4D0D' } : {}]} />
                )
              })}
            </View>
          </View>
          <View style={styles.bottomView}>
            <TouchableOpacity style={styles.loginButton} onPress={Actions.auth}>
              <Text style={styles.loginText}>Login</Text>
            </TouchableOpacity>
            <Text style={styles.startWithPlate}>to start dining with <Text style={styles.plateText}>Plate</Text></Text>
            {/* <TouchableOpacity disabled style={styles.btnSkip} onPress={Actions.root}> */}
            {/* <Text style={styles.textSkip}>Or, skip, and sign-up later …</Text> */}
            {/* </TouchableOpacity> */}
          </View>
        </SafeAreaView>
      </Animatable.View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(IntroScreen)
