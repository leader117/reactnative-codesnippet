import React, { Component } from 'react'
import { ScrollView, Text, View, TouchableOpacity, FlatList } from 'react-native'
import { connect } from 'react-redux'
import styles from './Styles/RestaurantDineInScreenStyle'
import { Hr } from '../Components'
import { Colors } from '../Themes'
import SafeAreaView from 'react-native-safe-area-view'
import Ionicons from 'react-native-vector-icons/Ionicons'
import { Actions } from 'react-native-router-flux'
import Accordion from 'react-native-collapsible/Accordion'

class RestaurantDineInConfirmScreen extends Component {
  state = {
    activeSections: [0],
  }

  setSections = sections => {
    this.setState({
      activeSections: sections.includes(undefined) ? [] : sections
    })
  };

  renderHeader = (section, _, isActive) => {
    return (
      <View>
        <View style={[styles.content]}>
          <View style={[styles.rowTable, { marginVertical: 15 }]}>
            <Text style={[section.user ? styles.rowTableText : styles.rowTableTextBold, { flex: 1 }]}>
              {section.user || 'New orders'}:
            </Text>
            <Text style={[styles.rowTableText, { marginHorizontal: 5 }]}>AED {this.props.current_order.total_price}</Text>
            <Ionicons name={isActive ? 'ios-arrow-down' : 'ios-arrow-forward'} size={20} color='#0F1524' />
          </View>
        </View>
        <Hr marginTop={0} marginLeft={0} marginRight={0} lineHeight={5} />
      </View>
    )
  }

  renderContent = (section, _, isActive) => {
    return (
      <View>
        <FlatList
          style={{ height: Object.entries(section.dishes).length * 66 }}
          scrollEnabled={false}
          data={Object.entries(section.dishes)}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item, index }) => {
            return (
              <TouchableOpacity
                style={styles.itemWrap}
                activeOpacity={0.7}
              >
                <View style={[styles.rowItem, { justifyContent: 'space-between' }]}>
                  <View style={styles.rowItem}>
                    <Text style={styles.textNumber}>1x</Text>
                    <Text style={styles.rowItemName}>{item[1].name}</Text>
                  </View>
                  <Text style={styles.rowItemPrice}>AED {item[1].total_price}</Text>
                </View>
              </TouchableOpacity>
            )
          }}
        />
        <Hr marginTop={0} marginLeft={0} marginRight={0} lineHeight={isActive ? 4 : 1} />
      </View>
    )
  }

  render () {
    return (
      <View style={styles.container}>
        <ScrollView contentContainerStyle={[styles.scrollView, { paddingBottom: this.state.footerHeight || 0 }]}>
          <Accordion
            activeSections={this.state.activeSections}
            sections={[this.props.current_order]}
            touchableComponent={TouchableOpacity}
            renderHeader={this.renderHeader}
            renderContent={this.renderContent}
            duration={400}
            onChange={this.setSections}
          />
          <View style={styles.content}>
            <View style={[styles.rowTable, { marginTop: 25 }]}>
              <Text style={[styles.rowTableText, { flex: 1 }]}>Total</Text>
              <View style={styles.totalWrap}>
                <Text style={[styles.rowTableText, { marginHorizontal: 5, color: Colors.white }]}>AED {this.props.current_order.total_price}</Text>
              </View>
            </View>
          </View>
        </ScrollView>
        <View style={styles.footer} onLayout={(e) => this.setState({ footerHeight: e.nativeEvent.layout.height })}>
          <View style={styles.footerContent}>
            <SafeAreaView forceInset={{ bottom: 'always' }}>
              <TouchableOpacity
                style={styles.btnBill}
                activeOpacity={0.7}
                onPress={() => {
                  this.props.sendToKitchen()
                  Actions.pop()
                }}
              >
                <Text style={styles.btnBillText}>Send to Kitchen</Text>
              </TouchableOpacity>
            </SafeAreaView>

          </View>
        </View>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RestaurantDineInConfirmScreen)
