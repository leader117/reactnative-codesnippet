import React, { Component } from 'react'
import { ScrollView, Text, Image, View, TouchableOpacity, ImageBackground, FlatList } from 'react-native'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles, { cardHeight } from './Styles/PaymentScreenStyle'
import { Hr } from '../Components'
import { Images, Metrics, Colors } from '../Themes'
import SafeAreaView from 'react-native-safe-area-view'
import Ionicons from 'react-native-vector-icons/Ionicons'
import { Actions } from 'react-native-router-flux'
import _ from 'lodash'
import Modal from 'react-native-modal'

class PaymentScreen extends Component {
  state = {
    cards: [
      {
        id: 0,
        type: 'visa',
        name: 'Visa',
        last4: 5588,
        primary: true
      },
      {
        id: 1,
        type: 'mastercard',
        name: 'Mastercard',
        last4: 3200
      },
      {
        id: 2,
        type: 'visa',
        name: 'Visa',
        last4: 5566
      }
    ],
    check: 0
  }

  componentDidMount () {
    setTimeout(() => {
      this._renderRight()
    }, 300)
  }

  _renderRight () {
    Actions.refresh({
      rightTitle: this.state.isEdit ? 'Done' : 'Edit',
      onRight: () => this.setState({ isEdit: !this.state.isEdit }, () => this._renderRight()),
      rightButtonTextStyle: styles.rightButtonTextStyle
    })
  }

  _removeCard (card) {
    this.setState({ cards: _.filter(this.state.card, c => c.id !== card.id) })
  }

  _renderFooter () {
    return (
      <View style={styles.unpaidContent}>
        <View style={styles.unpaidContentRow}>
          <Text style={styles.unpaidTitle}>Unpaid Balance</Text>
          <Image source={Images.unpaidIcon} />
        </View>
        <Text style={styles.unpaidInfo}>Update your payment card to settle your balance and try again.</Text>
        <View style={styles.unpaidContentRow}>
          <Text style={styles.unpaidPrice}>AED 124.43</Text>
          <TouchableOpacity
            style={styles.btnPay}
            activeOpacity={0.7}
            onPress={() => this.setState({ paySuccess: true })}
          >
            <Text style={styles.btnPayText}>Pay</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  _renderFooterPaySuccess () {
    return (
      <Modal
        isVisible={this.state.paySuccess}
        style={{ margin: 0 }}
      >
        <View style={styles.footerPaySuccess}>
          <SafeAreaView forceInset={{ bottom: 'always' }}>
            <Text style={styles.modalTitle}>Success</Text>
            <Text style={styles.modalInfo}>You’ve sucessfully settled your balance.</Text>
            <TouchableOpacity
              style={styles.btnDone}
              activeOpacity={0.7}
              onPress={() => this.setState({ paySuccess: false })}
            >
              <Text style={styles.btnDoneText}>Done</Text>
            </TouchableOpacity>
          </SafeAreaView>
        </View>

      </Modal>
    )
  }

  render () {
    const cards = [
      {
        id: 0,
        type: 'visa',
        name: 'Visa',
        last4: 5588,
        primary: true
      },
      {
        id: 1,
        type: 'mastercard',
        name: 'Mastercard',
        last4: 3200
      },
      {
        id: 2,
        type: 'visa',
        name: 'Visa',
        last4: 5566
      }
    ]
    return (
      <View style={styles.container}>
        <View style={styles.backgroundImage}>
          <FlatList
            data={cards}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item, index }) => {
              const card = item
              return (
                <TouchableOpacity
                  style={[styles.rowItem, !this.state.isEdit && { paddingLeft: 17 }]}
                  activeOpacity={1}
                  onPress={() => this.setState({ check: index })}
                >
                  {this.state.isEdit && <TouchableOpacity
                    style={styles.btnDelete}
                    activeOpacity={0.7}
                    onPress={() => this._removeCard(card)}
                  >
                    <Image source={Images.iconDeleteCard} style={{ width: 20, height: 20 }} />
                  </TouchableOpacity>}
                  <Image source={card.type === 'visa' ? Images.iconVisa : Images.iconMasterCard} />
                  <View style={styles.rowItemLeft}>
                    <Text style={styles.cardName}>{card.name}</Text>
                    <Text style={styles.cardLast4}>Ending {card.last4}</Text>
                  </View>
                  {this.state.check === index && <View
                    style={[styles.btnDelete, { paddingHorizontal: 0, marginRight: -8 }]}
                  >
                    <Image source={Images.icTickPayment} style={{ width: 37, height: 37 }} />
                  </View>}
                </TouchableOpacity>
              )
            }}
            ListFooterComponent={() => this._renderFooter()}
          />

          <SafeAreaView>
            <View style={styles.footer}>
              {/* <TouchableOpacity */}
              {/* style={styles.rowFooter} */}
              {/* activeOpacity={0.7} */}
              {/* > */}
              {/* <View style={styles.rowFooterIcon}> */}
              {/* <Image style={{ width: 24, height: 17 }} source={Images.iconCash} /> */}
              {/* </View> */}
              {/* <Text style={styles.rowFooterText}>Cash</Text> */}
              {/* </TouchableOpacity> */}
              <TouchableOpacity
                style={styles.rowFooter}
                activeOpacity={0.7}
                onPress={() => Actions.paymentPromoCodeScreen()}
              >
                <View style={styles.rowFooterIcon}>
                  <Image resizeMode='center' style={{ width: 24, height: 17 }} source={Images.iconPromoCode} />
                </View>
                <Text style={styles.rowFooterText}>Promo Code</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.rowFooter}
                activeOpacity={0.7}
                onPress={() => Actions.paymentAddCardScreen()}
              >
                <View style={styles.rowFooterIcon}>
                  <Image resizeMode='center' style={{ width: 24, height: 16 }} source={Images.iconAddPayment} />
                </View>
                <Text style={styles.rowFooterText}>Add Payment Method</Text>
              </TouchableOpacity>
            </View>
          </SafeAreaView>
        </View>
        {this._renderFooterPaySuccess()}
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PaymentScreen)
