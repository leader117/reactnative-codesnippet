import React, { Component } from 'react'
import { Text, Image, View, TouchableOpacity, TextInput } from 'react-native'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/RestaurantRateScreenStyle'
import { Hr, CardView } from '../Components'
import { Images, Metrics, Colors } from '../Themes'
import SafeAreaView from 'react-native-safe-area-view'
import { Actions } from 'react-native-router-flux'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import StarRating from '../Components/StarRating'

class RestaurantRateScreen extends Component {

  constructor(props){
    super(props)
    this.state = {
      data: {}
    }
  } 

  componentDidMount () {

  }

  onSubmitPressed(){

    Actions.popTo('home')
  }

  render () {
    var testimonials = this.state.data
    var dishes
    if(this.props.dishes)
      dishes = Object.entries(this.props.dishes)
    else
      dishes = []
    return (
      <View style={styles.container}>
        <KeyboardAwareScrollView contentContainerStyle={[styles.scrollView]}>
        {
          dishes.map(dish => {
            if(!testimonials[dish[0]])
            testimonials[dish[0]] = {
              rating: 0,
              text: ""
            }
            return (
            <CardView style={[styles.cardView, {marginBottom: 5}]} key={dish[0]}>
              <Text style={styles.itemName}>{dish[1].name}</Text>
              <StarRating
                style={styles.rateWrap}
                rating={testimonials[dish[0]].rating}
                selectedStar={(rating) => {
                  testimonials[dish[0]].rating = rating
                  this.setState({ data: testimonials })
                }}
              />
              <Hr marginLeft={10} marginRight={10} marginTop={-10} marginBottom={15} />
              <Text style={styles.txtQues}>What are your thoughts about your plate?</Text>
              <View style={styles.inputWrap}>
                <TextInput
                  style={styles.input}
                  value={String(testimonials[dish[0]].text)}
                  onChangeText={(text) =>{ 
                    testimonials[dish[0]].text = text
                    this.setState({ data: testimonials })
                  }}
                  placeholder='Please add your comments here.'
                  placeholderTextColor='#BDBDBD'
                  multiline
                  minHeight={40}
                  underlineColorAndroid='transparent'
                />
              </View>
            </CardView>
            )
          })
        }
          
        </KeyboardAwareScrollView>
        <View style={styles.bottomView}>
          <TouchableOpacity
            style={styles.btnSubmit}
            activeOpacity={0.7}
            onPress={() => this.onSubmitPressed()}
          >
            <Text style={styles.btnSubmitText}>Submit</Text>
          </TouchableOpacity>
          <SafeAreaView forceInset={{ bottom: 'always', top: 'never' }} />
        </View>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RestaurantRateScreen)
