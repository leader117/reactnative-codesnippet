import React, { Component } from 'react'
import { Text, TouchableOpacity, TextInput, View, Image } from 'react-native'
import { connect } from 'react-redux'
import RNGooglePlaces from 'react-native-google-places';
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/SearchScreenStyle'
import { CardView, Hr } from '../Components'
import { Images } from '../Themes'
import { Actions } from 'react-native-router-flux'
import { KeyboardAwareFlatList } from 'react-native-keyboard-aware-scroll-view'
import { bindActionCreators } from 'redux'
import { RestaurantActions } from '../Redux/Actions'
import _ from 'lodash'
import Api from '../Services/Api'
import firebase from 'react-native-firebase'

const api = Api.create()

class SearchScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      text: '',
      service_type: 'dine-in',
      restaurants: [],
    }
  }

  async componentDidMount () {
    await this.getRestaurantData()
  }

  async getRestaurantData(){
    const token = await firebase.auth().currentUser.getIdToken()
    await api.setToken(token)
    const reqData = {
      'service_type': this.state.service_type,
    }
    if(this.props.geo_loc)
      reqData['_loc'] = {
        lat: this.props.geo_loc.latitude,
        lon: this.props.geo_loc.longitude
      }
    const response = await api.searchRestaurant(reqData)
    if(response && response.data && response.data.data)
      this.setState({restaurants: response.data.data})
    return true
  }

  _showRestaurant (res_data) {
    this.props.profile ? Actions.restaurant({data: res_data, title: res_data.name.toUpperCase()}) : Actions.introduce()
  }

  render () {
    const restaurants = this.state.restaurants;
    const searchQuery = this.state.text.toLowerCase().trim();
    const data = restaurants.filter(el => el.name.toLowerCase().includes(searchQuery));

    return (
      <View style={styles.container}>
        <CardView style={[styles.cardSearch]}>
          <Image source={Images.icSearch} style={styles.icSearch} />
          <TextInput
            style={styles.textInput}
            onChangeText={(text) => this.setState({ text })}
            value={this.state.text}
            placeholder='Search '
            autoCapitalize={'none'}
          />
        </CardView>
        <View style={styles.listResult}>
          <View style={[styles.rowCard, { marginVertical: 5 }]}>
            <TouchableOpacity
              style={styles.btnType}
              activeOpacity={0.7}
              onPress={() => {
                this.setState({ service_type: 'dine-in', text: '' })
                this.getRestaurantData()
              }}
            >
              <Text style={this.state.service_type === 'dine-in' ? styles.btnTypeTextActive : styles.btnTypeText}>Dine-in</Text>
              <View style={[styles.lineActive, this.state.index !== 'dine-in' ? { backgroundColor: '#FAFAFA' } : null]} />
            </TouchableOpacity>
            <View style={[styles.line]} />
            <TouchableOpacity
              style={styles.btnType}
              activeOpacity={0.7}
              onPress={() => {
                this.setState({ service_type: 'pick-up', text: '' })
                this.getRestaurantData()
              }}
            >
              <Text style={this.state.service_type === 'pick-up' ? styles.btnTypeTextActive : styles.btnTypeText}>Pick-up</Text>
              <View style={[styles.lineActive, this.state.service_type !== 'pick-up' ? { backgroundColor: '#FAFAFA' } : null]} />
            </TouchableOpacity>
          </View>

          <KeyboardAwareFlatList
            data={data}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item }) => (
              <TouchableOpacity onPress={() => this._showRestaurant(item)}>
                <CardView style={styles.cardLocation}>
                  <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Image style={styles.restaurantImage} source={require('../Images/TAQ_ID.png')} resizeMode={'contain'} />
                    <Text style={styles.txtAddress}>{item.name}</Text>
                  </View>
                  <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    {item.discount && <View style={styles.viewPercent}>
                      <Text style={styles.percent}>{item.discount}%</Text>
                    </View>}
                    <Text style={[styles.txtAddress, { color: '#BDBDBD' }]}>{(item.distance||0).toFixed(1)} km</Text>
                  </View>
                </CardView>
              </TouchableOpacity>
            )}
          />
        </View>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    location: state.location,
    profile: state.profile.profile,
    restaurants: state.restaurant.restaurants
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    ...bindActionCreators(RestaurantActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchScreen)
