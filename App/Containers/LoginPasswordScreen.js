import React, { Component } from 'react'
import {
  Text,
  View,
  TextInput,
  KeyboardAvoidingView,
  TouchableOpacity,
  Image,
  TouchableWithoutFeedback,
  Keyboard,
  Platform
} from 'react-native'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/LoginScreenStyle'
import { Images } from '../Themes'
import Ionicons from 'react-native-vector-icons/Ionicons'
import { Actions } from 'react-native-router-flux'

class LoginPasswordScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      // textWrongPw: 'Oops! Wrong password. Try again!',
      // textHeader: 'Please type your password here.',
      password: props.password || ''
    }
  }

  render () {
    const { password } = this.state
    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={styles.container}>
          <KeyboardAvoidingView style={styles.keyboardContainer} enabled={Platform.OS === 'ios'} keyboardVerticalOffset={70} behavior='padding'>
            <Text style={styles.txtHeader}>Please type your confirmation password here.</Text>
            <View style={styles.middleView}>
              <View style={styles.rowInput}>
                <TextInput
                  style={[styles.textInput, { marginLeft: 0 }]}
                  onChangeText={(text) => this.setState({ password: text })}
                  value={password}
                  placeholder='Enter password'
                  underlineColorAndroid={'transparent'}
                  autoCapitalize='none'
                  autoCorrect={false}
                  secureTextEntry
                />
              </View>
              <Text style={styles.txtConnectSocial}>I forgot my password</Text>
            </View>

            <View style={styles.bottomView}>
              {!!password && (
                <TouchableOpacity style={styles.btnArrow} onPress={() => Actions.loginConfirmCode()}>
                  <Image source={Images.icArrowRight} style={styles.icArrowRight} />
                </TouchableOpacity>
              )}
            </View>
          </KeyboardAvoidingView>
        </View>
      </TouchableWithoutFeedback>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginPasswordScreen)
