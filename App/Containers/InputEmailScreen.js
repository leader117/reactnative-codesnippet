import React, { Component } from 'react'
import {
  TouchableWithoutFeedback,
  Text,
  View,
  TextInput,
  KeyboardAvoidingView,
  TouchableOpacity,
  Image,
  Keyboard,
  Platform,
  Alert
} from 'react-native'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/InputNameScreenStyle'
import { Images, Metrics } from '../Themes'
import { Actions } from 'react-native-router-flux'
import validator from 'validator'
import firebase from 'react-native-firebase'
import { bindActionCreators } from '../Redux/Actions'
import ProfileActions from '../Redux/ProfileRedux'
import _ from 'lodash'
import PhoneInput from 'react-native-phone-input'
import CountryPicker from 'react-native-country-picker-modal'

class InputEmailScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      confirmResult: props.confirmResult,
      first_name: '',
      last_name: '',
      phone_number: props.phoneNumber || '',
      cca2: 'US',
      email: ''
    }
  }

  componentDidMount () {
    // this.setState({
    //   pickerData: this.phone.getPickerData()
    // })
  }

  onPressFlag () {
    this.countryPicker.openModal()
  }

  selectCountry (country) {
    this.phone.selectCountry(country.cca2.toLowerCase())
    this.setState({ cca2: country.cca2 })
  }

  async onPressConfirm () {
    const { email } = this.state
    if (validator.isEmail(email)) {
      Actions.inputName({ email })
    } else {
      Alert.alert(
        'Notification',
        'Email address is required'
      )
    }
  }

  render () {
    const { email } = this.state
    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={styles.container}>
          <KeyboardAvoidingView style={styles.keyboardContainer} enabled={Platform.OS === 'ios'} keyboardVerticalOffset={70} behavior='padding'>
            <Text style={styles.txtHeader}>What’s your email address?</Text>
            <View style={styles.middleView}>
              <View style={styles.rowInput}>
                <TextInput
                  style={[styles.textInput, { marginLeft: 0 }]}
                  onChangeText={(email) => this.setState({ email })}
                  value={email}
                  placeholder='Email Address'
                  underlineColorAndroid={'transparent'}
                  autoCapitalize='none'
                  keyboardType='email-address'
                />
              </View>
            </View>

            <View style={styles.bottomView}>
              {!!email && (
                <TouchableOpacity style={styles.btnArrow} onPress={() => this.onPressConfirm()}>
                  <Image source={Images.icArrowRight} style={styles.icArrowRight} />
                </TouchableOpacity>
              )}
            </View>
          </KeyboardAvoidingView>
        </View>
      </TouchableWithoutFeedback>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    ...bindActionCreators(ProfileActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(InputEmailScreen)
