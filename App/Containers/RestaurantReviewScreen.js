import React, { Component } from 'react'
import { Text, Image, View, TouchableOpacity, TextInput } from 'react-native'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/RestaurantReviewScreenStyle'
import { Hr, CardView } from '../Components'
import { Images, Metrics, Colors } from '../Themes'
import SafeAreaView from 'react-native-safe-area-view'
import { Actions } from 'react-native-router-flux'
import { KeyboardAwareFlatList } from 'react-native-keyboard-aware-scroll-view'
import StarRating from '../Components/StarRating'
import numeral from 'numeral'
import ReadMore from 'react-native-read-more-text'

class RestaurantReviewScreen extends Component {
  state = {
    rating: 0
  }

  componentDidMount () {
  }

  render () {
    const comments = [
      {
        user: {
          name: 'Peter O’Conor',
          avatar: 'http://endlesstheme.com/simplify1.0/images/profile/profile4.jpg',
          point: 4822
        },
        comment: 'Food is delicious and place is super nice! Will be back soon to taste other plates!',
        rating: 5,
        like: 20,
        liked: false
      },
      {
        user: {
          name: 'Bryan Craig',
          avatar: 'http://endlesstheme.com/simplify1.0/images/profile/profile4.jpg',
          point: 4822
        },
        comment: 'A must visit for seafood lovers. Came Across this place while strolling around MOE. I felt excited seeing the sign “bla bla” as I Food is delicious and place is super nice! Will be back soon to taste other plates!',
        rating: 5,
        like: 10,
        liked: true
      }
    ]

    return (
      <View style={styles.container}>
        <KeyboardAwareFlatList
          data={comments}
          keyExtractor={(item, index) => index.toString()}
          ListHeaderComponent={() => (
            <CardView style={styles.cardView}>
              <Image
                borderRadius={5}
                style={styles.imageItem}
                source={{ uri: 'https://i2.wp.com/chewnibblenosh.com/wp-content/uploads/2015/06/Copycat-P.F.-Changs-Chicken-Lettuce-Wraps-Chew-Nibble-Nosh..png' }}
              />
              <View style={styles.row}>
                <Text style={styles.itemName}>Dali Chicken</Text>
                <View style={styles.row}>
                  <Image style={styles.imageStar} source={Images.starYellow} />
                  <Text style={styles.ratingText}>4.6</Text>
                </View>
              </View>
              <Text style={styles.itemNameInfo}>P.F. Chang’s</Text>
            </CardView>
          )}
          renderItem={({ item }) => {
            return (
              <View style={styles.item}>
                <View style={styles.row}>
                  <Image source={{ uri: item.user.avatar }} style={styles.itemAvatar} />
                  <View style={styles.itemUserInfo}>
                    <Text style={styles.userName}>{item.user.name}</Text>
                    <Text style={styles.userPoint}>{numeral(item.user.point).format('0,0')} Points</Text>
                  </View>
                </View>
                <View style={[styles.row, { marginTop: 10 }]}>
                  <View style={[styles.row, { flex: 1 }]}>
                    <Image style={[styles.imageStar, { marginRight: 3 }]} source={Images.starYellow} />
                    <Image style={[styles.imageStar, { marginRight: 3 }]} source={Images.starYellow} />
                    <Image style={[styles.imageStar, { marginRight: 3 }]} source={Images.starYellow} />
                    <Image style={[styles.imageStar, { marginRight: 3 }]} source={Images.starYellow} />
                  </View>
                  <Text style={styles.txtTime}>2 days ago.</Text>
                </View>
                <View style={{ height: 15 }} />
                <ReadMore
                  numberOfLines={3}
                  renderTruncatedFooter={(onPress) => this.renderViewMore(onPress)}
                  renderRevealedFooter={(onPress) => this.renderViewLess(onPress)}
                >
                  <Text style={styles.txtComment}>{item.comment}</Text>
                </ReadMore>
                <View style={[styles.row, { marginTop: 15 }]}>
                  <TouchableOpacity style={styles.row}>
                    <Image source={item.liked ? Images.liked : Images.like} style={styles.iconAction} />
                    <Text style={[styles.txtAction, item.liked && { color: '#ff4d0d' }]}>{item.liked ? 'Liked' : 'Like'}</Text>
                  </TouchableOpacity>
                  <View style={styles.oval} />
                  <Text style={styles.txtLike}>{item.like} {item.like <= 1 ? 'like' : 'likes' }</Text>
                </View>
              </View>
            )
          }}
        />
        <View style={{ height: 5 }} />
        {/* <View style={styles.bottomView}> */}
        {/* <View style={styles.inputWrap}> */}
        {/* <TextInput */}
        {/* style={styles.input} */}
        {/* placeholder='Please add your comments here.' */}
        {/* placeholderTextColor='#BDBDBD' */}
        {/* multiline */}
        {/* selectionColor={Colors.active} */}
        {/* /> */}
        {/* </View> */}
        {/* <SafeAreaView forceInset={{ bottom: 'always', top: 'never' }} /> */}
        {/* </View> */}
      </View>
    )
  }

  renderViewMore (onPress) {
    return (
      <Text style={styles.txtReadMore} suppressHighlighting onPress={onPress}>read more</Text>
    )
  }
  renderViewLess (onPress) {
    return (
      <Text style={styles.txtReadMore} suppressHighlighting onPress={onPress}>view less</Text>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RestaurantReviewScreen)
