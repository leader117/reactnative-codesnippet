import React, { Component } from 'react'
import {
  ScrollView,
  Text,
  Image,
  View,
  TouchableOpacity,
  Animated,
  Easing,
  StatusBar
} from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators, AuthActions } from '../Redux/Actions'
import { Actions } from 'react-native-router-flux'
// Styles
import styles from './Styles/LaunchScreenStyles'
import { Images, Metrics } from '../Themes'

class LaunchScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      isStartUp: true,
      color: '#000000'
    }
  }

  static getDerivedStateFromProps (nextProps, prevState) {
    if (nextProps.isStarting !== prevState.isStarting) {
      return {
        isStartUp: nextProps.isStarting
      }
    }
    return null
  }

  heightValue1 = new Animated.Value(0)
  heightValue2 = new Animated.Value(0)
  heightValue3 = new Animated.Value(0)

  componentDidMount = () => {
    setTimeout(() => this.setState({ color: '#FFFFFF' }), 400)
    Animated.sequence([
      Animated.timing(
        this.heightValue1,
        {
          toValue: Metrics.screenHeight,
          duration: 300,
          easing: Easing.linear
        }
      ),
      Animated.timing(
        this.heightValue2,
        {
          toValue: Metrics.screenHeight,
          duration: 300,
          delay: 100,
          easing: Easing.linear
        }
      ),
      Animated.timing(
        this.heightValue3,
        {
          toValue: Metrics.screenHeight,
          duration: 300,
          delay: 100,
          easing: Easing.linear
        }
      )
    ]).start()
  };

  // componentDidUpdate (prevProps, prevState) {
  //   if (this.state.isStartUp !== prevState.isStartUp) {
  //     setTimeout(() => {
  //       Actions.auth()
  //     }, 1000)
  //   }
  // }

  render () {
    return (
      <View style={styles.container}>
        <StatusBar hidden />
        <Animated.View
          style={[styles.background1, { height: this.heightValue1 }]}
        />
        <Animated.View
          style={[styles.background2, { height: this.heightValue2 }]}
        />
        <Animated.View
          style={[styles.background3, { height: this.heightValue3 }]}
        />
        <Text style={[styles.title, { color: this.state.color }]}>plate</Text>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    isStarting: state.startup.isStarting,
    auth: state.auth
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    ...bindActionCreators(AuthActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LaunchScreen)
