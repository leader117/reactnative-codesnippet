import React, { Component } from 'react'
import { Text, Image, View, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/PaymentPromoCodeScreenStyle'
import { Hr } from '../Components'
import { Images, Metrics, Colors } from '../Themes'
import SafeAreaView from 'react-native-safe-area-view'
import Ionicons from 'react-native-vector-icons/Ionicons'
import { Actions } from 'react-native-router-flux'
import TextInput from '../Components/card/TextInput'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

class PaymentPromoCodeScreen extends Component {
  state = {

  }

  render () {
    return (
      <View style={styles.container}>
        <KeyboardAwareScrollView>
          <Text style={styles.title}>Please enter your promo code</Text>
          <View style={styles.form}>
            <TextInput
              ref='code'
              isMask
              type={'only-numbers'}
              placeholder='Promo Code'
              showLabel
              value={this.state.code}
              onChangeText={(code) => {
                this.setState({ code })
              }}
            />

          </View>
          <SafeAreaView>
            <TouchableOpacity
              style={styles.btn}
              activeOpacity={0.7}
              onPress={() => Actions.paymentPromoCodeSuccessScreen()}
            >
              <Text style={styles.btnText}>Next</Text>
            </TouchableOpacity>
          </SafeAreaView>
        </KeyboardAwareScrollView>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PaymentPromoCodeScreen)
