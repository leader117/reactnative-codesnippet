import React, { Component } from 'react'
import { ScrollView, Text, KeyboardAvoidingView, Image, View } from 'react-native'
import { connect } from 'react-redux'
import { DEFAULT_CURRENCY_NAME } from '../Config/constatnts';
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/PlateCashScreenStyle'
import { Images } from '../Themes'
import firebase from 'react-native-firebase'
import moment from 'moment'
class PlateCashScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      plateCash: null
    }
  }

  componentDidMount (){
      const currentUser = firebase.auth().currentUser
      firebase.database().ref(`users/${currentUser._user.uid}/plate_cash`).on('value', snapshot => {
        if(snapshot.exists())
          this.setState({ plateCash: snapshot.val() }) 
      })
  }

  render () {
    const { plateCash } = this.state
    let currencyName, cashAmount, showExpirationContent = false, text1, text2;

    if (plateCash && plateCash.amount > 0) {
      currencyName = plateCash.currency;
      cashAmount = plateCash.amount.toFixed(2);
      text1 = "Available Plate Cash"
      text2 = "Use your earned cash on your next bill!"
      if(plateCash.expiration && plateCash.expiration.date && plateCash.expiration.price && plateCash.expiration.price.amount && plateCash.expiration.price.currency)
        showExpirationContent = true;
    } else {
      currencyName = DEFAULT_CURRENCY_NAME;
      cashAmount = '0.00'
      text1 = "You don’t have any Plate Cash available :("
      text2 = "Earn some on your bill the next time you dine-in!"
    }
    
    return (
      <ScrollView style={styles.container}>
        <Text style={styles.title}>Plate Wallet</Text>
        <Image source={Images.icWallet} style={styles.icWallet} resizeMode={'contain'} />
        <View style={styles.viewMoney}>
          <Text style={styles.label}>{currencyName} <Text style={styles.number}>{cashAmount}</Text></Text>
        </View>
          <View style={styles.viewDetail}>
            <Text style={styles.detail}>{text1}</Text>
            <Text style={styles.date}>{text2}</Text>
            {showExpirationContent &&
            <Text style={[styles.date, { marginTop: 15 }]}>
              {plateCash.expiration.price.currency + ' ' + plateCash.expiration.price.amount} Plate Cash will expire on {moment(plateCash.expiration.date).format('MMMM Do, YYYY')}</Text>
            }
        </View>
      </ScrollView>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PlateCashScreen)
