import React, { Component } from 'react'
import { ScrollView, Text, KeyboardAvoidingView, Image, View, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
import firebase from 'react-native-firebase'
import moment from 'moment'
const db = firebase.database()

import styles from './Styles/ActiveOrdersScreenStyle'
import { CardView } from '../Components'
import { Fonts, Images, Metrics } from '../Themes'
import { Actions } from 'react-native-router-flux';

class ActiveOrdersScreen extends Component {

  constructor(props){
    super(props)
    this.state = {
      active_orders: [],
      
    }
  }

  componentDidMount(){
    this.getActiveOrders()
  }

  getActiveOrders(){
    const uid = firebase.auth().currentUser.uid
    db.ref(`users/${uid}/order_history`).on("value",async snapshot => {
      const val = snapshot.val()
      if(val){
        const arr = Object.entries(val)
        var new_arr = []
        for(let i = 0; i < arr.length; i++)
        {
          const order_id = arr[i][0]
          if(arr[i][1].status != "closed"){
            try {
              var order = await db.ref(`orders/${order_id}`).once("value")
              if(order.val())
              {
                order = order.val()
                const host_user =  await db.ref(`users/${order.host_user_id}/user_meta`).once("value")
                if(host_user.val())
                  order['host_user_meta'] = host_user.val()
                order['order_id'] = order_id
                new_arr.push(order)
              }
            } catch (error) {
              
            }
          }
        }
        this.setState({active_orders: new_arr})
      }
    })
  }

  async onViewOrderPressed(order){
    try {
      const res = await db.ref(`restaurants/${order.restaurant_id}`).once("value")
      if(res.val())
      {
        Actions.restaurant({data: res.val(), title: order.restaurant_name.toUpperCase(),order: order, getActiveOrders: () => this.getActiveOrders()})
      }
    } catch (error) {
      
    }
    
  }

  onInvitationCancelPressed(user_id, order){
    const uid = firebase.auth().currentUser.uid
    db.ref(`orders/${order.order_id}/users/${user_id}`).remove()
    db.ref(`users/${user_id}/order_history/${order.order_id}`).remove()
    this.getActiveOrders()
  }

  onAcceptPressed(order){
    const uid = firebase.auth().currentUser.uid
    db.ref(`orders/${order.order_id}/users/${uid}`).update({status: "accepted"})
    db.ref(`users/${uid}/order_history/${order.order_id}`).update({status: "accepted"})
    this.getActiveOrders()
  }

  onDeclinePressed(order){
    const uid = firebase.auth().currentUser.uid
    db.ref(`orders/${order.order_id}/users/${uid}`).remove()
    db.ref(`users/${uid}/order_history/${order.order_id}`).remove()
    this.getActiveOrders()
  }

  render () {
    const uid = firebase.auth().currentUser.uid

    var hasInvites = false
    var hasPickUpOrders = false
    var hasDineInOrders = false

    const sendingInvites = !this.state.active_orders? null : this.state.active_orders.map(item => {
      if(item.service_type == "dine-in" && item.host_user_id == uid){
        if(item.users)
          return Object.entries(item.users).map(user => {
            if(user[1].status == "invited"){
              hasInvites = true
              return (
                <CardView style={styles.cardStyle} key={user[0]}>
                  <Image source={item.host_user_meta.avatar_url ? { uri: item.host_user_meta.avatar_url } : Images.profile} style={styles.icAva} />
                  <View style={{ flex: 1 }}>
                    <View style={styles.rowInfo}>
                      <Text style={styles.txtName}>{item.restaurant_name}</Text>
                      <Text style={styles.txtTime}>{moment(item.created_at).format("D MMM")}</Text>
                    </View>
                    <View style={styles.rowInfo}>
                      <Text style={[styles.txtTime, { fontSize: 14 }]}>Sent to {user[1].user_meta.first_name} {user[1].user_meta.last_name[0].toUpperCase()}.</Text>
                      <Text style={styles.txtTime}>11:50 AM</Text>
                    </View>
                    <View style={[styles.rowInfo, { marginTop: Metrics.baseMargin }]}>
                      <TouchableOpacity style={[styles.btnCard, { borderColor: '#979797' }]} onPress={()=>this.onInvitationCancelPressed(user[0], item)}>
                        <Text style={{ color: '#979797' }}>Cancel</Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </CardView>
                )
              }
            })
      }
    })

    const pick_upOrders = !this.state.active_orders? null : this.state.active_orders.map(item => {
      if(item.service_type == "pick-up"){
        hasPickUpOrders = true
        return (
          <CardView style={styles.cardStyle} key={item.created_at}>
            <Image source={item.host_user_meta.avatar_url ? { uri: item.host_user_meta.avatar_url } : Images.profile} style={styles.icAva} />
            <View style={{ flex: 1 }}>
              <View style={styles.rowInfo}>
                <Text style={styles.txtName}>{item.restaurant_name}</Text>
                <Text style={styles.txtTime}>{moment(item.created_at).format("D MMM")}</Text>
              </View>
              <View style={styles.rowInfo}>
                <Text style={[styles.txtTime, { fontSize: 14 }]}>Hosted by {item.host_user_meta.first_name} {item.host_user_meta.last_name[0].toUpperCase()}.</Text>
                <Text style={styles.txtTime}>{moment(item.created_at).format("h:m A")}</Text>
              </View>
              <View style={[styles.rowInfo, { marginTop: Metrics.baseMargin }]}>
                <TouchableOpacity style={[styles.btnCard, { borderColor: '#0f1524' }]}>
                  <Text style={{ color: '#0f1524' }}>View Order</Text>
                </TouchableOpacity>
              </View>
            </View>
          </CardView>
        )
      }
    })

    const dine_inOrders = !this.state.active_orders? null : this.state.active_orders.map(item => {
      if(item.service_type == "dine-in"){
        hasDineInOrders = true
        return (
          <CardView style={styles.cardStyle} key={item.created_at}>
            <Image source={item.host_user_meta.avatar_url ? { uri: item.host_user_meta.avatar_url } : Images.profile} style={styles.icAva} />
            <View style={{ flex: 1 }}>
              <View style={styles.rowInfo}>
                <Text style={styles.txtName}>{item.restaurant_name}</Text>
                <Text style={styles.txtTime}>{moment(item.created_at).format("D MMM")}</Text>
              </View>
              <View style={styles.rowInfo}>
                <Text style={[styles.txtTime, { fontSize: 14 }]}>Hosted by {item.host_user_meta.first_name} {item.host_user_meta.last_name[0].toUpperCase()}.</Text>
                <Text style={styles.txtTime}>{moment(item.created_at).format("h:m A")}</Text>
              </View>
              {
                item.host_user_id != uid && item.users[uid] && item.users[uid].status == "invited"?
                <View style={[styles.rowInfo, { marginTop: Metrics.baseMargin }]}>
                  <TouchableOpacity style={styles.btnCard} onPress = {()=>this.onAcceptPressed(item)}>
                    <Text style={styles.txtBtn}>Accept</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={[styles.btnCard, { borderColor: '#979797' }]} onPress = {()=>this.onDeclinePressed(item)}>
                    <Text style={{ color: '#979797' }}>Decline</Text>
                  </TouchableOpacity>
                </View>:
                <View style={[styles.rowInfo, { marginTop: Metrics.baseMargin }]}>
                  <TouchableOpacity style={[styles.btnCard, { borderColor: '#0f1524' }]} onPress={()=> this.onViewOrderPressed(item)}>
                    <Text style={{ color: '#0f1524' }}>View Order</Text>
                  </TouchableOpacity>
                </View>
              }
            </View>
          </CardView>
        )
      }
    })

    return (
      <ScrollView style={styles.container}>
        {hasDineInOrders?<Text style={styles.title}>Dine-in Tables</Text>:null}
        {dine_inOrders}        
        {hasPickUpOrders?<Text style={styles.title}>Pick-up Orders</Text>:null}
        {pick_upOrders}
        {hasInvites?<Text style={styles.title}>Sent Invitations <Text style={[styles.title, { fontFamily: Fonts.type.avenirLightOblique }]}>(Pending)</Text></Text>:null}
        {sendingInvites}
      </ScrollView>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ActiveOrdersScreen)
