import React, { Component } from 'react'
import { ScrollView, Text, KeyboardAvoidingView, View, TouchableOpacity, Image } from 'react-native'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/ProfileScreenStyle'
import { Images } from '../Themes'
import { Actions } from 'react-native-router-flux'

class ProfileScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
    }
  }

  render () {
    const { profile = {}} = this.props;
    const { location: { city, country } = {}, plate_cash } = profile;
    const { amount = '0.00', currency = 'AED' } = plate_cash || {};
    const location = city + ', ' + country;
    
    return (
      <View style={styles.container}>
        <View style={styles.cardDetail}>
          <TouchableOpacity onPress={() => {}}>
            <Image source={this.props.avatar ? { uri: this.props.avatar } : Images.profile} defaultSource={Images.profile} style={styles.icAvaMan} resizeMode='cover' />
          </TouchableOpacity>
          <View>
            <Text style={styles.name}>{profile.user_meta.first_name + ' ' + profile.user_meta.last_name}</Text>
            <View style={[styles.viewLocation, { marginBottom: 8 }]}>
              <Image source={Images.icLocationProfile} style={{ width: 9.6, height: 12 }} resizeMode={'contain'} />
              <Text style={styles.location}>{location || '---'}</Text>
            </View>
            <View style={styles.viewLocation}>
              <TouchableOpacity
                onPress={() => Actions.profileEdit()}
                style={styles.buttonEdit}>
                <Text style={styles.edit}>Edit Profile</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => Actions.friendList()}
                style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Image source={Images.icPeople} style={{ width: 20, height: 18, marginRight: 6 }} resizeMode={'contain'} />
                <Text style={styles.edit}>{this.props.friends.length}</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View style={styles.cardPlate}>
          <Text style={styles.title}>Plate Wallet</Text>
          <View style={[styles.viewLocation, { marginLeft: 16.5 }]}>
            <View style={styles.viewMoney}>
              <Text style={styles.label}>{currency} <Text style={styles.number}>{(amount||0).toFixed(2)}</Text></Text>
            </View>
            <View style={styles.viewDetail}>
              <Text style={styles.detail}>Available Plate Cash</Text>
              <Text style={styles.date}>Use your earned cash on your next bill!</Text>
            </View>
          </View>
        </View>
        <View style={styles.line} />
        <View style={styles.cardButton}>
          <TouchableOpacity style={styles.button}>
            <Text style={styles.textButton}>Plate History</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.buttonArrow}>
            <Text style={styles.textButtonArrow}>P.F. Chang’s</Text>
            <Image source={Images.icArrowProfile} style={{ width: 10, height: 17 }} resizeMode={'contain'} />
          </TouchableOpacity>
          <TouchableOpacity style={styles.buttonArrow}>
            <Text style={styles.textButtonArrow}>Cafe Bateel</Text>
            <Image source={Images.icArrowProfile} style={{ width: 10, height: 17 }} resizeMode={'contain'} />
          </TouchableOpacity>
          <TouchableOpacity style={styles.buttonArrow}>
            <Text style={styles.textButtonArrow}>Vapiano’s</Text>
            <Image source={Images.icArrowProfile} style={{ width: 10, height: 17 }} resizeMode={'contain'} />
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    profile: state.profile.profile,
    user: state.profile.profile.user_meta,
    avatar: state.profile.profile.user_meta.avatar_url,
    friends: state.friend.friends
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfileScreen)
