import React, { Component } from 'react'
import { Text, Image, View, TouchableOpacity, FlatList } from 'react-native'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/RestaurantFriendScreenStyle'
import { CardView } from '../Components'
import { Images } from '../Themes'
import SafeAreaView from 'react-native-safe-area-view'
import Ionicons from 'react-native-vector-icons/Ionicons'

class RestaurantFriendScreen extends Component {
  state = {
    index: 0
  }

  _renderUsers (data) {
    return (
      <FlatList
        style={styles.list}
        data={data}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item }) => {
          const user = item
          return (
            <View style={styles.rowUser}>
              <Image source={{ uri: user.avatar }} style={styles.rowUserAvatar} />
              <View style={styles.rowUserContent}>
                <Text style={styles.rowUserName}>{user.name}</Text>
                <Text style={styles.rowUserPlate}>{user.plate} Plate</Text>
              </View>
              <TouchableOpacity
                style={styles.rowUserIconAdd}
                activeOpacity={0.7}
              >
                <Ionicons name='md-add' size={25} color='#BDBDBD' />
              </TouchableOpacity>
            </View>
          )
        }}
        ListFooterComponent={() => <SafeAreaView forceInset={{ bottom: 'always' }} />}
      />
    )
  }

  render () {
    return (
      <View style={styles.container}>
        <CardView style={[styles.cardStyle, styles.borderShadow]} />
        {
          !this.props.friends? null: 
          <FlatList
            style={styles.list}
            data={Object.entries(this.props.friends)}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item }) => {
              const user = item[1].user_meta
              return (
                <View style={styles.rowUser}>
                  <Image source={user.avatar_url ? { uri: user.avatar_url } : Images.profile} style={styles.rowUserAvatar} />
                  <View style={styles.rowUserContent}>
                    <Text style={styles.rowUserName}>{user.first_name} {user.last_name}</Text>
                  </View>
                </View>
              )
            }}
            ListFooterComponent={() => <SafeAreaView forceInset={{ bottom: 'always' }} />}
          />
        }
        
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RestaurantFriendScreen)
