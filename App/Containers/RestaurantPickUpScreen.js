import React, { Component } from 'react'
import { ScrollView, Text, Image, View, TouchableOpacity, FlatList } from 'react-native'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/RestaurantPickUpScreenStyle'
import { Hr, CardView } from '../Components'
import { Images, Metrics, Colors, Fonts } from '../Themes'
import SafeAreaView from 'react-native-safe-area-view'
import Ionicons from 'react-native-vector-icons/Ionicons'
import Feather from 'react-native-vector-icons/Feather'
import { Actions } from 'react-native-router-flux'
import Accordion from 'react-native-collapsible/Accordion'
import Modal from 'react-native-modal'

class RestaurantPickUpScreen extends Component {
  state = {
    activeSections: [0],
    indexAdd: 0
  }

  componentDidMount () {

  }

  setSections = sections => {
    this.setState({
      activeSections: sections.includes(undefined) ? [] : sections
    })
  };

  renderHeader = (section, _, isActive) => {
    return (
      <View>
        <View style={[styles.content]}>
          <View style={[styles.rowTable, { marginVertical: 15 }]}>
            <Text style={[section.user ? styles.rowTableText : styles.rowTableTextBold, { flex: 1 }]}>
              {section.user || 'Your orders'}:
            </Text>
            <Text style={[styles.rowTableText, { marginHorizontal: 5 }]}>AED {section.total}</Text>
            <Ionicons name={isActive ? 'ios-arrow-down' : 'ios-arrow-forward'} size={20} color='#0F1524' />
          </View>
        </View>
        <Hr marginTop={0} marginLeft={0} marginRight={0} lineHeight={5} />
      </View>
    )
  }

  renderContent = (section, _, isActive) => {
    return (
      <View>
        <FlatList
          style={{ height: section.items.length * 66 }}
          scrollEnabled={false}
          data={section.items}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item, index }) => {
            return (
              <TouchableOpacity
                style={styles.itemWrap}
                activeOpacity={0.7}
                onPress={() => this._showItemActions(item)}
              >
                <View style={[styles.rowItem, { justifyContent: 'space-between' }]}>
                  <View style={styles.rowItem}>
                    <Text style={styles.textNumber}>{index + 1}x</Text>
                    <Text style={styles.rowItemName}>{item.name}</Text>
                  </View>
                  <Text style={styles.rowItemPrice}>AED {item.price}</Text>
                </View>
              </TouchableOpacity>
            )
          }}
        />
        <Hr marginTop={0} marginLeft={0} marginRight={0} lineHeight={isActive ? 4 : 1} />
      </View>
    )
  }

  _showItemActions = (item) => {
    if (item.status === 'being_prepared') {
      this.setState({ showModalOrderTime: true })
    }
    if (item.status === 'served') {
      this.setState({ showModalOrderAdd: true })
    }
  }

  render () {
    const sections = [
      {
        user: null,
        total: 69.71,
        items: [
          {
            name: 'Dynamite Shrimp',
            price: 56.80,
            rating: 4.2,
            status: 'being_prepared'
          },
          {
            name: 'Crap Wontons',
            price: 56.80,
            rating: 4.2,
            status: 'served'
          }
        ]
      }
    ]
    return (
      <View style={styles.container}>
        <ScrollView contentContainerStyle={[styles.scrollView, { paddingBottom: this.state.footerHeight || 0 }]}>
          <Accordion
            activeSections={this.state.activeSections}
            sections={sections}
            touchableComponent={TouchableOpacity}
            renderHeader={this.renderHeader}
            renderContent={this.renderContent}
            duration={400}
            onChange={this.setSections}
          />
          <View style={styles.content}>
            <View style={[styles.rowTable, { marginTop: 25 }]}>
              <Text style={[styles.rowTableText, { flex: 1 }]}>Total</Text>
              <View style={styles.totalWrap}>
                <Text style={[styles.rowTableText, { marginHorizontal: 5, color: Colors.white }]}>AED 205.80</Text>
              </View>
            </View>
          </View>
        </ScrollView>
        <View style={styles.footer} onLayout={(e) => this.setState({ footerHeight: e.nativeEvent.layout.height })}>
          <View style={styles.footerContent}>
            <SafeAreaView forceInset={{ bottom: 'always' }}>
              <TouchableOpacity
                style={styles.btnBill}
                activeOpacity={0.7}
                onPress={() => {
                  if (this.props.isPickUp) {
                    this.setState({ showModalPay: true })
                  } else {
                    this.props.onSendToKitchen && this.props.onSendToKitchen()
                    Actions.pop()
                  }
                }}
              >
                <Text style={styles.btnBillText}>{this.props.isPickUp ? 'Order & Pay' : 'Send to Kitchen'}</Text>
              </TouchableOpacity>
            </SafeAreaView>

          </View>
        </View>
        <Modal
          isVisible={this.state.showModalPay}
          style={styles.bottomModal}
          onBackdropPress={() => this.setState({ showModalPay: false })}
        >
          <View style={styles.modalContent}>
            <View style={[styles.modalRowHeader]}>
              <Text style={styles.modalRowHeaderText}>Payment</Text>
              <TouchableOpacity
                activeOpacity={0.7}
                onPress={() => this.setState({ showModalPay: false })}
              >
                <Ionicons name='ios-close' size={25} color='#292D44' style={{ marginHorizontal: 20, marginVertical: 10 }} />
              </TouchableOpacity>
            </View>
            <Text style={styles.txtContent}>You are about to pay <Text style={[styles.txtContent, { fontSize: 18, fontFamily: Fonts.type.avenirHeavy }]}>AED 70.83</Text> with your selected payment option.</Text>
            <Hr marginTop={15} marginLeft={20} marginRight={20} />
            <View style={styles.rowCard}>
              <Text style={[styles.rowCardText, { flex: 1 }]}>Credit Card</Text>
              <Text
                onPress={() => {
                  this.setState({ showModalPay: false })
                  setTimeout(() => {
                    Actions.paymentScreen()
                  }, 500)
                }}
                suppressHighlighting
                style={styles.rowCardText}>
                Change
              </Text>
            </View>
            <Text style={[styles.rowCardTextNumber]}>•••• •••• •••• 0042</Text>
            <TouchableOpacity
              style={[styles.btnBill, { marginTop: 30 }]}
              activeOpacity={0.7}
              onPress={() => {
                this.setState({ showModalPay: false })
                setTimeout(() => {
                  Actions.restaurantPickUpPaySuccessScreen()
                }, 500)
              }}
            >
              <Text style={[styles.btnBillText]}>Pay</Text>
            </TouchableOpacity>
            <SafeAreaView forceInset={{ bottom: 'always' }} />
          </View>
        </Modal>
      </View>
    )
  }

  renderViewMore (onPress) {
    return (
      <Text style={styles.txtReadMore} suppressHighlighting onPress={onPress}>read more</Text>
    )
  }
  renderViewLess (onPress) {
    return (
      <Text style={styles.txtReadMore} suppressHighlighting onPress={onPress}>view less</Text>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RestaurantPickUpScreen)
