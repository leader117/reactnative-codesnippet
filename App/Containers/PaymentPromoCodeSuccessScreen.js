import React, { Component } from 'react'
import { Text, Image, View, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/PaymentPromoCodeSuccessScreenStyle'
import { Hr } from '../Components'
import { Images, Metrics, Colors } from '../Themes'
import { Actions } from 'react-native-router-flux'

class PaymentPromoCodeSuccessScreen extends Component {
  state = {

  }

  render () {
    return (
      <View style={styles.container}>
        <View style={{ alignSelf: 'center' }}>
          <Image style={styles.imageCheck} source={Images.iconCheck} />

        </View>
        <Text style={styles.txtSuccess}>Success</Text>
        <Text style={styles.txtInfo}>You have successfully added your promo codel!</Text>
        <Hr lineHeight={8} marginBottom={5} marginTop={20} marginLeft={0} marginRight={0} />
        <Text style={styles.txtInfo}>Enjoy 20AED of your next order</Text>
        <Text style={styles.txtCode}>20OFF</Text>

        <TouchableOpacity
          style={styles.btn}
          activeOpacity={0.7}
          onPress={() => Actions.popTo('paymentScreen')}
        >
          <Text style={styles.btnText}>Done</Text>
        </TouchableOpacity>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PaymentPromoCodeSuccessScreen)
