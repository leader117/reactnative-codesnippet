// a library to wrap and simplify api calls
import apisauce from 'apisauce'
import Config from 'react-native-config'

// our "constructor"
const create = (baseURL = Config.API_URL) => {

  const api = apisauce.create({
    // base URL is read from the "constructor"
    baseURL,
    // here are some default headers
    headers: {
      'Cache-Control': 'no-cache'
    },
    // 10 second timeout...
    timeout: 30000
  })

  const apiGoogle = apisauce.create({ baseURL: 'https://maps.googleapis.com/maps/api/' })

  const setToken = token => api.setHeader('authorization', token)

  const getProfile = () => api.get('/user')
  const updateProfile = (profile) => api.post('/user', profile)

  const getLocation = location => apiGoogle.get('geocode/json', {
    latlng: `${location.latitude},${location.longitude}`,
    key: 'AIzaSyCacF1EOb6t73VWzcSqzUkIPl__frsoKvY'
  })
  const searchRestaurant = (data) => api.post('/restaurant/search', data)
  const syncContacts = (data) => api.post('/syncContacts', { data })
  const getUser = (uid) => api.post('/get-user-by-id', { uid })
  const addFriend = (uid) => api.post('/add-friend', { uid })
  const removeFriend = (uid) => api.post('/remove-friend', { uid })
  // ------
  // STEP 3
  // ------
  //
  // Return back a collection of functions that we would consider our
  // interface.  Most of the time it'll be just the list of all the
  // methods in step 2.
  //
  // Notice we're not returning back the `api` created in step 1?  That's
  // because it is scoped privately.  This is one way to create truly
  // private scoped goodies in JavaScript.
  //
  return {
    setToken,
    // a list of the API functions from step 2
    getProfile,
    updateProfile,
    searchRestaurant,
    getLocation,
    syncContacts,
    getUser,
    addFriend,
    removeFriend
  }
}

// let's return back our create method as the default.
export default {
  create
}
