const type = {
  base: 'Avenir-Book',
  bold: 'Avenir-Black',
  italic: 'PlayfairDisplay-Italic',
  emphasis: 'HelveticaNeue-Italic',
  avenirBlackOblique: 'Avenir-BlackOblique',
  avenirBookOblique: 'Avenir-BookOblique',
  avenirBook: 'Avenir-Book',
  avenirHeavy: 'Avenir-Heavy',
  avenirHeavyOblique: 'Avenir-HeavyOblique',
  avenirLight: 'Avenir-Light',
  avenirLightOblique: 'Avenir-LightOblique',
  avenirMedium: 'Avenir-Medium',
  avenirMediumOblique: 'Avenir-MediumOblique',
  avenirOblique: 'Avenir-Oblique',
  avenirRoman: 'Avenir-Roman',
  mohrRounded: 'MohrRounded-Medium',
  mohrRoundedLight: 'MohrRounded-Light'
}

const size = {
  h1: 38,
  h2: 34,
  h3: 30,
  h4: 26,
  h5: 20,
  h6: 19,
  input: 18,
  regular: 17,
  medium: 14,
  small: 12,
  tiny: 8.5,
  tFour: 24,
  sixteen: 16
}

const style = {
  h1: {
    fontFamily: type.base,
    fontSize: size.h1
  },
  h2: {
    fontWeight: 'bold',
    fontSize: size.h2
  },
  h3: {
    fontFamily: type.emphasis,
    fontSize: size.h3
  },
  h4: {
    fontFamily: type.base,
    fontSize: size.h4
  },
  h5: {
    fontFamily: type.base,
    fontSize: size.h5
  },
  h6: {
    fontFamily: type.emphasis,
    fontSize: size.h6
  },
  normal: {
    fontFamily: type.base,
    fontSize: size.regular,
    backgroundColor: 'transparent'
  },
  bold: {
    fontFamily: type.bold,
    fontSize: size.regular,
    backgroundColor: 'transparent',
    fontWeight: 'bold'
  },
  description: {
    fontFamily: type.base,
    fontSize: size.medium,
    backgroundColor: 'transparent'
  }
}

export default {
  type,
  size,
  style
}
