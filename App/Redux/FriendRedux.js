import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  syncContacts: ['data', 'dataContacts'],
  syncContactsSuccess: ['contacts'],

  getFriends: null,
  getFriendsSuccess: ['friends'],

  addFriend: ['uid', 'contacts', 'dataContacts'],

  removeFriend: ['uid', 'contacts', 'dataContacts']
})

export const FriendTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  contacts: [],
  friends: []
})

/* ------------- Selectors ------------- */

export const FriendSelectors = {
  getIsLoading: state => state.isLoading
}

/* ------------- Reducers ------------- */

export const syncContacts = (state) => state
export const syncContactsSuccess = (state, { contacts }) => state.merge({ contacts })

export const getFriends = (state) => state
export const getFriendsSuccess = (state, { friends }) => state.merge({ friends })
/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.SYNC_CONTACTS]: syncContacts,
  [Types.SYNC_CONTACTS_SUCCESS]: syncContactsSuccess,

  [Types.GET_FRIENDS]: getFriends,
  [Types.GET_FRIENDS_SUCCESS]: getFriendsSuccess,

  [Types.ADD_FRIEND]: getFriends,

  [Types.REMOVE_FRIEND]: getFriends
})
