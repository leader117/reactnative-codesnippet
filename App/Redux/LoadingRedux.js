import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  show: null,
  hide: null
})

export const LoadingTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  isLoading: false
})

/* ------------- Selectors ------------- */

export const LoadingSelectors = {
  getIsLoading: state => state.isLoading
}

/* ------------- Reducers ------------- */

export const show = (state, { data }) =>
  state.merge({ isLoading: true })

export const hide = (state, { data }) =>
  state.merge({ isLoading: false })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.SHOW]: show,
  [Types.HIDE]: hide
})
