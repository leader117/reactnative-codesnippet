import { combineReducers } from 'redux'
import configureStore from './CreateStore'
import rootSaga from '../Sagas/'

/* ------------- Assemble The Reducers ------------- */
export const reducers = combineReducers({
  search: require('./SearchRedux').reducer,
  startup: require('./StartupRedux').reducer,
  auth: require('./AuthRedux').reducer,
  location: require('./LocationRedux').reducer,
  profile: require('./ProfileRedux').reducer,
  loading: require('./LoadingRedux').reducer,
  restaurant: require('./RestaurantRedux').reducer,
  friend: require('./FriendRedux').reducer
})

export default () => {
  let { store, sagasManager, sagaMiddleware } = configureStore(reducers, rootSaga)

  if (module.hot) {
    module.hot.accept(() => {
      const nextRootReducer = require('./').reducers
      store.replaceReducer(nextRootReducer)

      const newYieldedSagas = require('../Sagas').default
      sagasManager.cancel()
      sagasManager.done.then(() => {
        sagasManager = sagaMiddleware.run(newYieldedSagas)
      })
    })
  }

  return store
}
