import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  clearProfile: [],
  checkProfile: [],
  getProfile: [],
  getProfileSuccess: ['profile'],
  getProfileFailure: ['error'],
  updateProfile: ['profile'],
  updateProfileSuccess: ['profile'],
  updateProfileFailure: ['error'],
  uploadAvatar: ['data'],
  uploadAvatarSuccess: ['profile'],
  uploadAvatarFailure: ['error']
})

export const ProfileTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: null
})

/* ------------- Selectors ------------- */

export const ProfileSelectors = {
  getData: state => state.data
}

/* ------------- Reducers ------------- */

// request the data from an api
export const checkProfile = (state) => state

export const getProfile = (state) => state.merge({ fetching: true })
// successful api lookup
export const getProfileSuccess = (state, { profile }) => state.merge({ fetching: false, error: null, profile })
// Something went wrong somewhere.
export const getProfileFailure = (state, { error }) => state.merge({ fetching: false, error })

// request the data from an api
export const updateProfile = (state) => state.merge({ fetching: true })
// successful api lookup
export const updateProfileSuccess = (state, { profile }) => state.merge({ fetching: false, error: null })
// Something went wrong somewhere.
export const updateProfileFailure = (state, { error }) => state.merge({ fetching: false, error })

// request the data from an api
export const uploadAvatar = (state) => state.merge({ fetching: true })
// successful api lookup
export const uploadAvatarSuccess = (state, { profile }) => state.merge({ fetching: false, error: null, profile })
// Something went wrong somewhere.
export const uploadAvatarFailure = (state, { error }) => state.merge({ fetching: false, error })

export const clearProfile = (state) => INITIAL_STATE

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.CHECK_PROFILE]: checkProfile,
  [Types.GET_PROFILE]: getProfile,
  [Types.GET_PROFILE_SUCCESS]: getProfileSuccess,
  [Types.GET_PROFILE_FAILURE]: getProfileFailure,

  [Types.UPDATE_PROFILE]: updateProfile,
  [Types.UPDATE_PROFILE_SUCCESS]: updateProfileSuccess,
  [Types.UPDATE_PROFILE_FAILURE]: updateProfileFailure,

  [Types.UPLOAD_AVATAR]: uploadAvatar,
  [Types.UPLOAD_AVATAR_SUCCESS]: uploadAvatarSuccess,
  [Types.UPLOAD_AVATAR_FAILURE]: uploadAvatarFailure,

  [Types.CLEAR_PROFILE]: clearProfile
})
