import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  searchRestaurantRequest: ['data'],
  searchRestaurantSuccess: ['restaurants'],
  searchRestaurantFailure: null
})

export const RestaurantTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  restaurants: [],
  fetching: null,
  payload: null,
  error: null
})

/* ------------- Selectors ------------- */

export const RestaurantSelectors = {
  getData: state => state.data
}

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state) => state
// successful api lookup
export const success = (state, { restaurants }) => state.merge({ fetching: false, error: null, restaurants })
// Something went wrong somewhere.
export const failure = state => state.merge({ fetching: false, error: true, payload: null })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.SEARCH_RESTAURANT_REQUEST]: request,
  [Types.SEARCH_RESTAURANT_SUCCESS]: success,
  [Types.SEARCH_RESTAURANT_FAILURE]: failure
})
