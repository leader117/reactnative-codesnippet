import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  setCountry: ['country'],
  locationRequest: ['data'],
  locationSuccess: ['payload'],
  locationFailure: null
})

export const LocationTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: null,
  country: null
})

/* ------------- Selectors ------------- */

export const LocationSelectors = {
  getData: state => state.data
}

/* ------------- Reducers ------------- */

export const setCountry = (state, { country }) => state.merge({ country })

// request the data from an api
export const request = (state, { data }) =>
  state.merge({ fetching: true, data, payload: null })

// successful api lookup
export const success = (state, action) => {
  const { payload } = action
  return state.merge({ fetching: false, error: null, payload })
}

// Something went wrong somewhere.
export const failure = state =>
  state.merge({ fetching: false, error: true, payload: null })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.SET_COUNTRY]: setCountry,
  [Types.LOCATION_REQUEST]: request,
  [Types.LOCATION_SUCCESS]: success,
  [Types.LOCATION_FAILURE]: failure
})
