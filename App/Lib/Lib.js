export const mapObjectToArray = data => {
  const dataArray = []
  for (const key in data) {
    const object = data[key]
    object.key = key
    dataArray.push(object)
  }
  return dataArray
}
