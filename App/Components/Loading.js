// @flow

import React from 'react'
import { connect } from 'react-redux'
import Spinner from 'react-native-loading-spinner-overlay'

class Loading extends React.Component {
  render () {
    return (
      <Spinner
        visible={this.props.isLoading}
      />
    )
  }
}

const mapStateToProps = (state) => {
  return {
    isLoading: state.loading.isLoading
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Loading)
