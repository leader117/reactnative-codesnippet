import React, { Component } from 'react'
import {
  Text,
  NetInfo,
  View,
  TouchableOpacity
} from 'react-native'
import styles from './Styles/InternetStatusViewStyles'
import SafeAreaView from 'react-native-safe-area-view'

export default class InternetStatusView extends Component {
  constructor (props) {
    super(props)
    this.state = {
      isConnected: true,
      isVisible: true
    }
  }

  componentDidMount () {
    NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectivityChange)
  }

  componentWillUnmount () {
    NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectivityChange)
  }

  handleConnectivityChange = isConnected => {
    if (isConnected) {
      this.setState({ isConnected, isVisible: true })
    } else {
      this.setState({ isConnected })
    }
  }

  render () {
    if (!this.state.isConnected && this.state.isVisible) {
      return (
        <SafeAreaView forceInset={{ top: 'always' }} style={styles.errorTextContainer}>
          <View style={styles.row}>
            <Text style={styles.errorTextShow}>
              Connection lost. Please check your internet connection.
            </Text>
            <TouchableOpacity
              style={styles.buttonDismiss}
              activeOpacity={0.7}
              onPress={() => this.setState({ isVisible: false })}
            >
              <Text style={[styles.errorTextShow, { fontSize: 10, flex: 0 }]}>Dismiss</Text>
            </TouchableOpacity>
          </View>
        </SafeAreaView>
      )
    }
    return null
  }
}
