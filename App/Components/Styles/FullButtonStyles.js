import { StyleSheet } from 'react-native'
import { Fonts, Colors } from '../../Themes/'

export default StyleSheet.create({
  button: {
    // marginVertical: 5,
    borderTopColor: Colors.fire,
    backgroundColor: '#FF4D0D',
    justifyContent: 'center',
    alignItems: 'center',
    height: 42,
    width: 315
  },
  buttonText: {
    // margin: 18,
    textAlign: 'center',
    color: Colors.white,
    fontFamily: Fonts.type.avenirHeavy,
    fontSize: Fonts.size.medium
  }
})
