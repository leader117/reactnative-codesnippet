import React, { Component } from 'react'
import { StyleSheet } from 'react-native'
import { Fonts, Metrics, Colors } from '../../Themes'

export default StyleSheet.create({
  container: {
    // margin: Metrics.smallMargin,
    // borderColor: Colors.border,
    borderRadius: 7,
    borderBottomWidth: 0.5,
    padding: Metrics.baseMargin,
    backgroundColor: Colors.white,
    borderColor: Colors.transparent,
    // marginBottom: Metrics.smallMargin
    borderWidth: 1
  }
})
