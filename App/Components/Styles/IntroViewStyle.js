import { StyleSheet } from 'react-native'
import { Metrics } from '../../Themes/'

export default StyleSheet.create({
  containerIntroView: {
    // flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: 'green',
    height: 100,
    width: 100
  },
  imgIntro: {
    height: 150,
    width: 100
  }

})
