import { StyleSheet } from 'react-native'
import { Fonts } from '../../Themes/'

export default StyleSheet.create({
  errorTextShow: {
    fontFamily: Fonts.type.avenirRoman,
    fontSize: 14,
    color: '#ffffff',
    lineHeight: 19,
    flex: 1
  },
  errorTextContainer: {
    backgroundColor: '#0F1524',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 20,
    paddingRight: 0
  },
  buttonDismiss: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 10,
    paddingHorizontal: 20
  }
})
