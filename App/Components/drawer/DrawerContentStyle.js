import { StyleSheet } from 'react-native'
import { Colors, Metrics, Fonts } from '../../Themes/'

export default StyleSheet.create({
  container: {
    backgroundColor: Colors.white,
    flex: 1
  },
  headDrawer: {
    justifyContent: 'center',
    alignItems: 'center',
    maxHeight: Metrics.screenHeight / 3
  },
  icAvaMan: {
    height: 90,
    width: 90,
    borderRadius: 45,
    borderColor: Colors.transparent,
    marginTop: Metrics.doubleSection
  },
  txtName: {
    fontFamily: Fonts.type.avenirRoman,
    fontSize: Fonts.size.input,
    marginTop: 8
  },
  icPlace: {
    height: 14,
    width: 10,
    marginRight: Metrics.smallMargin
  },
  rowAddress: {
    flexDirection: 'row',
    fontFamily: Fonts.type.avenirRoman,
    fontSize: Fonts.size.small,
    marginVertical: Metrics.doubleBaseMargin,
    color: Colors.textGrey
  },
  txtAddress: {
    color: '#9FA2A8',
    fontFamily: Fonts.type.avenirRoman,
    fontSize: Fonts.size.small
  },
  bodyDrawer: {
    padding: Metrics.doubleBaseMargin,
    flex: 1
  },
  icItem: {
    height: 18,
    width: 18,
    marginRight: Metrics.doubleBaseMargin
  },
  rowItem: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: Metrics.doubleBaseMargin
  },
  txtItem: {
    fontFamily: Fonts.type.avenirRoman,
    fontSize: Fonts.size.sixteen,
    color: '#636770'
  },
  buttonLogin: {
    width: 181,
    height: 42,
    borderRadius: 21,
    backgroundColor: '#eceff4',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    marginBottom: 20,
    marginTop: 10
  },
  txtLogin: {
    color: '#9b9b9b',
    fontFamily: Fonts.type.avenirHeavy,
    fontSize: 14
  },
  txtPlate: {
    color: '#000000',
    fontFamily: Fonts.type.mohrRounded,
    fontSize: 30,
    textAlign: 'center',
    marginTop: Metrics.doubleSection
  }
})
