import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Text, View, ViewPropTypes, Image, TouchableOpacity, Alert } from 'react-native'
import { Actions } from 'react-native-router-flux'
import styles from './DrawerContentStyle'
import { Images, Metrics, Colors } from '../../Themes'
import { Hr } from '../../Components'
import firebase from 'react-native-firebase'
import { bindActionCreators } from 'redux'
import ProfileActions from '../../Redux/ProfileRedux'

class DrawerContent extends React.Component {
  static propTypes = {
    name: PropTypes.string,
    sceneStyle: ViewPropTypes.style,
    title: PropTypes.string
  }

  static contextTypes = {
    drawer: PropTypes.object
  }

  logout = () => {
    Alert.alert('Confirmation', 'Do you want to sign out?',
      [
        { text: 'Cancel', style: 'cancel' },
        {
          text: 'OK',
          onPress: async () => {
            await firebase.auth().signOut()
            this.props.clearProfile()
            Actions.introduce({ type: 'reset' })
          }
        }
      ]
    )
  }

  render () {
    const profile = this.props.profile
    const location = profile && profile.location && profile.location.city && profile.location.country && [profile.location.city, profile.location.country].join(', ')
    return (
      <View style={styles.container}>
        {profile ? <View style={styles.headDrawer}>
          <Image source={profile && profile.user_meta.avatar_url ? { uri: profile.user_meta.avatar_url } : Images.profile} style={styles.icAvaMan} defaultSource={Images.profile} resizeMode='cover' />
          <Text style={styles.txtName}>{profile ? profile.user_meta.first_name + ' ' + profile.user_meta.last_name : 'Guest'}</Text>
          <View style={styles.rowAddress}>
            <Image source={Images.icPlace} style={styles.icPlace} />
            <Text style={styles.txtAddress}>{location || '---'}</Text>
          </View>
        </View>
          : <View style={styles.headDrawer}>
            <Text style={styles.txtPlate}>plate</Text>
            <TouchableOpacity
              onPress={() => Actions.introduce()}
              style={styles.buttonLogin}>
              <Text style={styles.txtLogin}>Login</Text>
            </TouchableOpacity>
          </View> }
        <Hr lineHeight={1} marginLeft={Metrics.smallMargin} marginRight={Metrics.smallMargin} lineColor={Colors.frost} marginTop={Metrics.baseMargin} marginBottom={Metrics.baseMargin} />
        <View style={styles.bodyDrawer}>
          <View style={{ flex: 1 }}>
            <TouchableOpacity style={styles.rowItem} onPress={() => profile ? Actions.profile() : Actions.introduce()}>
              <Image source={Images.icProfileDrawer} style={styles.icItem} />
              <Text style={styles.txtItem}>Profile</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.rowItem} onPress={() => profile ? Actions.activeOrders() : Actions.introduce()}>
              <Image source={Images.icOrdersDrawer} style={[styles.icItem, { height: 14 }]} />
              <Text style={styles.txtItem}>Active Orders</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => profile ? Actions.plateHistory() : Actions.introduce()}
              style={styles.rowItem}>
              <Image source={Images.icPlateHistoryDrawer} style={[styles.icItem, { height: 15 }]} />
              <Text style={styles.txtItem}>Plate History</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => profile ? Actions.paymentScreen() : Actions.introduce()} style={styles.rowItem}>
              <Image source={Images.icPaymentDrawer} style={[styles.icItem, { height: 12 }]} />
              <Text style={styles.txtItem}>Payment</Text>
              {this.props.isUnpaid && <Image style={{ marginHorizontal: 10 }} source={Images.unpaidIcon} />}
            </TouchableOpacity>
            <TouchableOpacity onPress={() => profile ? Actions.plateCash() : Actions.introduce()} style={styles.rowItem}>
              <Image source={Images.icPlateCashDrawer} style={[styles.icItem, { height: 18 }]} resizeMode={'contain'} />
              <Text style={styles.txtItem}>Plate Cash</Text>
            </TouchableOpacity>
            {/* <TouchableOpacity style={styles.rowItem}>
              <Image source={Images.icRewardsDrawer} style={styles.icItem} />
              <Text style={styles.txtItem}>Rewards</Text>
            </TouchableOpacity> */}
          </View>
          {profile && <TouchableOpacity style={styles.rowItem} onPress={() => Actions.support()}>
            <Image source={Images.icHeadPhoneDrawer} style={[styles.icItem, { height: 17 }]} resizeMode={'contain'} />
            <Text style={[styles.txtItem, { color: '#9FA2A8' }]}>Support</Text>
          </TouchableOpacity>}
          {profile && <TouchableOpacity style={styles.rowItem} onPress={() => profile && this.logout()}>
            <Image source={Images.icLogoutDrawer} style={[styles.icItem, { height: 14 }]} />
            <Text style={[styles.txtItem, { color: '#9FA2A8' }]}>Log out</Text>
          </TouchableOpacity>}
          {/* <TouchableOpacity style={styles.rowItem}>
            <SimpleLineIcons name='settings' size={18} style={{ marginRight: Metrics.doubleBaseMargin }} />
            <Text style={styles.txtItem}>Settings</Text>
          </TouchableOpacity> */}

        </View>

      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    profile: state.profile.profile
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    ...bindActionCreators(ProfileActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DrawerContent)
