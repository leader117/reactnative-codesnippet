import React, { Component } from 'react'
// import PropTypes from 'prop-types'
import { View, Text, StyleSheet, ActivityIndicator, Platform, TextInput as TextInputCore } from 'react-native'
import { Colors, Metrics, Fonts } from '../../Themes/'
import { TextInputMask } from 'react-native-masked-text'

export default class TextInput extends Component {
  static defaultProps = {
    showLabel: true
  }

  static propTypes = {
  }

  focus () {
    if (this.props.isMask) {
      this.refs.input.getElement().focus()
    } else {
      this.refs.input.focus()
    }
  }

  render () {
    const {
      containerStyle,
      inputWrapStyle,
      inputStyle,
      errorStyle,
      errorMessage,
      loading,
      showLabel,
      isMask
    } = this.props
    return (
      <View
        style={[styles.container, containerStyle]}
      >
        <Text style={styles.label}>{showLabel && this.props.value ? this.props.placeholder : ' '}</Text>
        <View style={[styles.inputWrap, inputWrapStyle]}>
          {
            isMask ? (
              <TextInputMask
                ref='input'
                style={[styles.input, inputStyle, { textAlignVertical: 'top' }]}
                placeholderTextColor='#BDBDBD'
                underlineColorAndroid='transparent'
                autoCorrect={false}
                selectionColor='#0F1524'
                {...this.props}
              />
            ) : (
              <TextInputCore
                ref='input'
                style={[styles.input, inputStyle, { textAlignVertical: 'top' }]}
                placeholderTextColor='#BDBDBD'
                underlineColorAndroid='transparent'
                autoCorrect={false}
                selectionColor='#0F1524'
                {...this.props}
              />
            )
          }
          {!!loading && <ActivityIndicator size='small' color={Colors.textGray} style={styles.activityIndicator} />}
        </View>
        {errorMessage ? (
          <Text style={[styles.error, errorStyle && errorStyle]}>
            {errorMessage}
          </Text>
        ) : null}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    minHeight: 60
  },
  inputWrap: {
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 2,
    borderBottomColor: '#0F1524'
  },
  input: {
    fontFamily: Fonts.type.avenirRoman,
    fontSize: 18,
    color: '#0F1524',
    flex: 1,
    marginHorizontal: 5,
    marginBottom: Platform.OS === 'ios' ? 10 : 0,
    marginTop: Platform.OS === 'ios' ? 8 : 0
  },
  error: {
    fontFamily: Fonts.type.avenirRoman,
    color: '#FF2D00',
    marginHorizontal: 15,
    margin: 5,
    fontSize: 12
  },
  label: {
    fontFamily: Fonts.type.avenirRoman,
    color: '#BDBDBD',
    fontSize: 12,
    marginHorizontal: Platform.OS === 'ios' ? 5 : 10
  },
  activityIndicator: {
    marginHorizontal: 3
  }
})
