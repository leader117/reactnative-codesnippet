import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View, Text, TouchableOpacity } from 'react-native'
import styles from './Styles/StarRatingStyle'
import Ionicons from 'react-native-vector-icons/Ionicons'

export default class StarRating extends Component {
  // // Prop type warnings
  static propTypes = {
    selectedStar: PropTypes.func.isRequired,
    size: PropTypes.number,
    rating: PropTypes.number
  }
  //
  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }

  render () {
    const rating = Math.round(this.props.rating)
    return (
      <View style={[styles.container, this.props.style]}>
        <TouchableOpacity style={styles.button} onPress={() => this.props.selectedStar(1)}>
          <Ionicons name='ios-star' size={this.props.size || 32} color={rating < 1 ? '#E7EAEF' : '#FDA24F'} />
        </TouchableOpacity>
        <TouchableOpacity style={styles.button} onPress={() => this.props.selectedStar(2)}>
          <Ionicons name='ios-star' size={this.props.size || 32} color={rating < 2 ? '#E7EAEF' : '#FF6C08'} />
        </TouchableOpacity>
        <TouchableOpacity style={styles.button} onPress={() => this.props.selectedStar(3)}>
          <Ionicons name='ios-star' size={this.props.size || 32} color={rating < 3 ? '#E7EAEF' : '#FF6C08'} />
        </TouchableOpacity>
        <TouchableOpacity style={styles.button} onPress={() => this.props.selectedStar(4)}>
          <Ionicons name='ios-star' size={this.props.size || 32} color={rating < 4 ? '#E7EAEF' : '#FF570B'} />
        </TouchableOpacity>
        <TouchableOpacity style={styles.button} onPress={() => this.props.selectedStar(5)}>
          <Ionicons name='ios-star' size={this.props.size || 32} color={rating < 5 ? '#E7EAEF' : '#FF450E'} />
        </TouchableOpacity>
      </View>
    )
  }
}
