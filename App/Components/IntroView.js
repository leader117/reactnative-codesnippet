import React, { Component } from 'react'
import { View, Image, Text } from 'react-native'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/IntroViewStyle'

class IntroScreen extends Component {
  render () {
    return (
      <View styles={styles.containerIntroView}>
        <Image source={this.props.urlImg} styles={styles.imgIntro} />
        <Text>{this.props.description}</Text>
      </View>
    )
  }
}

export default IntroScreen
