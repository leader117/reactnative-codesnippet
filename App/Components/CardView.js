import React, { Component } from 'react'
import {
  View
} from 'react-native'
import styles from './Styles/CardViewStyles'

export default class CardView extends Component {
  render () {
    return (
      <View {...this.props} style={[styles.container, this.props.style]}>
        {this.props.children}
      </View>
    )
  }
}
