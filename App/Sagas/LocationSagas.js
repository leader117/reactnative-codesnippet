/* ***********************************************************
* A short word on how to use this automagically generated file.
* We're often asked in the ignite gitter channel how to connect
* to a to a third party api, so we thought we'd demonstrate - but
* you should know you can use sagas for other flow control too.
*
* Other points:
*  - You'll need to add this saga to sagas/index.js
*  - This template uses the api declared in sagas/index.js, so
*    you'll need to define a constant in that file.
*************************************************************/

import { call, put } from 'redux-saga/effects'
import LocationActions from '../Redux/LocationRedux'
// import { LocationSelectors } from '../Redux/LocationRedux'
import ipstack from 'ipstackclient'
import Config from 'react-native-config'

export function * getCountry () {
  try {
    const response = yield ipstack.create(Config.IPSTACK_KEY).requesterLookup()
    __DEV__ && console.log('geo ip', response)
    yield put(LocationActions.setCountry(response))
  } catch (error) {

  }
}
