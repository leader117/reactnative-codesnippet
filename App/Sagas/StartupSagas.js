import { put, select, call, all } from 'redux-saga/effects'
// import { is } from 'ramda'
import firebase from 'react-native-firebase'
import { Actions } from 'react-native-router-flux'
import { getCountry } from './LocationSagas'
// import LoadingActions from '../Redux/LoadingRedux'
// import ProfileActions from '../Redux/ProfileRedux'
import { delay } from 'redux-saga'
import { getProfile } from './ProfileSagas'

// process STARTUP actions
export function * startup (api) {
  const results = yield all([
    call(checkLogged, api),
    call(getCountry),
    call(delay, 1500)
  ])
  const profile = results[0]
  if (profile && profile.user_meta.first_name) {
    yield call(Actions.root, { type: 'reset' })
  } else {
    yield call(Actions.introduce, { type: 'reset' })
  }
  // yield put(LoadingActions.hide())
}

function * checkLogged (api) {
  if (firebase.auth().currentUser) {
    const profile = yield call(getProfile, api)
    return profile
  }
}
