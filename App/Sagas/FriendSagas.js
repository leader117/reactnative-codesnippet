/* ***********************************************************
* A short word on how to use this automagically generated file.
* We're often asked in the ignite gitter channel how to connect
* to a to a third party api, so we thought we'd demonstrate - but
* you should know you can use sagas for other flow control too.
*
* Other points:
*  - You'll need to add this saga to sagas/index.js
*  - This template uses the api declared in sagas/index.js, so
*    you'll need to define a constant in that file.
*************************************************************/

import { call, put, select } from 'redux-saga/effects'
import FriendActions from '../Redux/FriendRedux'
import { showError } from '../Utilities/utils'
import firebase from 'react-native-firebase'
import { mapObjectToArray } from '../Lib/Lib'
import _ from 'lodash'
import LoadingActions from '../Redux/LoadingRedux'
// import { LocationSelectors } from '../Redux/LocationRedux'

export function * syncContacts (api, { data, dataContacts }) {
  const token = yield firebase.auth().currentUser.getIdToken()
  const currentUser = yield firebase.auth().currentUser
  api.setToken(token)
  const response = yield call(api.syncContacts, data)
  __DEV__ && console.log('response', response)
  if (response.ok) {
    const data = response.data.data
    let contacts = []
    for (let i = 0; i < data.length; i++) {
      let contact = { ..._.find(dataContacts, { phone_number: data[i].phone_number }), ...data[i] }
      console.log('contact', contact)
      if (data[i].uid === currentUser._user.uid) {
        contact = { ...contact, ...{ add: true, uid: data[i].uid } }
      } else {
        const { friends } = yield select(state => state.friend)
        if (_.findIndex(friends, { uid: data[i].uid }) === -1) {
          contact = { ...contact, ...{ add: false, uid: data[i].uid } }
        } else {
          contact = { ...contact, ...{ add: true, uid: data[i].uid } }
        }
      }
      console.log('contact', contact)
      contacts.push(contact)
    }
    yield put(FriendActions.syncContactsSuccess(contacts))
  } else {
    showError(response.data.error || 'cannot connect to server')
  }
}

export function * getFriends (api) {
  const token = yield firebase.auth().currentUser.getIdToken()
  const currentUser = yield firebase.auth().currentUser
  api.setToken(token)
  try {
    const response = yield firebase.database().ref(`users/${currentUser._user.uid}/friends`).once('value')
    const friends = mapObjectToArray(response.val())
    yield put(FriendActions.getFriendsSuccess(friends))
  } catch (e) {
    console.log('e', e)
  }
}

export function * addFriend (api, { uid, contacts, dataContacts }) {
  yield put(LoadingActions.show())
  const token = yield firebase.auth().currentUser.getIdToken()
  api.setToken(token)
  const response = yield call(api.addFriend, uid)
  __DEV__ && console.log('response', response)
  if (response.ok) {
    yield call(getFriends, api)
    yield call(syncContacts, api, { data: contacts, dataContacts })
  }
  yield put(LoadingActions.hide())
}

export function * removeFriend (api, { uid, contacts, dataContacts }) {
  yield put(LoadingActions.show())
  const token = yield firebase.auth().currentUser.getIdToken()
  api.setToken(token)
  const response = yield call(api.removeFriend, uid)
  __DEV__ && console.log('response', response)
  if (response.ok) {
    yield call(getFriends, api)
    yield call(syncContacts, api, { data: contacts, dataContacts })
  }
  yield put(LoadingActions.hide())
}
