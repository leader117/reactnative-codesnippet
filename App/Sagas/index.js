import { takeLatest, all } from 'redux-saga/effects'
import API from '../Services/Api'
import FixtureAPI from '../Services/FixtureApi'
import DebugConfig from '../Config/DebugConfig'

/* ------------- Types ------------- */

import { StartupTypes } from '../Redux/StartupRedux'
import { FriendTypes } from '../Redux/FriendRedux'
// import { AuthTypes } from '../Redux/AuthRedux'
/* ------------- Sagas ------------- */

import { startup } from './StartupSagas'
// import {
// login
// } from './AuthSagas'
/* ------------- API ------------- */
import { ProfileTypes } from '../Redux/ProfileRedux'
import { RestaurantTypes } from '../Redux/RestaurantRedux'
import { getProfile, uploadAvatar, updateProfile, checkProfile } from './ProfileSagas'
import { searchRestaurant } from './RestaurantSagas'
import { syncContacts, getFriends, addFriend, removeFriend } from './FriendSagas'
// The API we use is only used from Sagas, so we create it here and pass along
// to the sagas which need it.
const api = DebugConfig.useFixtures ? FixtureAPI : API.create()

/* ------------- Connect Types To Sagas ------------- */

export default function * root () {
  yield all([
    // some sagas only receive an action
    takeLatest(StartupTypes.STARTUP, startup, api),
    // some sagas receive extra parameters in addition to an action
    takeLatest(ProfileTypes.GET_PROFILE, getProfile, api),
    takeLatest(ProfileTypes.CHECK_PROFILE, checkProfile, api),
    takeLatest(ProfileTypes.UPDATE_PROFILE, updateProfile, api),
    takeLatest(ProfileTypes.UPLOAD_AVATAR, uploadAvatar, api),
    takeLatest(RestaurantTypes.SEARCH_RESTAURANT_REQUEST, searchRestaurant, api),
    takeLatest(FriendTypes.SYNC_CONTACTS, syncContacts, api),
    takeLatest(FriendTypes.GET_FRIENDS, getFriends, api),
    takeLatest(FriendTypes.ADD_FRIEND, addFriend, api),
    takeLatest(FriendTypes.REMOVE_FRIEND, removeFriend, api)
  ])
}
