/* ***********************************************************
* A short word on how to use this automagically generated file.
* We're often asked in the ignite gitter channel how to connect
* to a to a third party api, so we thought we'd demonstrate - but
* you should know you can use sagas for other flow control too.
*
* Other points:
*  - You'll need to add this saga to sagas/index.js
*  - This template uses the api declared in sagas/index.js, so
*    you'll need to define a constant in that file.
*************************************************************/

import { call, put, select } from 'redux-saga/effects'
import ProfileActions from '../Redux/ProfileRedux'
// import { ProfileSelectors } from '../Redux/ProfileRedux'
// import rnFetchBlob from 'rn-fetch-blob'
import firebase from 'react-native-firebase'
import LoadingActions from '../Redux/LoadingRedux'
import { showError } from '../Utilities/utils'
import { Actions } from 'react-native-router-flux'
// import Config from 'react-native-config'

export function * checkProfile (api) {
  const result = yield call(getProfile, api)
  if (!result || !result.user_meta.first_name) {
    yield call(Actions.inputEmail)
  } else {
    yield call(Actions.root, { type: 'reset' })
  }
  // yield call(Actions.inputEmail)
}

export function * getProfile (api, action) {
  // yield put(LoadingActions.show())
  // get current data from Store
  const token = yield firebase.auth().currentUser.getIdToken()
  api.setToken(token)
  // make the call to the api
  const response = yield call(api.getProfile)
  // yield put(LoadingActions.hide())
  __DEV__ && console.log('get profile', response)
  // success?
  if (response.ok) {
    // You might need to change the response here - do this with a 'transform',
    // located in ../Transforms/. Otherwise, just pass the data back from the api.
    yield put(ProfileActions.getProfileSuccess(response.data.data || {}))
    return response.data.data
  } else {
    yield put(ProfileActions.getProfileFailure())
  }
}

export function * updateProfile (api, action) {
  yield put(LoadingActions.show())
  const { profile } = action
  // get current data from Store
  const token = yield firebase.auth().currentUser.getIdToken()
  api.setToken(token)
  // make the call to the api
  const response = yield call(api.updateProfile, profile) 
  yield put(LoadingActions.hide())
  __DEV__ && console.log('update profile', response)
  // success?
  if (response.ok) {
    yield call(getProfile, api)
    yield put(ProfileActions.updateProfileSuccess(response.data))
    yield call(Actions.root, { type: 'reset' })
  } else {
    showError((response.data || {}).error || 'cannot connect to server')
    yield put(ProfileActions.updateProfileFailure())
  }
}

export function * uploadAvatar (api, action) {
  const { data } = action
  yield put(LoadingActions.show())
  try {
    const userId = firebase.auth().currentUser.uid
    const ref = firebase.storage().ref().child(`avatars/${userId}`)
    // yield call(ref.putFile, data.path)
    const avatar = yield new Promise(resolve => {
      const unsubscribe = ref.putFile(data.path).on(
        'state_changed',
        snapshot => { },
        err => {
          console.log(err)
          unsubscribe()
          resolve(null)
        },
        uploadedFile => {
          __DEV__ && console.log('upload', uploadedFile)
          const url = uploadedFile.downloadURL
          unsubscribe()
          resolve(url)
        }
      )
    })
    __DEV__ && console.log('avatar', avatar)
    const profile = yield select(state => state.profile.profile)
    const token = yield firebase.auth().currentUser.getIdToken()
    api.setToken(token)
    // make the call to the api
    const response = yield call(api.updateProfile, { user_meta: { ...profile.user_meta, ...{ avatar_url: avatar } } })
    __DEV__ && console.log('update profile', response)
    if (response.ok) {
      yield call(getProfile, api)
    } else {
      showError(response.data.error || 'cannot connect to server')
      yield put(ProfileActions.updateProfileFailure())
    }
    // const token = yield firebase.auth().currentUser.getIdToken()
    // 'data:image/jpeg;base64,'
    // console.log(token)
    // const url = `${Config.API_URL}/profile/upload`
    // console.log(url)
    // const response = yield rnFetchBlob.fetch('POST', url, {
    //   authorization: token,
    //   'Content-Type': 'multipart/form-data'
    // }, [{ name: 'image', filename: 'avatar.jpg', type: 'image/jpg', data }])
    // __DEV__ && console.log('upload avatar', response.data)
    // if (response.respInfo && response.respInfo.status === 200) {
    //   yield call(getProfile)
    // } else {
    //   showError(response.data)
    // }
  } catch (error) {
    console.log(error)
    showError(error.message)
  } finally {
    yield put(LoadingActions.hide())
  }
}
