import { Alert } from 'react-native'
import geoLib from 'geolib';

// export function rgbColor (hex, a) {
//   return `rgba(${hexToRgb(hex).r},${hexToRgb(hex).g},${hexToRgb(hex).b},${a})`
// }

export const showError = (message) => {
  setTimeout(() => {
    Alert.alert('Error', message)
  }, 300)
}

export const showMessage = (title, message) => {
  setTimeout(() => {
    Alert.alert(title || '', message)
  }, 300)
}

export const calculateDistance = (start, end) => {
  const result = geolib.getDistance(start, end);
    
  return (result / 1000).toFixed(2);
}