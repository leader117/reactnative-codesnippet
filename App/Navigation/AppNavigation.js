import React, { Component } from 'react'
import { Platform, StatusBar, View } from 'react-native'
// import { StackViewStyleInterpolator } from 'react-navigation-stack'
import {
  Scene,
  Router,
  // Actions,
  Reducer,
  // ActionConst,
  Overlay,
  // Tabs,
  Modal,
  Drawer,
  Stack
  // Lightbox
} from 'react-native-router-flux'
import DrawerContent from '../Components/drawer/DrawerContent'
// import TabIcon from '../Components/tab/TabIcon'
import { Colors, Images } from '../Themes'
import styles from './Styles/NavigationStyles'

import LaunchScreen from '../Containers/LaunchScreen'
import LoginScreen from '../Containers/LoginScreen'
import HomeScreen from '../Containers/HomeScreen'
import SearchScreen from '../Containers/SearchScreen'
import ProfileScreen from '../Containers/ProfileScreen'
import ProfileEditScreen from '../Containers/ProfileEditScreen'
import ActiveOrdersScreen from '../Containers/ActiveOrdersScreen'
import LoginPhoneScreen from '../Containers/LoginPhoneScreen'
import IntroScreen from '../Containers/IntroScreen'
import LoginConfirmCodeScreen from '../Containers/LoginConfirmCodeScreen'
import LoginPasswordScreen from '../Containers/LoginPasswordScreen'
import MenuScreen from '../Containers/MenuScreen'
import InputEmailScreen from '../Containers/InputEmailScreen'
import InputNameScreen from '../Containers/InputNameScreen'
import AddPaymentScreen from '../Containers/AddPaymentScreen'
import AddCardScreen from '../Containers/AddCardScreen'
import RestaurantScreen from '../Containers/RestaurantScreen'
import RestaurantInviteFriendScreen from '../Containers/RestaurantInviteFriendScreen'
import RestaurantFriendScreen from '../Containers/RestaurantFriendScreen'
import RestaurantItemScreen from '../Containers/RestaurantItemScreen'
import RestaurantTableScreen from '../Containers/RestaurantTableScreen'
import RestaurantBillScreen from '../Containers/RestaurantBillScreen'
import RestaurantDineInConfirmScreen from '../Containers/RestaurantDineInConfirmScreen'
import RestaurantPickUpScreen from '../Containers/RestaurantPickUpScreen'
import RestaurantPickUpPaySuccessScreen from '../Containers/RestaurantPickUpPaySuccessScreen'
import RestaurantRateScreen from '../Containers/RestaurantRateScreen'
import RestaurantReviewScreen from '../Containers/RestaurantReviewScreen'
import ConditionsPrivacyPolicyScreen from '../Containers/ConditionsPrivacyPolicyScreen'
import TermsAndConditionsScreen from '../Containers/TermsAndConditionsScreen'
import PrivacyPolicyScreen from '../Containers/PrivacyPolicyScreen'
import PaymentScreen from '../Containers/PaymentScreen'
import PaymentAddCardScreen from '../Containers/PaymentAddCardScreen'
import PaymentPromoCodeScreen from '../Containers/PaymentPromoCodeScreen'
import PaymentPromoCodeSuccessScreen from '../Containers/PaymentPromoCodeSuccessScreen'
import PlateCashScreen from '../Containers/PlateCashScreen'
import SupportScreen from '../Containers/SupportScreen'
import UpdatePhoneNumberScreen from '../Containers/UpdatePhoneNumberScreen'
import FriendListScreen from '../Containers/FriendListScreen'
import CustomNavBar from './CustomNavBar'
import CustomWhiteNavBar from './CustomWhiteNavBar'
import CustomNavBarHome from './CustomNavBarHome'
import PlateHistoryScreen from '../Containers/PlateHistoryScreen'
import PlateHistoryCommentScreen from '../Containers/PlateHistoryCommentScreen'
import UpdatePhoneConfirmCodeScreen
  from '../Containers/UpdatePhoneConfirmCodeScreen'

const reducerCreate = params => {
  const defaultReducer = new Reducer(params)
  return (state, action) => {
    // console.log('ACTION:', action)
    return defaultReducer(state, action)
  }
}

const getSceneStyle = () => ({
  backgroundColor: '#F5FCFF',
  shadowOpacity: 0,
  shadowRadius: 0
})

// on Android, the URI prefix typically contains a host in addition to scheme
const prefix = Platform.OS === 'android' ? 'mychat://mychat/' : 'mychat://'

class AppNavigation extends Component {
  render () {
    return (
      <Router
        createReducer={reducerCreate}
        getSceneStyle={getSceneStyle}
        uriPrefix={prefix}
      >
        <Overlay key='overlay'>
          <Modal key='modal'
            hideNavBar
          >
            <Stack
              hideNavBar
              key='root'
              type='reset'
              headerLayoutPreset='center'
            >
              <Drawer
                hideNavBar
                key='drawer'
                contentComponent={DrawerContent}
                drawerImage={Images.icMenu}
                drawerWidth={300}
              >

                <Stack>
                  <Scene
                    initial
                    key='home'
                    navTransparent
                    component={HomeScreen}
                    title='plate'
                    navBar={CustomNavBarHome}
                  />
                  <Scene key='profile' component={ProfileScreen} title='PROFILE' back navBar={props => <CustomWhiteNavBar {...props} noBorder />} />
                  <Scene key='friendList' component={FriendListScreen} title='FRIEND LIST' back navBar={props => <CustomWhiteNavBar {...props} noBorder />} />
                  <Scene key='profileEdit' component={ProfileEditScreen} title='EDIT PROFILE' back navBar={props => <CustomWhiteNavBar {...props} noBorder />} />
                  <Scene key='updatePhoneNumber' component={UpdatePhoneNumberScreen} title='' back navBar={CustomWhiteNavBar} />
                  <Scene key='updatePhoneConfirmCode' component={UpdatePhoneConfirmCodeScreen} title='' back navBar={CustomWhiteNavBar} />
                  <Scene key='activeOrders' component={ActiveOrdersScreen} title='ACTIVE ORDERS' back navBar={CustomWhiteNavBar} />
                  <Scene key='plateCash' component={PlateCashScreen} title='PLATE CASH' back navBar={CustomWhiteNavBar} />
                  <Scene key='support' component={SupportScreen} title='SUPPORT' back navBar={CustomWhiteNavBar} />
                  <Scene key='plateHistory' component={PlateHistoryScreen} back navBar={CustomWhiteNavBar} title='PLATE HISTORY' />
                  <Scene key='plateHistoryComment' component={PlateHistoryCommentScreen} back navBar={CustomWhiteNavBar} title='' />
                  <Scene key='menu' component={MenuScreen} title='PLATE' />
                </Stack>
              </Drawer>
              <Scene key='search' component={SearchScreen} title='SEARCH' hideNavBar={false} back navBar={CustomWhiteNavBar} />
              <Scene key='restaurant' component={RestaurantScreen} hideNavBar={false} back navBar={CustomNavBar} />
              <Scene key='restaurantInviteFriend' component={RestaurantInviteFriendScreen} title='INVITE' hideNavBar={false} back navBar={props => <CustomWhiteNavBar {...props} noBorder />} />
              <Scene key='restaurantFriend' component={RestaurantFriendScreen} title='FRIENDS' hideNavBar={false} back navBar={props => <CustomWhiteNavBar {...props} noBorder />} />
              <Scene key='restaurantItemScreen' component={RestaurantItemScreen} hideNavBar={false} back navBar={CustomWhiteNavBar} />
              <Scene key='restaurantTableScreen' component={RestaurantTableScreen} hideNavBar={false} back navBar={CustomWhiteNavBar} />
              <Scene key='restaurantBillScreen' component={RestaurantBillScreen} hideNavBar={false} back navBar={CustomWhiteNavBar} title='THE BILL' />
              <Scene key='restaurantDineInConfirmScreen' component={RestaurantDineInConfirmScreen} title='TABLE 10' hideNavBar={false} back navBar={CustomWhiteNavBar} />
              <Scene key='restaurantPickUpScreen' component={RestaurantPickUpScreen} title='PICK UP' hideNavBar={false} back navBar={CustomWhiteNavBar} />
              <Scene key='restaurantPickUpPaySuccessScreen' component={RestaurantPickUpPaySuccessScreen} title='' hideNavBar={false} back navBar={CustomWhiteNavBar} />
              <Scene key='restaurantRateScreen' component={RestaurantRateScreen} title='RATE' hideNavBar={false} back navBar={CustomWhiteNavBar} />
              <Scene key='restaurantReviewScreen' component={RestaurantReviewScreen} title='REVIEWS' hideNavBar={false} back navBar={CustomWhiteNavBar} />
              <Scene key='paymentScreen' component={PaymentScreen} hideNavBar={false} back navBar={CustomWhiteNavBar} title='PAYMENT' />
              <Scene key='paymentAddCardScreen' component={PaymentAddCardScreen} hideNavBar={false} back navBar={CustomWhiteNavBar} title='ADD CARD' />
              <Scene key='paymentPromoCodeScreen' component={PaymentPromoCodeScreen} hideNavBar={false} back navBar={CustomWhiteNavBar} title='PROMO CODE' />
              <Scene key='paymentPromoCodeSuccessScreen' component={PaymentPromoCodeSuccessScreen} hideNavBar={false} back navBar={CustomWhiteNavBar} title='PROMO CODE' />
            </Stack>
            <Scene initial key='launchScreen' component={LaunchScreen} title='Launch' hideNavBar />
            <Stack
              key='auth'
              type='reset'
              navBar={props => <CustomWhiteNavBar {...props} noBorder noSpace />}
            >
              <Scene key='loginPhone' component={LoginPhoneScreen} hideNavBar={false} />
              <Scene key='login' component={LoginScreen} hideNavBar={false} />
              <Scene key='loginConfirmCode' component={LoginConfirmCodeScreen} hideNavBar={false} back />
              <Scene key='loginPassword' component={LoginPasswordScreen} hideNavBar={false} back />
              <Scene key='inputEmail' component={InputEmailScreen} hideNavBar={false} back />
              <Scene key='inputName' component={InputNameScreen} hideNavBar={false} back />
              <Scene key='conditionsPrivacyPolicy' component={ConditionsPrivacyPolicyScreen} title='plate' hideNavBar={false} titleStyle={styles.titleHome} back />
              <Scene key='termsAndConditions' component={TermsAndConditionsScreen} title='plate' hideNavBar={false} titleStyle={styles.titleHome} back />
              <Scene key='privacyPolicy' component={PrivacyPolicyScreen} title='plate' hideNavBar={false} titleStyle={styles.titleHome} back />
              <Scene key='addPayment' component={AddPaymentScreen} title='plate' hideNavBar={false} titleStyle={styles.titleHome} back />
              <Scene key='addCard' component={AddCardScreen} title='ADD CARD' back />
            </Stack>
            <Scene
              key='introduce'
              component={IntroScreen}
              hideNavBar={false}
              title='plate'
              navBar={props => <CustomWhiteNavBar {...props} noBorder noSpace />}
              titleStyle={styles.titleHome}
              type='reset'
            />

          </Modal>
        </Overlay>
      </Router>
    )
  }
}

export default AppNavigation
