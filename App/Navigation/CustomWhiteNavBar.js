import {
  Image,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  StatusBar
} from 'react-native'
import React from 'react'
import { Actions } from 'react-native-router-flux'
import { Images, Fonts } from '../Themes'

const styles = StyleSheet.create({
  container: {
    height: Platform.OS === 'ios' ? 88 : 58,
    flexDirection: 'row',
    alignItems: 'flex-end',
    paddingBottom: 15,
    backgroundColor: '#fff'
  },
  border: {
    borderBottomWidth: 1,
    borderBottomColor: '#cccccc',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.06,
    shadowRadius: 8,
    elevation: 2
  },
  navBarItem: {
    flex: 1,
    height: 30,
    justifyContent: 'center'
  },
  title: {
    fontFamily: Fonts.type.avenirHeavy,
    letterSpacing: 1.75,
    fontSize: 14,
    alignSelf: 'center',
    textAlign: 'center',
    color: '#0F1524'
  },
  rightTitle: {
    fontFamily: Fonts.type.avenirRoman,
    fontSize: 14,
    alignSelf: 'flex-end',
    textAlign: 'right',
    color: '#0F1524',
    marginRight: 22
  }
})

export default class CustomWhiteNavBar extends React.Component {
  // constructor(props) {
  //   super(props)
  // }

  _renderBack () {
    return this.props.back
      ? (
        <TouchableOpacity
          onPress={Actions.pop}
          style={[styles.navBarItem]}
        >
          <Image style={{ width: 18, height: 14, marginLeft: 22 }}
            resizeMode='contain'
            source={Images.icArrowLeft}
          />
        </TouchableOpacity>
      )
      : <View style={styles.navBarItem} />
  }

  _renderRight () {
    return !this.props.rightTitle || !this.props.onRight
      ? <View style={styles.navBarItem}><Text>{' '}</Text></View>
      : (
        <TouchableOpacity style={[styles.navBarItem, { alignItems: 'flex-end' }]} onPress={() => this.props.onRight()}>
          <Text style={styles.rightTitle}>{this.props.rightTitle}</Text>
        </TouchableOpacity>
      )
  }

  _renderTitle () {
    return (
      <View style={[styles.navBarItem, { flex: 2, alignItems: 'center' }]}>
        <Text style={[styles.title, this.props.titleStyle]}>{this.props.title}</Text>
      </View>
    )
  }

  render () {
    return (
      <View style={[styles.container, !this.props.noBorder ? styles.border : null]}>
        <StatusBar barStyle={Platform.select({ ios: 'dark-content' })} />
        {this._renderBack()}
        {this._renderTitle()}
        {this.props.renderRight
          ? this.props.renderRight()
          : this._renderRight()
        }
      </View>
    )
  }
}
