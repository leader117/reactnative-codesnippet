import { StyleSheet, Platform } from 'react-native'
import { Colors, Fonts } from '../../Themes/'

export default StyleSheet.create({
  header: {
    backgroundColor: Colors.backgroundColor
  },
  navigationBarStyle: {
    height: Platform.OS === 'ios' ? 88 : 78,
    backgroundColor: Colors.white,
    borderBottomWidth: 0,
    shadowOpacity: 0,
    elevation: 0,
    flexDirection: 'row',
    alignItems: 'flex-end',
    paddingBottom: 19
  },
  leftButtonIconStyle: {
    width: 18,
    height: 14
  },
  title: {
    fontFamily: Fonts.type.avenirHeavy,
    fontSize: 14,
    alignSelf: 'center',
    textAlign: 'center'
  },
  txtSkip: {
    color: '#555',
    fontSize: 14
  },
  navBarBorderStyle: {
    backgroundColor: Colors.white,
    borderBottomWidth: 1,
    shadowOpacity: 1,
    elevation: 1,
    borderBottomColor: Colors.backgroundGrey
  },
  navBarNoBorderStyle: {
    backgroundColor: Colors.white,
    borderBottomWidth: 0,
    shadowOpacity: 0,
    elevation: 0,
    borderBottomColor: 'transparent'
  },
  titleHomeStack: {
    fontFamily: Fonts.type.avenirHeavy,
    fontSize: Fonts.size.medium,
    alignSelf: 'center',
    textAlign: 'center',
    flex: 1
  },
  icMenu: {
    height: 14,
    width: 31
  },
  titleHome: {
    fontFamily: Fonts.type.mohrRounded,
    fontSize: 18,
    color: '#0F1524',
    alignSelf: 'center',
    textAlign: 'center',
    letterSpacing: 0
  }
})
