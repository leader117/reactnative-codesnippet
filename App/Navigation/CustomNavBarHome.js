import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native'
import React from 'react'
// import { Actions } from 'react-native-router-flux'
import { Fonts } from '../Themes'
import ButtonMenu from './ButtonMenu'

const styles = StyleSheet.create({
  container: {
    height: Platform.OS === 'ios' ? 88 : 58,
    flexDirection: 'row',
    alignItems: 'flex-end',
    paddingBottom: 15,
    backgroundColor: 'transparent'
  },
  navBarItem: {
    flex: 1,
    height: 30,
    justifyContent: 'center'
  },
  title: {
    fontFamily: Fonts.type.mohrRounded,
    fontSize: 18,
    color: '#fff',
    alignSelf: 'center',
    textAlign: 'center'
  }
})

export default class CustomNavBarHome extends React.Component {
  // constructor(props) {
  //   super(props)
  // }

  _renderTitle () {
    return (
      <View style={styles.navBarItem}>
        <Text style={styles.title}>{this.props.title}</Text>
      </View>
    )
  }

  render () {
    return (
      <View style={styles.container}>
        <View style={styles.navBarItem}>
          <ButtonMenu />
        </View>
        <View style={styles.navBarItem}>
          <Text style={styles.title}>{this.props.title}</Text>
        </View>
        <View style={styles.navBarItem} />
      </View>
    )
  }
}
