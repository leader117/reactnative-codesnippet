import {
  Image,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ImageBackground
} from 'react-native'
import React from 'react'
import { Actions } from 'react-native-router-flux'
import { Images, Fonts } from '../Themes'

const styles = StyleSheet.create({
  container: {
    height: Platform.OS === 'ios' ? 88 : 58,
    flexDirection: 'row',
    alignItems: 'flex-end',
    paddingBottom: 15
  },
  navBarItem: {
    flex:1,
    height: 30,
    justifyContent: 'center'
  },
  title: {
    fontFamily: Fonts.type.avenirHeavy,
    letterSpacing: 1.75,
    fontSize: 14,
    alignSelf: 'center',
    textAlign: 'center',
    color: '#fff'
  }
})

export default class CustomNavBar extends React.Component {
  // constructor(props) {
  //   super(props)
  // }

  _renderBack () {
    return (
      <TouchableOpacity
        onPress={Actions.pop}
        style={styles.navBarItem}
      >
        <Image style={{ width: 18, height: 14, marginLeft: 22 }}
          resizeMode='contain'
          source={Images.icBack}
        />
      </TouchableOpacity>
    )
  }

  _renderRight () {
    return (
      <View style={styles.navBarItem} />
    )
  }

  _renderTitle () {
    return (
      <View style={styles.navBarItem}>
        <Text style={styles.title}>{this.props.title}</Text>
      </View>
    )
  }

  render () {
    return (
      <ImageBackground style={styles.container} source={Images.backgroundHomeHeader}>
        {this.props.back && this._renderBack()}
        {this._renderTitle()}
        {this.props.renderRight ? this.props.renderRight() : this._renderRight()}
      </ImageBackground>
    )
  }
}
