import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Image, TouchableOpacity } from 'react-native'
import { Colors, Images, Metrics } from '../Themes'
import styles from './Styles/NavigationStyles'
import { Actions } from 'react-native-router-flux'

const ButtonMenu = ({ onPressMenu }) => {
  return (
    <TouchableOpacity onPress={() => Actions.drawerOpen()} style={{ paddingLeft: 22 }}>
      <Image source={Images.icMenuTwo} style={styles.icMenu} />
    </TouchableOpacity>
  )
}

ButtonMenu.propTypes = {
}

export default ButtonMenu
